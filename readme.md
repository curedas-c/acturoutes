# HOW TO DEPLOY

1 - Connect via SSH `ssh root@198.245.60.125` with password `Fac4LjnNZrHj`
2 - go to root directory `cd /home/actinfo/public`.
3 - Assure to have latest version of `package.json`, at this folder level.
4 - run `npm i --legacy-peer-deps` if necessary.
5 - copy build folder [dist] here, if necessary. API `files` folder should be at `dist/apps/` level.
6 - Restart project from admin panel [NodeJS-manager].
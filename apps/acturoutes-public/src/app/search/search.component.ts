import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { SharedObjectService } from '~core/services/shared-object.service';
import { Article } from '~shared/models/article/article.model';
import { finalize, takeUntil } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {
  articles: Article[];
  isLoading = false;
  currentPage = 1;
  totalPage = 1;
  searchItem: string = '';
  rootURL = environment.serverAssetUrl || 'assets/images/';
  optionForm: FormGroup = new FormGroup({});
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  private unsubscribe$ = new Subject<void>();
  constructor(private reqService: SharedObjectService, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.optionForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(5)]],
      type: [''],
    });
  }

  getData(page: number = 1) {
    this.switchLoadingState();
    const params = {
        filterOn: this.optionForm.controls.type.value,
        filter: this.optionForm.controls.title.value,
        limit: 9,
        page
    }
    this.reqService
      .getArticle(params)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchLoadingState();
        })
      )
      .subscribe((res) => {
        this.articles = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      },
      err => {
        if (this.currentPage > 1) {
          this.currentPage -= 1;
        }
      });
  }

  getAds() {
    this.reqService
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }

  changePage(page: number) {
    this.getData(page);
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }

}

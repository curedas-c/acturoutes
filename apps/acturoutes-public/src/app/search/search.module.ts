import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';
import { SharedModule } from '~shared/modules/shared/shared.module';


@NgModule({
  declarations: [
    SearchComponent
  ],
  imports: [
    CommonModule,
    SearchRoutingModule,
    PaginationModule,
    SharedModule
  ]
})
export class SearchModule { }

import { Component, NgZone } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, takeUntil, tap } from 'rxjs/operators';
import { CookiesService } from '@core/services/cookies.service';
import { CurrentStateService } from '~core/services/current-state.service';
import { routeAnimation } from '~shared/animations/animation';
import { GlobalFacade } from '~store/facades/global.facade';
import {
  faXTwitter
} from '@fortawesome/free-brands-svg-icons';
import { FaIconLibrary } from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'acturoutes-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimation],
})
export class AppComponent {
  currentPath: string;
  oldPath: string;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private ngZone: NgZone,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cookies: CookiesService,
    private state: CurrentStateService,
    private globalFacade: GlobalFacade,
    private faIconLibrary: FaIconLibrary
  ) {
    this.faIconLibrary.addIcons(faXTwitter)
    this.router.events
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((event) => event instanceof NavigationEnd)
      )
      .subscribe((ev: NavigationEnd) => {
        this.currentPath = ev.url;
        if (!this.oldPath) {
          this.oldPath === ev.url;
        }
      });

    this.listenToRouteParamsChanges();
  }

  ngOnInit(): void {
    this.ngZone.runOutsideAngular(() => {
      setInterval(() => {
        if (this.oldPath === this.currentPath) {
          this.ngZone.run(() => {
            this.reload(this.currentPath);
          });
        }
        this.oldPath = this.currentPath;
      }, 400000);
    });

    this.cookies
      .getCookie('credentials')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res) {
          this.state.setUserState(true);
        }
      });
  }

  // reloads app
  reload(route: string) {
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([route]);
    });
  }

  ngOnDestroy() {
    // this.clearIntroData();
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  listenToRouteParamsChanges() {
    this.router.events
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((event) => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map((route) => {
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        tap(({ snapshot }) => {
          const routeData = this.globalFacade.getRouteData();
          this.globalFacade.setRouteData({
            params: snapshot.params,
            queryParams: snapshot.queryParams,
            currentUrl: this.router.url,
            currentUrlWithoutParams: this.router.url.split('?')[0],
            previousUrl: routeData?.currentUrl || '/',
          });
        })
      )
      .subscribe();
  }

  // clearIntroData() {
  //   sessionStorage.removeItem('lastTimeClosedIntro');
  // }
}

// import { NgModule } from '@angular/core';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import { AppModule} from './app.module';
// import { AppComponent } from './app.component';
// // import { BrowserTransferStateModule } from '@angular/platform-browser';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { BrowserStateInterceptor } from '~core/interceptors/browserstate.interceptor';
// import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// @NgModule({
//   imports: [
//     AppModule,
//     BrowserAnimationsModule,
//     // BrowserTransferStateModule,
//     FontAwesomeModule
//   ],
//   providers: [
//     {
//         provide: HTTP_INTERCEPTORS,
//         useClass: BrowserStateInterceptor,
//         multi: true,
//       },
//   ],
//   bootstrap: [AppComponent]
// })
// export class AppBrowserModule { }
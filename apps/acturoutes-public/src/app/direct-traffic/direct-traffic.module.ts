import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectTrafficRoutingModule } from './direct-traffic-routing.module';
import { DirectTrafficComponent } from './direct-traffic.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { DirectTrafficService } from './shared/services/direct-traffic.service';
import { PaginationModule } from '../~shared/modules/pagination/pagination.module';


@NgModule({
  declarations: [
    DirectTrafficComponent
  ],
  imports: [
    CommonModule,
    DirectTrafficRoutingModule,
    PaginationModule,
    SharedModule
  ],
  providers: [DirectTrafficService]
})
export class DirectTrafficModule { }

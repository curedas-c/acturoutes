import { Component, OnInit } from '@angular/core';
import { Traffic } from '~shared/models/traffic/traffic.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { DirectTrafficService } from './shared/services/direct-traffic.service';

@Component({
  selector: 'app-direct-traffic',
  templateUrl: './direct-traffic.component.html',
  styleUrls: ['./direct-traffic.component.scss']
})
export class DirectTrafficComponent implements OnInit {

  traffics: Traffic[];
  currentPage = 1;
  totalPage = 1;
  params: any = {
    filterOn: 'isRdcMessage',
    filterOnValue: false,
  };
  haveError = false;
  isLoading = false;
  today: string = new Date().toISOString();
  private unsubscribe$ = new Subject<void>();
  constructor(private trafficService: DirectTrafficService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.switchLoadingState();
    this.haveError = false;
    this.traffics = null;
    this.trafficService
      .getDayTraffic(this.params)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => {
        this.switchLoadingState();
      }))
      .subscribe((res) => {
        this.traffics = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      }, err => {
        this.haveError = true;
      });
  }

  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page
    };
    this.getData();
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }

  changeFilter(useRdc: boolean) {
    this.params = {
      filterOn: 'isRdcMessage',
      filterOnValue: useRdc,
    }
    this.getData();
  }

}

import { Injectable } from '@angular/core';
import { ApiService } from '~core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Traffic } from '~shared/models/traffic/traffic.model';
import { Res } from '~shared/models/generic/response.model';

@Injectable()
export class DirectTrafficService {
  private endpoints = {
    traffic: 'traffic'
  };
  constructor(private _apiService: ApiService) {}

  getDayTraffic(params?: any): Observable<Res<Traffic>> {
    return this._apiService
      .get(`${this.endpoints.traffic}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Traffic[] = response.items?.map((res: any) => {
            return new Traffic(res);
          });

          return {
            items: mapped,
            total_items: parseInt(response.total_items, 10),
            total_pages: parseInt(response.total_pages, 10),
            current_page: parseInt(response.current_page, 10)
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }
}

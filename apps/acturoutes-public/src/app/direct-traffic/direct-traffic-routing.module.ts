import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirectTrafficComponent } from './direct-traffic.component';

const routes: Routes = [
  {
    path: '',
    component: DirectTrafficComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectTrafficRoutingModule { }

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { TrafficImage } from '../../models/traffic-image/traffic-image.model';
import { inOutAnimation } from '../../animations/animation';
import { GlobalFacade } from '~store/facades/global.facade';

@Component({
  selector: 'app-traffic-image',
  templateUrl: './traffic-image.component.html',
  styleUrls: ['./traffic-image.component.scss'],
  animations: [inOutAnimation],
})
export class TrafficImageComponent implements OnInit, OnDestroy {
  images: TrafficImage[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  showModal = false;
  currentImage = '';
  currentText = '';
  isRdcPage$ = this.globalFacade.routeData$.pipe(
    map((res) => res?.currentUrl?.includes('/acturoutes-rdc')),
    distinctUntilChanged()
  );
  private unsubscribe$ = new Subject<void>();
  constructor(
    private api: SharedObjectService,
    private globalFacade: GlobalFacade
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.isRdcPage$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap((isRdcPage) =>
          isRdcPage
            ? this.api.getTrafficImage({
                filterOn: 'isRdcImage',
                filterOnValue: true,
              })
            : this.api.getTrafficImage({
                filterOn: 'isRdcImage',
                filterOnValue: false,
              })
        )
      )
      .subscribe((res) => {
        this.images = res;
      });
  }

  toggleModal() {
    this.showModal = !this.showModal;
  }

  setCurrentData(img: string, text: string) {
    this.currentImage = img;
    this.currentText = text;
    this.toggleModal();
  }
}

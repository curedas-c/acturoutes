import { Component, OnDestroy } from '@angular/core';
import { Popup } from '~shared/models/popup/popup.model';
import { Observable, Subject } from 'rxjs';
import { CurrentStateService } from '~core/services/current-state.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent {

  popupState$: Observable<Popup>;
  constructor(private state: CurrentStateService) {
    this.popupState$ = this.state.popup$;
  }

  close(): void {
    this.state.setPopupState({visible: false, msg: '', colorClass: ''});
  }
}
import { Component, OnInit, OnDestroy } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Article } from '~shared/models/article/article.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-big-format',
  templateUrl: './big-format.component.html',
  styleUrls: ['./big-format.component.scss'],
})
export class BigFormatComponent implements OnInit, OnDestroy {
  firstArticle: Article;
  otherArticles: Article[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getArticle({}, 'big-format')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if(res.items) {
          this.firstArticle = res.items[0];
          this.otherArticles = res.items.slice(1, res.items.length)
        }
      });
  }
}

import { isPlatformBrowser } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectionStrategy, Component, OnDestroy, AfterViewInit, Inject, PLATFORM_ID, ChangeDetectorRef } from '@angular/core';
import { environment } from '~environments/environment';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { Weather } from '../../models/weather/weather.model';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherComponent implements AfterViewInit, OnDestroy {
  weatherURL = `http://api.weatherstack.com/current?access_key=${environment.weatherStackKey}`;
  weatherIcon!: string | null | undefined;
  weatherTemperature: number | null | undefined;
  weatherLocation!: string | null | undefined;

  private unsubscribe$ = new Subject<void>();
  constructor(public http: HttpClient, @Inject(PLATFORM_ID) private platformId: Object, private ref: ChangeDetectorRef) {}

  ngAfterViewInit(): void {
    this.loadIP();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  loadIP() {
    if (isPlatformBrowser(this.platformId)) {
      this.http.get('https://ipv4.jsonip.com/').pipe(
        takeUntil(this.unsubscribe$),
        switchMap((value: any) => {
          return this.getWeatherByIP(value.ip);
        })
      ).subscribe((res: Weather) => {
        this.weatherTemperature = res?.current?.temperature || null;
        this.weatherLocation = res?.location?.name || null;
        this.weatherIcon = res?.current?.weather_icons[0] || null;
        this.ref.detectChanges();
      });
    }
  }

  getWeatherByIP(ip: string) {
    return this.http.get(`${this.weatherURL}&query=${ip}`).pipe(takeUntil(this.unsubscribe$));
  }

  getWeather() {
    this.http.get(`${this.weatherURL}&query=fetch:ip&language=fr`).pipe(takeUntil(this.unsubscribe$)).subscribe((res: Weather) => {
      this.weatherTemperature = res?.current?.temperature || null;
      this.weatherLocation = res?.location?.name || null;
      this.weatherIcon = res?.current?.weather_icons[0] || null;
      this.ref.detectChanges();
    });
  }
}

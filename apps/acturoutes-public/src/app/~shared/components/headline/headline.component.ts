import {
  Component,
  OnInit,
  OnDestroy,
  NgZone,
  ChangeDetectorRef,
} from '@angular/core';
import { Article } from '~shared/models/article/article.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { reorderArray } from '../../utils/array-reorder';

@Component({
  selector: 'app-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.scss'],
})
export class HeadlineComponent implements OnInit, OnDestroy {
  articles: Article[];
  firstArticle: Article;
  secondArticle: Article;
  thirdArticle: Article;
  isFirstArticleVisible = true;
  rootURL = environment.serverAssetUrl || 'assets/images/';
  private unsubscribe$ = new Subject<void>();
  constructor(
    private api: SharedObjectService,
    private ngZone: NgZone,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getArticle({}, 'headline')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.items) {
          this.articles = res.items;
          this.setArticles();
          this.ngZone.runOutsideAngular(() => {
            setInterval(() => {
              this.articles = reorderArray(this.articles);
              this.setArticles();
              this.ref.detectChanges();
            }, 120000);
          });
        }
      });
  }

  setArticles() {
    this.firstArticle = this.articles[0];
    this.secondArticle = this.articles[1];
    this.thirdArticle = this.articles[2];
  }

  switchMainContentVisibility(visible: boolean) {
    this.isFirstArticleVisible = !visible;
  }
}

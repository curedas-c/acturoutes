import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActuTVComponent } from './actu-tv.component';

describe('ActuTVComponent', () => {
  let component: ActuTVComponent;
  let fixture: ComponentFixture<ActuTVComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActuTVComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuTVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActuTV } from '../../models/actu-tv/actu-tv.model';

@Component({
  selector: 'app-actu-tv',
  templateUrl: './actu-tv.component.html',
  styleUrls: ['./actu-tv.component.scss'],
})
export class ActuTVComponent implements OnInit, OnDestroy {
  actuTv: ActuTV;
  rootURL = environment.serverAssetUrl || 'assets/images/';
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getActuTv()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res) {
          this.actuTv = res;
        }
      });
  }
}

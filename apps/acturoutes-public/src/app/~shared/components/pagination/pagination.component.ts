import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PaginationComponent {
  @Input() activePage = 1;
  @Input() totalPage = 5;
  @Output() onPageChange: EventEmitter<number> = new EventEmitter();

  decrease() {
    if(this.activePage > 1) {
      this.activePage -= 1;
      this.onPageChange.emit(this.activePage);
    }
  }

  increase() {
    if(this.activePage < this.totalPage) {
      this.activePage += 1;
      this.onPageChange.emit(this.activePage);
    }
  }

  get canDecrease () {
    return this.activePage > 1;
  }
  get canIncrease () {
    return this.activePage < this.totalPage;
  }
}

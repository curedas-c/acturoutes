import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Subject } from 'rxjs';
import { AuthService } from '~core/services/auth.service';
import { CurrentStateService } from '~core/services/current-state.service';
import { filter, takeUntil } from 'rxjs/operators';
import { animate, style, transition, trigger } from '@angular/animations';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('slideDownUp', [
      transition(':enter', [
        style({ opacity: 0, transform: 'translateY(-20vh) scale(1)' }),
        animate('600ms ease'),
      ]),
      transition(':leave', [
        animate(
          500,
          style({ opacity: 0, transform: 'translateY(-50vh) scale(0)' })
        ),
      ]),
    ]),
  ],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @HostListener('window:scroll', ['$event.target'])
  scroll(e) {
    let scroll = e.scrollingElement.scrollTop;
    if (scroll < 10) {
      this.scrollTopReached = true;
    } else if (this.scrollTopReached === true) {
      this.scrollTopReached = false;
    }
  }

  scrollTopReached = true;
  showAd = false;
  menuOpen = false;
  userConnected: boolean;

  arbLink = 'https://www.arb-tbn.org';
  ramLink = 'https://www.lesramesabidjan.com';
  imageBankLink = 'https://www.flickr.com/photos/201636178@N03/with/54074800439/';
  private unsubscribe$ = new Subject<void>();

  constructor(private auth: AuthService, private state: CurrentStateService, private router: Router) {
    this.state.userConnected$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.userConnected = res;
      });
  }

  ngOnInit(): void {
    this.router.events
      .pipe(
        takeUntil(this.unsubscribe$),
        filter((event) => event instanceof NavigationEnd)
      )
      .subscribe((ev: NavigationEnd) => {
        const isListRout = ev.url.lastIndexOf('/articles') === 0 && ev.url.lastIndexOf('/articles/') !== 0;
        if (isListRout) {
          this.showAd = true;
        } else {
          this.showAd = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  swithMenu() {
    this.menuOpen = !this.menuOpen;
  }

  logout() {
    this.auth.logout().subscribe((res) => {});
  }
}

import { Component, Input, OnChanges, OnInit, SimpleChanges, NgZone, ChangeDetectorRef } from '@angular/core';
import { Ad } from '../../models/ads/ads.model';
import { inOutAnimation } from '../../animations/animation';
import { environment } from '~environments/environment';

@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.scss'],
  animations: [inOutAnimation]
})
export class AdsComponent implements OnInit, OnChanges {

  @Input() isSquare: boolean = false;
  @Input() imageUrl: any[] = [];
  currentImage: Ad | undefined = undefined;
  rootURL = environment.serverAssetUrl || 'assets/images/';
  constructor(private ngZone: NgZone, private ref: ChangeDetectorRef) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const arrayLength = changes.imageUrl.currentValue?.length;
    if (arrayLength > 0) {
      let index = 1;
      this.currentImage = this.imageUrl[0];
      if (arrayLength > 1) {
        this.ngZone.runOutsideAngular(() => {
          setInterval(() => {
            this.currentImage = this.imageUrl[index];
            this.ref.detectChanges();
            index === arrayLength - 1 ? index = 0 : index += 1;
          }, 3000);
        });
      }
      }
  }

  get isMultiple() {
    return this.imageUrl?.length > 1;
  }

}

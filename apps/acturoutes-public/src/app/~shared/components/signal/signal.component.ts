import {
  Component,
  NgZone,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { interval, Subject, Subscription } from 'rxjs';
import {
  map,
  takeUntil,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { inOutAnimation, slideFromRight } from '../../animations/animation';
import { GlobalFacade } from '~store/facades/global.facade';

@Component({
  selector: 'app-signal',
  templateUrl: './signal.component.html',
  styleUrls: ['./signal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [inOutAnimation, slideFromRight],
})
export class SignalComponent implements OnInit, OnDestroy {
  contentMessage: string[] = [];
  currentIndex = 0;
  zoneSubscription: Subscription | undefined;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private ngZone: NgZone,
    private ref: ChangeDetectorRef,
    private api: SharedObjectService,
    private globalFacade: GlobalFacade
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    const isRdcPage$ = this.globalFacade.routeData$.pipe(
      map((res) => res?.currentUrl?.includes('/acturoutes-rdc')),
      distinctUntilChanged()
    );

    isRdcPage$
      .pipe(
        takeUntil(this.unsubscribe$),
        switchMap((isRdcPage) =>
          isRdcPage
            ? this.api.getSignalMessage({
                filterOn: 'isRdcMessage',
                filterOnValue: true,
              })
            : this.api.getSignalMessage({
                filterOn: 'isRdcMessage',
                filterOnValue: false,
              })
        )
      )
      .subscribe((res) => {
        this.contentMessage = [];
        this.currentIndex = 0;
        this.zoneSubscription?.unsubscribe();
        if (res?.length > 0) {
          this.contentMessage = res;
          this.currentIndex = this.contentMessage.length > 1 ? 1 : 0;
          this.ref.detectChanges();
          this.ngZone.runOutsideAngular(() => {
            this.zoneSubscription = interval(5000)
              .pipe(takeUntil(this.unsubscribe$))
              .subscribe(() => {
                if (!this.contentMessage.length) {
                  return;
                }
                const maxIndex = this.contentMessage.length - 1;
                if (this.currentIndex === maxIndex) {
                  this.currentIndex = 0;
                } else {
                  this.currentIndex += 1;
                }
                this.ref.detectChanges();
              });
          });
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

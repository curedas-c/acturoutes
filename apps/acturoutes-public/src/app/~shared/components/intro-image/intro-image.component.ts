import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { distinctUntilChanged, map, Subject, switchMap, tap } from 'rxjs';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { inOutAnimation } from '~shared/animations/animation';
import dayjs from '~shared/libs/dayjs.lib';
import { getTimeElapsedFromNow } from '~shared/utils/time';
import { GlobalFacade } from '~store/facades/global.facade';

@Component({
  selector: 'intro-image',
  templateUrl: './intro-image.component.html',
  styleUrls: ['./intro-image.component.scss'],
  animations: [inOutAnimation],
})
export class IntroImageComponent implements OnInit {
  intro$ = this.api.getIntroImage();
  isClosed = false;
  rootURL = environment.serverAssetUrl || 'assets/images/';
  timeOut: NodeJS.Timeout | undefined;
  @Output() imageVisible = new EventEmitter<boolean>();
  private unsubscribe$ = new Subject<void>();
  constructor(
    private api: SharedObjectService,
    private globalFacade: GlobalFacade
  ) {}

  ngOnInit() {
    // const isRdcPage$ = this.globalFacade.routeData$.pipe(
    //   map((res) => res?.currentUrl?.includes('/acturoutes-rdc')),
    //   distinctUntilChanged()
    // );

    // this.intro$ = isRdcPage$.pipe(
    //   switchMap((isRdcPage) =>
    //     isRdcPage
    //       ? this.api.getIntroImage({
    //           filterOn: 'isRdcImage',
    //           filterOnValue: true,
    //         })
    //       : this.api.getIntroImage({
    //           filterOn: 'isRdcImage',
    //           filterOnValue: false,
    //         })
    //   ),

    this.intro$ = this.api.getIntroImage()
    .pipe(
      tap({
        next: val => {
          // if (val && this.canShowImage())
          if (val) {
            this.imageVisible.emit(true);
            this.isClosed = false;
            this.timeOut = setTimeout(() => {
              this.close();
            }, 15000);
            return;
          }
          
          this.isClosed = true;
        }
      })
    );
  }

  ngOnDestroy() {
    clearTimeout(this.timeOut);
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  close() {
    this.isClosed = true;
    this.imageVisible.emit(false);
    // this.setLastClosedTime();
  }

  // canShowImage() {
  //   const lastTimeClosed = sessionStorage.getItem('lastTimeClosedIntro');
  //   return lastTimeClosed ? getTimeElapsedFromNow(lastTimeClosed) > 900 : true;
  // }

  // setLastClosedTime() {
  //   sessionStorage.setItem('lastTimeClosedIntro', dayjs().toISOString());
  // }
}

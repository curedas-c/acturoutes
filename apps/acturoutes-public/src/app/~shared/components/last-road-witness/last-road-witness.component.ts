import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RoadWitness } from '../../models/road-witness/road-witness.model';

@Component({
  selector: 'app-last-road-witness',
  templateUrl: './last-road-witness.component.html',
  styleUrls: ['./last-road-witness.component.scss'],
})
export class LastRoadWitnessComponent implements OnInit, OnDestroy {
  article: RoadWitness;
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    limit: 1,
  };
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getRoadWitness(this.params)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.article = res.items[0];
      });
  }
}

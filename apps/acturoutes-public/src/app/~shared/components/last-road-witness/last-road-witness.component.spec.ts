import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LastRoadWitnessComponent } from './last-road-witness.component';

describe('LastRoadWitnessComponent', () => {
  let component: LastRoadWitnessComponent;
  let fixture: ComponentFixture<LastRoadWitnessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LastRoadWitnessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LastRoadWitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

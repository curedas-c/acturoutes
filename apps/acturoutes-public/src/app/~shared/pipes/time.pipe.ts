import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'timeTrans'})
export class TimeTransPipe implements PipeTransform {
  transform(value: string | Date): string | Date {
    if (value && value.constructor === String && Date.parse(value)) {
        const parsedDate = new Date(value).toLocaleDateString(undefined, {
            weekday: 'short',
            hour: '2-digit',
            minute: '2-digit'
          });

          return parsedDate;
      }
      else if (value.constructor === Date) {
          return value.toLocaleString(undefined, {
            weekday: 'short',
            hour: '2-digit',
            minute: '2-digit'
          });
      }
    return value;
  }
}
import { Ad } from "~shared/models/ads/ads.model";
import { environment } from '~environments/environment';

function setCorrectURI (ads: Ad[]): Ad[] {
    ads.map(el => {
        el.image = `${environment.serverAssetUrl}/${el.image}`;
        return el;
    });
    return ads;
}

export function divideAds(ads: Ad[]): {first: Ad[], second: Ad[]} {
    const adsLength = ads.length;
    if (adsLength > 1) {
        let firstList;
        let secondList;
        if (adsLength % 2 === 0) {
            const middle = adsLength / 2;
            firstList = ads.slice(0, middle);
            secondList = ads.slice(middle, adsLength);
        } else {
            const nearMiddle = (adsLength - 1) / 2
            firstList = ads.slice(0, nearMiddle);
            secondList = ads.slice(nearMiddle, adsLength);
        }
        return {
            first: setCorrectURI(firstList),
            second: setCorrectURI(secondList)
        }
    }

    return {
        first: setCorrectURI(ads),
        second: []
    }
}
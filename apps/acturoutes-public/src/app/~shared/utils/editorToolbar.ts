export const editorToolbar = 
`
<span class="ql-formats">
<select class="ql-align" title="'Alignement'">
  <option selected>Aucun</option>
  <option value="center">Centre</option>
  <option value="right">Droit</option>
  <option value="justify">Gauche</option>
</select>
<button class="ql-indent" value="-1" title="Diminuer le retrait"></button>
<button class="ql-indent" value="+1" title="Augmenter le retrait"></button>
</span>

<span class="ql-formats">
<select class="ql-color" title="Couleur de Police"></select>
</span>

<span class="ql-formats">
<button class="ql-bold"></button>
<button class="ql-italic"></button>
</span>

<span class="ql-formats">
<button class="ql-list" value="ordered"></button>
<button class="ql-list" value="bullet"></button>
</span>

<span class="ql-formats">
<button class="ql-clean" title="Effacer Tous les styles"></button>
</span>
</div>
`;
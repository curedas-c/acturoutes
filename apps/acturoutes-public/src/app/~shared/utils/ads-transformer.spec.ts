import { divideAds } from './ads-transformer';

describe('Ads transformer Test', () => {
  it('should return entry array as first response', () => {
    const ads = [
      {
        image: 'assets/images/c1.jpg',
        type: 'square',
        link: 'https://youtube.com',
      }
    ];
    const result = divideAds(ads);
    expect(result.first.length).toEqual(1);
    expect(result.second.length).toEqual(0);
  });

  it('should return same sized arrays', () => {
    const ads = [
      {
        image: '1',
        type: 'square',
        link: 'https://youtube.com',
      },
      {
        image: '2',
        type: 'square',
        link: '',
      },
      {
        image: '3',
        type: 'square',
        link: '',
      },
      {
        image: '4',
        type: 'square',
        link: '',
      },
    ];
    const result = divideAds(ads);
    expect(result.first.length).toEqual(2);
    expect(result.second.length).toEqual(2);
  });

  it('should return different sized arrays', () => {
    const ads = [
      {
        image: 'assets/images/c1.jpg',
        type: 'square',
        link: 'https://youtube.com',
      },
      {
        image: 'assets/images/c2.jpg',
        type: 'square',
        link: '',
      },
      {
        image: 'assets/images/c2.jpg',
        type: 'square',
        link: '',
      },
      {
        image: 'assets/images/c2.jpg',
        type: 'square',
        link: '',
      },
      {
        image: 'assets/images/c2.jpg',
        type: 'square',
        link: '',
      },
    ];
    const result = divideAds(ads);
    expect(result.first.length).toEqual(2);
    expect(result.second.length).toEqual(3);
  });
});

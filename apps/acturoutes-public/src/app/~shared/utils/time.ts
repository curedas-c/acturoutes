import dayjs from '~shared/libs/dayjs.lib';

export const getTimeElapsedFromNow = (date: dayjs.ConfigType) => {
  return dayjs(dayjs()).diff(date, 'second');
};

import { CurrentStateService } from '../../~core/services/current-state.service';
export function reorderArray(entryArray: any[]): any[] {
    const entryArrayLength = entryArray.length;
    let res = new Array(entryArrayLength);
    if (res.length < 2) {
        return entryArray;
    }

    res = [
        entryArray[entryArrayLength - 1],
        entryArray[0]
    ];
    if (entryArrayLength > 2) {
        res.push(...entryArray.slice(1, entryArrayLength - 1));
    }
    return res;
}
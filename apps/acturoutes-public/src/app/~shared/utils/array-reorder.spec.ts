import { reorderArray } from './array-reorder';

describe('Array Reorder function', () => {
  it('should return an array of same length than the entry', () => {
    const entry = [1, 2 , 3, 4];
    const result = reorderArray(entry);
    expect(result.length).toEqual(entry.length);
  });

  it('should return the same entry array if it contains less than 2 elements', () => {
    const entry = [];
    const secondEntry = [1];

    const result = reorderArray(entry);
    const secondResult = reorderArray(secondEntry);

    expect(result).toEqual(entry);
    expect(secondResult).toEqual(secondEntry);
  });
  

  it('first returned array value should be equal to last entry array value', () => {
    const entry = [15, 26 , 37, 10];
    const result = reorderArray(entry);
    expect(result[0]).toEqual(10);
  });

  it('second returned array value should be equal to first entry array value', () => {
    const entry = [15, 26 , 37, 10];
    const result = reorderArray(entry);
    expect(result[1]).toEqual(15);
  });

  it('subsequent returned array[2+] values, should be equals to entry[2+] array values', () => {
    const entry = [15, 26 , 37, 10, 15, 22, 13];
    const result = reorderArray(entry);
    expect(result).toEqual([13, 15, 26 , 37, 10, 15, 22]);
  });
});

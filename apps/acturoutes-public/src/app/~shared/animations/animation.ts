import {
  animate,
  animateChild,
  animation,
  group,
  keyframes,
  query,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const inOutAnimation = trigger('inOutAnimation', [
  transition(':enter', [
    animate(
      '.8s ease-in',
      keyframes([style({ opacity: 0 }), style({ opacity: 1 })])
    ),
  ]),
  transition(':leave', [
    animate(
      '.6s ease-in-out',
      keyframes([style({ opacity: 1 }), style({ opacity: 0 })])
    ),
  ]),
]);

export const slideFromRight = trigger('slideFromRight', [
  transition(':enter', [
    animate(
      '1.2s ease-in-out',
      keyframes([
        style({
          opacity: 0,
          position: 'relative',
          left: '1rem',
        }),
        style({
          opacity: 1,
          position: 'relative',
          left: 0,
        }),
      ])
    ),
  ]),
]);

export const routeAnimation = trigger('routeAnimations', [
  transition('* => HomePage', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' }), {
      optional: true,
    }),
    group([
      query(
        ':enter',
        [
          style({ transform: 'translateX(80%)', opacity: 0 }),
          animate('0.6s ease', style({ transform: 'translateX(0%)', opacity: 1 })),
        ],
        { optional: true }
      ),
      query(
        ':leave',
        [
          style({ transform: 'translateX(0%)', opacity: 1 }),
          animate('0.6s ease', style({ transform: 'translateX(-80%)', opacity: 0 })),
        ],
        { optional: true }
      ),
    ]),
  ]),
  transition('* <=> *', [
    query(':enter, :leave', style({ position: 'fixed', width: '100%' }), {
      optional: true,
    }),
    group([
      query(
        ':enter',
        [
          style({ transform: 'translateY(25%)', opacity: 0 }),
          animate('0.6s ease', style({ transform: 'translateY(0%)', opacity: 1 })),
        ],
        { optional: true }
      ),
      query(
        ':leave',
        [
          style({ transform: 'translateY(0%)', opacity: 1 }),
          animate('0.6s ease', style({ transform: 'translateY(15%)', opacity: 0 })),
        ],
        { optional: true }
      ),
    ]),
  ]),
]);

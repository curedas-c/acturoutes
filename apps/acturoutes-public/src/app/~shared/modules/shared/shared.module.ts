import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from '../../components/footer/footer.component';
import { AdsComponent } from '../../components/ads/ads.component';
import { TrafficImageComponent } from '../../components/traffic-image/traffic-image.component';
import { BigFormatComponent } from '../../components/big-format/big-format.component';
import { RouterModule } from '@angular/router';
import { HeadlineComponent } from '../../components/headline/headline.component';
import { ActuTVComponent } from '../../components/actu-tv/actu-tv.component';
import { DateTransPipe } from '../../pipes/date.pipe';
import { TimeTransPipe } from '../../pipes/time.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HtmlPipe } from '../../pipes/html.pipe';
import { SafeIframePipe } from '../../pipes/iframe.pipe';
import { TruncateAfterPipe } from '../../pipes/truncate.pipe';
import { AdsenseModule } from 'ng2-adsense';
import { ShareButtonsConfig } from 'ngx-sharebuttons';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { ScrollService } from '~core/services/scroll.service';
import { AnimateOnScrollDirective } from '../../directives/animateOnScroll.directive';
import { LastRoadWitnessComponent } from '../../components/last-road-witness/last-road-witness.component';
import { IntroImageComponent } from '../../components/intro-image/intro-image.component';



const MODULES: any = [
  RouterModule,
  FormsModule,
  ReactiveFormsModule,
  AdsenseModule.forRoot({
    adClient: 'ca-pub-2014772714156089',
    pageLevelAds: false
  }),
  ShareButtonsModule
];

const DECLARATIONS: any = [
  BigFormatComponent,
  TrafficImageComponent,
  FooterComponent,
  AdsComponent,
  HeadlineComponent,
  ActuTVComponent,
  LastRoadWitnessComponent,
  DateTransPipe,
  TimeTransPipe,
  HtmlPipe,
  SafeIframePipe,
  TruncateAfterPipe,
  AnimateOnScrollDirective,
  IntroImageComponent
];

const PROVIDERS = [ScrollService];

@NgModule({
  declarations: [...DECLARATIONS],
  imports: [CommonModule, ...MODULES],
  providers: [...PROVIDERS],
  exports: [...MODULES, ...DECLARATIONS],
})
export class SharedModule {}

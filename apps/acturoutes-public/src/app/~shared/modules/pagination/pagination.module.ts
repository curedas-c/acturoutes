import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from '../../components/pagination/pagination.component';
import { NgxPaginationModule } from 'ngx-pagination';

const DECLARATIONS = [PaginationComponent]

@NgModule({
  declarations: [...DECLARATIONS],
  imports: [
    CommonModule, NgxPaginationModule
  ],
  exports: [...DECLARATIONS, NgxPaginationModule]
})
export class PaginationModule { }

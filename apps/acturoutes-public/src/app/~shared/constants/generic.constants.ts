import { StateToken } from "@ngxs/store";
import { GlobalStateModel } from "~shared/models/generic/response.model";

export const GLOBAL_STATE_TOKEN = new StateToken<GlobalStateModel>('global');

export interface Res<T> {
  items: T[];
  total_items: number;
  total_pages: number;
  current_page: number;
}

export type GlobalStateModel = {
  routeData: GlobalRouteData | undefined;
};

export type GlobalRouteData = {
  params: any;
  queryParams: any;
  currentUrl: string;
  currentUrlWithoutParams: string;
  previousUrl: string;
};

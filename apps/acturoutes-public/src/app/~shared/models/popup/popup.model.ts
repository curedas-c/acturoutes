export class Popup {
    visible: boolean;
    msg: string;
    colorClass: string;

  constructor(options: {
    visible: boolean;
    msg: string;
    colorClass: string;
  }) {
    this.visible = options.visible;
    this.msg = options.msg;
    this.colorClass = options.colorClass;
  }
}

export class Article {
    _id: string;
    title: string;
    subtitle: string;
    type: string;
    description: string;
    main_image: string;
    thumbnail_image: string;
    publish_date: string;
    legend: string;
    slug: string;
    content: any;
    createdAt: string;
    viewCount: number;
    isAgenda: boolean;

  constructor(options: {
    _id: string;
    title: string;
    subtitle: string;
    type: string;
    description: string;
    main_image: string;
    thumbnail_image: string;
    publish_date: string;
    legend: string;
    slug: string;
    content: any;
    createdAt: string;
    viewCount: number;
    isAgenda?: boolean;
  }) {
    this._id = options._id;
    this.title = options.title;
    this.subtitle = options.subtitle;
    this.type = options.type;
    this.slug = options.slug;
    this.content = options.content;
    this.main_image = options.main_image;
    this.description = options.description;
    this.thumbnail_image = options.thumbnail_image;
    this.publish_date = options.publish_date;
    this.legend = options.legend;
    this.createdAt = options.createdAt;
    this.viewCount = options.viewCount;
    this.isAgenda = options.isAgenda ?? false;
  }
}

export class IntroImage {
  _id: string;
  image: string;
  link: string;
  isRdcImage: boolean;
  updatedAt: string;
  createdAt: string;

  constructor(options: {
    _id: string;
    image: string;
    link: string;
    isRdcImage: boolean;
    updatedAt: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.image = options.image;
    this.link = options.link;
    this.isRdcImage = options.isRdcImage;
    this.createdAt = options.createdAt;
    this.updatedAt = options.updatedAt;
  }
}

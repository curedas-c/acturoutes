import { AdType } from './ad.enum';
export class Ad {
    _id: string;
    image: string;
    type: AdType;
    link: string;

  constructor(options: {
    _id: string;
    image: string;
    type: AdType;
    link: string;
  }) {
    this._id = options._id;
    this.image = options.image;
    this.type = options.type;
    this.link = options.link;
  }
}

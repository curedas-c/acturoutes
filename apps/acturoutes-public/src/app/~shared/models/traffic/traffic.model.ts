export class Traffic {
    _id: string;
    createdAt: string;
    message: string;

  constructor(options: {
    _id: string;
    createdAt: string;
    message: string;
  }) {
    this._id = options._id;
    this.createdAt = options.createdAt;
    this.message = options.message;
  }
}

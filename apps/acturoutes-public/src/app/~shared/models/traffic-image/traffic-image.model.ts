export class TrafficImage {
    _id: string;
    title: string;
    image: string;

  constructor(options: {
    _id: string;
    title: string;
    image: string;
  }) {
    this._id = options._id;
    this.title = options.title;
    this.image = options.image;
  }
}

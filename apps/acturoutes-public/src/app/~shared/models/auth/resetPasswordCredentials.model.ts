export class ResetPasswordCredentials {
    email: string;
    password: string;
    password_confirmation: string;

  constructor(options: {
    email: string;
    password: string;
    password_confirmation: string;
  }) {
    this.email = options.email;
    this.password = options.password;
    this.password_confirmation = options.password_confirmation;
  }
}

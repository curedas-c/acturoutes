import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorInterceptor } from '~core/interceptors/error.interceptor';
import { RequestInterceptor } from '~core/interceptors/request.interceptor';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PopupComponent } from '~shared/components/popup/popup.component';
import { CookieModule } from 'ngx-cookie';
import { HeaderComponent } from '~shared/components/header/header.component';
import { SignalComponent } from '~shared/components/signal/signal.component';
import { AdsenseModule } from 'ng2-adsense';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { WeatherComponent } from '~shared/components/weather/weather.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxsModule } from '@ngxs/store';
// import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { GlobalState } from '~store/states/global.state';
import { GlobalFacade } from '~store/facades/global.facade';
import { environment } from '~environments/environment';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareButtonsConfig } from 'ngx-sharebuttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';

const buttonConfig: ShareButtonsConfig = {
  include: ['facebook', 'twitter', 'linkedin', 'whatsapp'],
  theme: 'default',
  prop: {
    twitter: {
    icon: ['fab', 'x-twitter']
    }
  }
};

@NgModule({
  declarations: [
    AppComponent,
    PopupComponent,
    HeaderComponent,
    SignalComponent,
    WeatherComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule.withConfig({}),
    AppRoutingModule,
    HttpClientModule,
    CookieModule.withOptions(),
    AdsenseModule.forRoot({
      adClient: 'ca-pub-2014772714156089',
      pageLevelAds: false
    }),
    LoadingBarModule,
    LoadingBarRouterModule,
    FontAwesomeModule,
    NgxsModule.forRoot([GlobalState], {
      developmentMode: !environment.production
    }),
    ShareIconsModule,
    ShareButtonsModule.withConfig(buttonConfig),
    // NgxsReduxDevtoolsPluginModule.forRoot(),
  ],
  providers: [
    GlobalFacade,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

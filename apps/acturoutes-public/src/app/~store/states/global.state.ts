/* eslint-disable @nrwl/nx/enforce-module-boundaries */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { GLOBAL_STATE_TOKEN } from '~shared/constants/generic.constants';
import { GlobalStateModel } from '~shared/models/generic/response.model';
import { SetRouteData } from '~store/actions/global.actions';

export const DEFAULT_GLOBAL_STATE = {
  routeData: undefined,
};

@State({
  name: GLOBAL_STATE_TOKEN,
  defaults: DEFAULT_GLOBAL_STATE
})
@Injectable()
export class GlobalState {
  constructor() {}

  @Action(SetRouteData)
  setRouteData(ctx: StateContext<GlobalStateModel>, action: SetRouteData) {
    const { routeData } = ctx.getState();

    ctx.patchState({
      routeData: {
        ...routeData,
        ...action.data
      }
    });
  }

  @Selector()
  static routeData(state: GlobalStateModel) {
    return state.routeData;
  }
}

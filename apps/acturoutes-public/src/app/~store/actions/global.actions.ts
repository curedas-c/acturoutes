import { GlobalRouteData } from "~shared/models/generic/response.model";

export class SetRouteData {
  static readonly type = '[Global] Set Route Data';
  constructor(public data: GlobalRouteData) {}
}
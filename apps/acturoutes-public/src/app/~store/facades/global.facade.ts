/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { GLOBAL_STATE_TOKEN } from '~shared/constants/generic.constants';
import { GlobalRouteData } from '~shared/models/generic/response.model';
import { SetRouteData } from '~store/actions/global.actions';
import { GlobalState } from '~store/states/global.state';

@Injectable()
export class GlobalFacade {
  @Select(GlobalState.routeData) routeData$!: Observable<
    ReturnType<typeof GlobalState.routeData>
  >;

  constructor(private store: Store) {}

  // Action Dispatchers
  setRouteData(data: GlobalRouteData) {
    return this.store.dispatch(new SetRouteData(data));
  }

  // Snapshots
  getRouteData() {
    return this.store.selectSnapshot(GLOBAL_STATE_TOKEN).routeData;
  }
}

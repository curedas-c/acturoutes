import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PageService } from '~core/services/page.service';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { RoadWitness } from '~shared/models/road-witness/road-witness.model';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-road-witness-detail',
  templateUrl: './road-witness-detail.component.html',
  styleUrls: ['./road-witness-detail.component.scss']
})
export class RoadWitnessDetailComponent implements OnDestroy {
  rootURL = environment.serverAssetUrl || 'assets/images/';
  shareURL = environment.articleShareUrl;
  currentArticle: RoadWitness;
  articleID: string;
  noData = false;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private api: SharedObjectService,
    private activatedRoute: ActivatedRoute,
    private pageService: PageService

  ) {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.unsubscribe$),
        map((p) => p.slug)
      )
      .subscribe((val) => {
        this.articleID = val;
        this.getData();
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getRoadWitnessDetail(this.articleID)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res) {
          this.currentArticle = res;
        } else {
          this.noData = true;
        }
        this.pageService.updatePage({
          metatags: [
            { name: 'description', content: res.title },
            { property: 'og:url', content: this.shareURL + res.slug },
            { property: 'og:title', content: res.title },
          ]
        });
      });
  }

}

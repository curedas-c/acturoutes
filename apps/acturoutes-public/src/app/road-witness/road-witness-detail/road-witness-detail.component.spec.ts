import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadWitnessDetailComponent } from './road-witness-detail.component';

describe('RoadWitnessDetailComponent', () => {
  let component: RoadWitnessDetailComponent;
  let fixture: ComponentFixture<RoadWitnessDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadWitnessDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadWitnessDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

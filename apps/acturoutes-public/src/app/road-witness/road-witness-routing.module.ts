import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoadWitnessDetailComponent } from './road-witness-detail/road-witness-detail.component';
import { RoadWitnessListComponent } from './road-witness-list/road-witness-list.component';
import { RoadWitnessPostComponent } from './road-witness-post/road-witness-post.component';
import { RoadWitnessComponent } from './road-witness.component';

const routes: Routes = [
  {
    path: '',
    component: RoadWitnessComponent,
    children: [
      {
        path: '',
        component: RoadWitnessListComponent
      },
      {
        path: 'temoigner',
        component: RoadWitnessPostComponent
      },
      {
        path: ':slug',
        component: RoadWitnessDetailComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoadWitnessRoutingModule { }

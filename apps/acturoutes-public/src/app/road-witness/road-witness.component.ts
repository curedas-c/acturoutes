import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { Ad } from '~shared/models/ads/ads.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-road-witness',
  templateUrl: './road-witness.component.html',
  styleUrls: ['./road-witness.component.scss'],
})
export class RoadWitnessComponent implements OnInit, OnDestroy {
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getAds() {
    this.api
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }
}

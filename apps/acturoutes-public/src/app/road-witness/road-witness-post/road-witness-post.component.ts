import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedObjectService } from '~core/services/shared-object.service';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { ENDPOINTS } from '~shared/constants/endpoints.constant';
import { DomSanitizer } from '@angular/platform-browser';
import { editorToolbar } from '~shared/utils/editorToolbar';
import { environment } from '~environments/environment.prod';

@Component({
  selector: 'app-road-witness-post',
  templateUrl: './road-witness-post.component.html',
  styleUrls: ['./road-witness-post.component.scss'],
})
export class RoadWitnessPostComponent implements OnInit, OnDestroy {
  defaultForm: FormGroup = new FormGroup({});
  isButtonDisabled = false;
  editorToolbar: any;
  recaptchaSiteKey = environment.recaptchaSiteKey;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private fb: FormBuilder,
    private api: SharedObjectService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
    this.editorToolbar = this.sanitizer.bypassSecurityTrustHtml(editorToolbar);
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      title: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(97),
        ],
      ],
      image: [null, [Validators.required]],
      content: ['', [Validators.required, Validators.minLength(5)]],
      author_name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      recaptcha: [null, Validators.required],
    });
  }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      this.defaultForm.patchValue({
        image: new Blob([event.target.files[0]], {
          type: 'application/octet-stream',
        }),
      });
    }
  }

  post() {
    this.switchButtonState();
    this.api
      .postItem<any>({ item: this.defaultForm, route: ENDPOINTS.roadWitness })
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
        })
      )
      .subscribe((res) => {
        if (res?.slug) {
          this.router.navigate(['/temoin-de-la-route', res.slug]);
        }
      });
  }

  addBindingCreated(quill: any) {
    // console.log(quill)
    // quill.keyboard.addbinding(
    //   {
    //     key: 13, //Enter key
    //     shiftkey: true,
    //   },
    //   (range: any, context: any) => {
    //     console.log('keybinding enter + shift', range, context);
    //   }
    // );
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

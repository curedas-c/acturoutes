import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadWitnessPostComponent } from './road-witness-post.component';

describe('RoadWitnessPostComponent', () => {
  let component: RoadWitnessPostComponent;
  let fixture: ComponentFixture<RoadWitnessPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadWitnessPostComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadWitnessPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

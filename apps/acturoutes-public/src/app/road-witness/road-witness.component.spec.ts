import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadWitnessComponent } from './road-witness.component';

describe('RoadWitnessComponent', () => {
  let component: RoadWitnessComponent;
  let fixture: ComponentFixture<RoadWitnessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadWitnessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadWitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

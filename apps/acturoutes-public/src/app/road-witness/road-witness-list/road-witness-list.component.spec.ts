import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadWitnessListComponent } from './road-witness-list.component';

describe('RoadWitnessListComponent', () => {
  let component: RoadWitnessListComponent;
  let fixture: ComponentFixture<RoadWitnessListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoadWitnessListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadWitnessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { RoadWitness } from '~shared/models/road-witness/road-witness.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-road-witness-list',
  templateUrl: './road-witness-list.component.html',
  styleUrls: ['./road-witness-list.component.scss']
})
export class RoadWitnessListComponent implements OnInit, OnDestroy {
  articles:RoadWitness[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    limit: 9
  };
  currentPage = 1;
  totalPage = 1;
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.switchLoadingState();
    this.articles = null;
    this.api
      .getRoadWitness(this.params)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => {
        this.switchLoadingState();
      }))
      .subscribe((res) => {
        this.articles = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      });
  }

  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page
    };
    this.getData();
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }
}

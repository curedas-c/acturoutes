import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoadWitnessRoutingModule } from './road-witness-routing.module';
import { RoadWitnessListComponent } from './road-witness-list/road-witness-list.component';
import { RoadWitnessDetailComponent } from './road-witness-detail/road-witness-detail.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';
import { RoadWitnessComponent } from './road-witness.component';
import { RoadWitnessPostComponent } from './road-witness-post/road-witness-post.component';
import { QuillModule } from 'ngx-quill';
import { RecaptchaFormsModule, RecaptchaModule } from 'ng-recaptcha';


@NgModule({
  declarations: [
    RoadWitnessListComponent,
    RoadWitnessDetailComponent,
    RoadWitnessComponent,
    RoadWitnessPostComponent
  ],
  imports: [
    CommonModule,
    RoadWitnessRoutingModule,
    SharedModule,
    RecaptchaModule,
    RecaptchaFormsModule,
    PaginationModule,
    QuillModule
  ]
})
export class RoadWitnessModule { }

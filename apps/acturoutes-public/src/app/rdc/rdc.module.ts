import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RdcRoutingModule } from './rdc-routing.module';
import { RdcComponent } from './rdc.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';

@NgModule({
  declarations: [RdcComponent],
  imports: [CommonModule, RdcRoutingModule, SharedModule, PaginationModule],
})
export class RdcModule {}

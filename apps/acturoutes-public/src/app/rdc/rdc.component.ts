import { Component, OnInit } from '@angular/core';
import { finalize, Subject, takeUntil } from 'rxjs';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';
import { Article } from '~shared/models/article/article.model';
import { GlobalFacade } from '~store/facades/global.facade';
import { RdcService } from './shared/services/rdc.service';

@Component({
  selector: 'acturoutes-rdc',
  templateUrl: './rdc.component.html',
  styleUrls: ['./rdc.component.scss'],
})
export class RdcComponent implements OnInit {
  mainArticle: Article;
  articles: Article[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    limit: 9,
    filterOn: 'isRdcArticle',
  };
  currentPage = 1;
  totalPage = 1;
  isLoading = false;
  isMainArticleVisible = true;
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService, private rdcService: RdcService, private globalFacade: GlobalFacade) {}

  ngOnInit(): void {
    this.getData();
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.switchLoadingState();
    this.articles = null;
    this.api
      .getArticle(this.params)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => {
        this.switchLoadingState();
      }))
      .subscribe((res) => {
        const [first, ...rest] = res.items;
        this.mainArticle = first;
        this.articles = rest;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      });
  }

  getAds() {
    if (this.globalFacade.getRouteData().currentUrl !== '/acturoutes-rdc') {
      return;
    }
    this.rdcService
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }


  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page
    };
    this.getData();
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }

  switchMainContentVisibility(visible: boolean) {
    this.isMainArticleVisible = !visible;
  }
}

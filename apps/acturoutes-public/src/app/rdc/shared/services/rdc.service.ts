import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ApiService } from '~core/services/api.service';
import { ENDPOINTS } from '~shared/constants/endpoints.constant';
import { AdPositionType, AdType } from '~shared/models/ads/ad.enum';
import { Ad } from '~shared/models/ads/ads.model';

@Injectable({
  providedIn: 'root'
})
export class RdcService {

  constructor(private _apiService: ApiService) { }
  getAds(params?: any): Observable<any[]> {
    return this._apiService.get(`${ENDPOINTS.rdcAds}`, { limit: 50, ...params }).pipe(
      map((response: any) => {
        const TOP_SQUARES = response.items?.filter(el => el.type === AdType.SQUARE && el.position === AdPositionType.TOP ).map(el => {
          return new Ad(el);
        });
        const BOTTOM_SQUARES = response.items?.filter(el => el.type === AdType.SQUARE && el.position === AdPositionType.BOTTOM ).map(el => {
          return new Ad(el);
        });
        const TOP_RECTANGLES = response.items?.filter(el => el.type === AdType.RECTANGLE && el.position === AdPositionType.TOP).map(el => {
          return new Ad(el);
        });

        const BOTTOM_RECTANGLES = response.items?.filter(el => el.type === AdType.RECTANGLE && el.position === AdPositionType.BOTTOM).map(el => {
          return new Ad(el);
        });

        // [divideAds(SQUARES), divideAds(RECTANGLES)]
        return [{
          first: TOP_SQUARES,
          second: BOTTOM_SQUARES
        },
        {
          first: TOP_RECTANGLES,
          second: BOTTOM_RECTANGLES
        }];
      })
    );
  }
}

import { TestBed } from '@angular/core/testing';

import { RdcService } from './rdc.service';

describe('RdcService', () => {
  let service: RdcService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RdcService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

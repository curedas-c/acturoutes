import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RdcComponent } from './rdc.component';

const routes: Routes = [
  {
    path: '',
    component: RdcComponent
  },
  {
    path: ':slug',
    loadChildren: () => import('../article-detail/article-detail.module').then(m => m.ArticleDetailModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RdcRoutingModule { }

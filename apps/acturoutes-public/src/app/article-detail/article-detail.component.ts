import { Component } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { PageService } from '~core/services/page.service';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';
import { Article } from '~shared/models/article/article.model';
import { Subject } from 'rxjs';
import { distinctUntilChanged, map, switchMap, takeUntil } from 'rxjs/operators';
import { RdcService } from '../rdc/shared/services/rdc.service';
import { GlobalFacade } from '~store/facades/global.facade';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss'],
})
export class ArticleDetailComponent {
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  // shareURL = environment.articleShareUrl;
  currentArticle: Article;
  articleID: string;
  noData = false;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private api: SharedObjectService,
    private activatedRoute: ActivatedRoute,
    private pageService: PageService,
    private rdcService: RdcService,
    private globalFacade: GlobalFacade,

  ) {
    this.activatedRoute.params
      .pipe(
        takeUntil(this.unsubscribe$),
        map((p) => p.slug)
      )
      .subscribe((val) => {
        this.articleID = val;
        this.getData();
        this.getAds();
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.api
      .getArticleDetail(this.articleID)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res) {
          this.currentArticle = res;
        } else {
          this.noData = true;
        }
        this.pageService.updatePage({
          metatags: [
            { name: 'description', content: res.description },
            { property: 'og:url', content: this.shareUrl + res.slug },
            { property: 'og:title', content: res.title },
          ]
        });
      });
  }

  getAds() {
    const request$ = this.globalFacade.routeData$.pipe(
      map((res) => res?.currentUrl?.includes('/acturoutes-rdc')),
      distinctUntilChanged(),
      switchMap(isRdcPage => isRdcPage ? this.rdcService.getAds() : this.api.getAds())
    );

    request$
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((res) => {
        this.squareListOne = res[0].first || [];
        this.squareListTwo = res[0].second  || [];
        this.rectangleListOne = res[1].first  || [];
        this.rectangleListTwo = res[1].second  || [];
    });
  }

  get shareUrl() {
    const url = `${window.location.hostname}${this.globalFacade.getRouteData()?.currentUrlWithoutParams}/${this.currentArticle?.slug || ''}`;
    return encodeURIComponent(url);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleDetailRoutingModule } from './article-detail-routing.module';
import { ArticleDetailComponent } from './article-detail.component';
import { SharedModule } from '~shared/modules/shared/shared.module';


@NgModule({
  declarations: [
    ArticleDetailComponent
  ],
  imports: [
    CommonModule,
    ArticleDetailRoutingModule,
    SharedModule
  ]
})
export class ArticleDetailModule { }

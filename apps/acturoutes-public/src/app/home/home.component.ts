import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ad } from '~shared/models/ads/ads.model';
import { SharedObjectService } from '../~core/services/shared-object.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  private unsubscribe$ = new Subject<void>();
  constructor(private sharedObject: SharedObjectService) { }

  ngOnInit(): void {
    this.sharedObject.getAds().pipe(takeUntil(this.unsubscribe$)).subscribe(res => {
      this.squareListOne = res[0].first;
      this.squareListTwo = res[0].second;
      this.rectangleListOne = res[1].first;
      this.rectangleListTwo = res[1].second;
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}

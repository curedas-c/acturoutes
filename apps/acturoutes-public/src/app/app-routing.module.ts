import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '~core/guards/auth.guard';

const routes: Routes = [
  {
    path: 'accueil',
    loadChildren: () => import('./home/home.module').then(m => m.HomeModule),
    data: { animation: "HomePage" }
  },
  {
    path: '404',
    loadChildren: () => import('./error-page/error-page.module').then(m => m.ErrorPageModule),
    data: { animation: "404Page" }
  },
  {
    path: 'articles',
    loadChildren: () => import('./article-list/article-list.module').then(m => m.ArticleListModule),
    data: { animation: "ArticlePage" }
  },
  {
    path: 'agenda',
    loadChildren: () => import('./article-list/article-list.module').then(m => m.ArticleListModule),
    data: { animation: "ArticlePage" }
  },
  {
    path: 'news',
    loadChildren: () => import('./news/news.module').then(m => m.NewsModule),
    data: { animation: "NewsPage" }
  },
  {
    path: 'articles/:slug',
    loadChildren: () => import('./article-detail/article-detail.module').then(m => m.ArticleDetailModule),
    data: { animation: "ArticleDetailPage" }
  },{
    path: 'agenda/:slug',
    loadChildren: () => import('./article-detail/article-detail.module').then(m => m.ArticleDetailModule),
    data: { animation: "ArticleDetailPage" }
  },
  {
    path: 'direct-trafic',
    canActivate: [AuthGuard],
    loadChildren: () => import('./direct-traffic/direct-traffic.module').then(m => m.DirectTrafficModule),
    data: { animation: "TrafficPage" }
  },
  {
    path: 'edition-speciale',
    loadChildren: () => import('./special-edition/special-edition.module').then(m => m.SpecialEditionModule),
    data: { animation: "SpecialPage" }
  },
  {
    path: 'recherche',
    loadChildren: () => import('./search/search.module').then(m => m.SearchModule),
    data: { animation: "SearchPage" }
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    data: { animation: "AuthPage" }
  },
  {
    path: 'temoin-de-la-route',
    loadChildren: () => import('./road-witness/road-witness.module').then(m => m.RoadWitnessModule),
    data: { animation: "RoadWitnessPage" }
  },
  {
    path: 'acturoutes-rdc',
    loadChildren: () => import('./rdc/rdc.module').then(m => m.RdcModule),
    data: { animation: "RdcPage" }
  },
  // {
  //   path: 'accueil',
  //   loadChildren: () => import('./construction/construction.module').then(m => m.ConstructionModule)
  // },
  // {
  //   path: '',
  //   pathMatch: 'full',
  //   redirectTo: 'accueil'
  // },
  // {
  //   path: '**',
  //   redirectTo: 'accueil'
  // },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'accueil'
  },
  {
    path: '**',
    redirectTo: '404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabledNonBlocking',
    onSameUrlNavigation: 'reload',
    scrollPositionRestoration: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

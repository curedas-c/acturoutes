import { Component, OnDestroy, OnInit } from '@angular/core';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';
import { Article } from '~shared/models/article/article.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})
export class ArticleListComponent implements OnInit, OnDestroy {
  articles: Article[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    limit: 9,
  };
  currentPage = 1;
  totalPage = 1;
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService, private router: Router) {}

  ngOnInit(): void {
    this.getData();
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.switchLoadingState();
    const isAgendaPage = this.router.url.startsWith('/agenda');
    this.articles = null;
    this.api
      .getArticle(
        isAgendaPage
          ? { ...this.params, filterOn: 'isAgenda' }
          : { ...this.params, filters: { isAgenda: { $ne: true } } }
      )
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchLoadingState();
        })
      )
      .subscribe((res) => {
        this.articles = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      });
  }

  getAds() {
    this.api
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }

  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page,
    };
    this.getData();
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleListRoutingModule } from './article-list-routing.module';
import { ArticleListComponent } from './article-list.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';


@NgModule({
  declarations: [
    ArticleListComponent
  ],
  imports: [
    CommonModule,
    ArticleListRoutingModule,
    SharedModule,
    PaginationModule,
  ]
})
export class ArticleListModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SpecialEditionComponent } from './special-edition.component';

const routes: Routes = [
  {
    path: '',
    component: SpecialEditionComponent
  },
  {
    path: ':slug',
    loadChildren: () => import('../article-detail/article-detail.module').then(m => m.ArticleDetailModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SpecialEditionRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SpecialEditionRoutingModule } from './special-edition-routing.module';
import { SpecialEditionComponent } from './special-edition.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';


@NgModule({
  declarations: [
    SpecialEditionComponent
  ],
  imports: [
    CommonModule,
    SpecialEditionRoutingModule,
    PaginationModule,
    SharedModule
  ]
})
export class SpecialEditionModule { }

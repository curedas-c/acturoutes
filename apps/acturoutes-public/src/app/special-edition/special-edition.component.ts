import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { SharedObjectService } from '~core/services/shared-object.service';
import { Article } from '~shared/models/article/article.model';
import { finalize, takeUntil } from 'rxjs/operators';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';

@Component({
  selector: 'app-special-edition',
  templateUrl: './special-edition.component.html',
  styleUrls: ['./special-edition.component.scss'],
})
export class SpecialEditionComponent implements OnInit, OnDestroy {
  articles: Article[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    filterOn: 'isEvent',
    limit: 9
  };
  currentPage = 1;
  totalPage = 1;
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  isLoading = false;
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.articles = null;
    this.switchLoadingState();
    this.api
      .getArticle(this.params)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => {
        this.switchLoadingState();
      }))
      .subscribe((res) => {
        this.articles = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      });
  }

  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page
    };
    this.getData();
  }

  getAds() {
    this.api
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

import { AuthService } from '~core/services/auth.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html'
})
export class ForgotComponent implements OnInit, OnDestroy {
  loginForm: FormGroup = new FormGroup({});
  isButtonDisabled = false;

  private unsubscribe$ = new Subject<void>();
  constructor(private fb: FormBuilder, private auth: AuthService) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  getPassword() {
    this.switchButtonState();
    this.auth
      .forgotPassword(this.loginForm.value)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
      .subscribe(
        (res) => {
          // this.snackBar.open(`Veuillez Consulter vos mails pour récuperer votre nouveau mot de passe`, 'Fermer', {
          //   horizontalPosition: this.horizontalPosition
          // });
        }
      );
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

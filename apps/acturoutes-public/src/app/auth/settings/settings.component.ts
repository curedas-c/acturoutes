import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '~core/services/auth.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  nameForm: FormGroup = new FormGroup({});
  passwordForm: FormGroup = new FormGroup({});
  isButtonDisabled = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private fb: FormBuilder,
    private auth: AuthService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.nameForm = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]]
    });
    this.passwordForm = this.fb.group({
      password: ['', [Validators.required]],
      password_rewrite: ['', [Validators.required]]
    });
  }

  updateName() {
  //   this.switchButtonState();
  //   this.auth
  //     .login(this.loginForm.value)
  //     .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
  //     .subscribe(
  //       (res) => {
  //         this.router.navigate();
  //       }
  //     );
  }

  updatePassword() {
    //   this.switchButtonState();
    //   this.auth
    //     .login(this.loginForm.value)
    //     .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
    //     .subscribe(
    //       (res) => {
    //         this.router.navigate();
    //       }
    //     );
    }

    deconnect() {}

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}


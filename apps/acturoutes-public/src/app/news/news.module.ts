import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { NewsComponent } from './news.component';
import { SharedModule } from '~shared/modules/shared/shared.module';
import { PaginationModule } from '~shared/modules/pagination/pagination.module';


@NgModule({
  declarations: [
    NewsComponent
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    SharedModule,
    PaginationModule,
  ]
})
export class NewsModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Article } from '~shared/models/article/article.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { SharedObjectService } from '~core/services/shared-object.service';
import { environment } from '~environments/environment';
import { Ad } from '~shared/models/ads/ads.model';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit, OnDestroy {

  articles: Article[];
  rootURL = environment.serverAssetUrl || 'assets/images/';
  params: any = {
    limit: 9,
    filterOn: 'isEnglishArticle',
  };
  currentPage = 1;
  totalPage = 1;
  isLoading = false;
  squareListOne: Ad[] = [];
  squareListTwo: Ad[] = [];
  rectangleListOne: Ad[] = [];
  rectangleListTwo: Ad[] = [];
  private unsubscribe$ = new Subject<void>();
  constructor(private api: SharedObjectService) {}

  ngOnInit(): void {
    this.getData();
    this.getAds();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getData() {
    this.switchLoadingState();
    this.articles = null;
    this.api
      .getArticle(this.params)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => {
        this.switchLoadingState();
      }))
      .subscribe((res) => {
        this.articles = res.items;
        this.currentPage = res.current_page;
        this.totalPage = res.total_pages;
      });
  }

  getAds() {
    this.api
      .getAds()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.squareListOne = res[0].first;
        this.squareListTwo = res[0].second;
        this.rectangleListOne = res[1].first;
        this.rectangleListTwo = res[1].second;
      });
  }


  changePage(page: number) {
    this.params = {
      ...this.params,
      page: page
    };
    this.getData();
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }

}

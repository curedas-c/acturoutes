import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CurrentStateService } from '~core/services/current-state.service';

const IgnoredEndpoints = ['direct-traffic', 'ads', 'signal', 'jsonip', 'weatherstack']
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private state: CurrentStateService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        const requestIncludeIgnored = IgnoredEndpoints.some(endpoint => {
          return request.url.indexOf(endpoint) > 0;
        })

        if (error.error && !requestIncludeIgnored) {
          if (error.status === 0) {
            this.state.setPopupState({
              visible: true,
              msg: `Une erreur s'est produite ou vous n'avez pas accès à internet`,
              colorClass: 'bg-red-500',
            });
          }
          else {
            this.state.setPopupState({
              visible: true,
              msg: error.error.message || `Une erreur s'est produite`,
              colorClass: 'bg-yellow-500',
            });
          }
        }

        return throwError(error);
      })
    );
  }
}

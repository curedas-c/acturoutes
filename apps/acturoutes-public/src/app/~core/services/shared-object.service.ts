import { Injectable } from '@angular/core';
import { ApiService } from '~core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Ad } from '~shared/models/ads/ads.model';
import { Article } from '~shared/models/article/article.model';
import { Res } from '~shared/models/generic/response.model';
import { ActuTV } from '~shared/models/actu-tv/actu-tv.model';
import { AdType, AdPositionType } from '~shared/models/ads/ad.enum';
import { TrafficImage } from '~shared/models/traffic-image/traffic-image.model';
import { RoadWitness } from '~shared/models/road-witness/road-witness.model';
import { FormGroup } from '@angular/forms';
import { ENDPOINTS } from '~shared/constants/endpoints.constant';
import { IntroImage } from '~shared/models/intro-image/intro-image.model';

@Injectable({
  providedIn: 'root',
})
export class SharedObjectService {
  constructor(private _apiService: ApiService) {}

  getAds(params?: any): Observable<any[]> {
    return this._apiService.get(`${ENDPOINTS.ad}`, { limit: 50, ...params }).pipe(
      map((response: any) => {
        const TOP_SQUARES = response.items?.filter(el => el.type === AdType.SQUARE && el.position === AdPositionType.TOP ).map(el => {
          return new Ad(el);
        });
        const BOTTOM_SQUARES = response.items?.filter(el => el.type === AdType.SQUARE && el.position === AdPositionType.BOTTOM ).map(el => {
          return new Ad(el);
        });
        const TOP_RECTANGLES = response.items?.filter(el => el.type === AdType.RECTANGLE && el.position === AdPositionType.TOP).map(el => {
          return new Ad(el);
        });

        const BOTTOM_RECTANGLES = response.items?.filter(el => el.type === AdType.RECTANGLE && el.position === AdPositionType.BOTTOM).map(el => {
          return new Ad(el);
        });

        // [divideAds(SQUARES), divideAds(RECTANGLES)]
        return [{
          first: TOP_SQUARES,
          second: BOTTOM_SQUARES
        },
        {
          first: TOP_RECTANGLES,
          second: BOTTOM_RECTANGLES
        }];
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getEvent(params?: any): Observable<Res<Article>> {
    return this._apiService.get(`${ENDPOINTS.event}`, params).pipe(
      map((response: any) => {
        const mapped: Article[] = response.items?.map((res: any) => {
          return new Article(res);
        });

        return {
          items: mapped,
          total_items: parseInt(response.total_items, 10),
          total_pages: parseInt(response.total_pages, 10),
          current_page: parseInt(response.current_page, 10),
        };
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getArticle(params?: any, route?: string): Observable<Res<Article>> {
    return this._apiService
      .get(`${route || ENDPOINTS.article}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Article[] = response.items?.map((res: any) => {
            return new Article(res);
          });

          return {
            items: mapped,
            total_items: parseInt(response.total_items, 10),
            total_pages: parseInt(response.total_pages, 10),
            current_page: parseInt(response.current_page, 10),
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getRoadWitness(params?: any): Observable<Res<RoadWitness>> {
    return this._apiService
      .get(`${ENDPOINTS.roadWitness}`, params)
      .pipe(
        map((response: any) => {
          const mapped: RoadWitness[] = response.items?.map((res: any) => {
            return new RoadWitness(res);
          });

          return {
            items: mapped,
            total_items: parseInt(response.total_items, 10),
            total_pages: parseInt(response.total_pages, 10),
            current_page: parseInt(response.current_page, 10),
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getArticleDetail(id: string, params?: any): Observable<Article> {
    return this._apiService.get(`${ENDPOINTS.article}/${id}`, params).pipe(
      map((response: any) => {
        if (response.data) {
          const mapped: Article = new Article(response.data);
          return mapped;
        }
        return null;
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getRoadWitnessDetail(slug: string, params?: any): Observable<RoadWitness> {
    return this._apiService.get(`${ENDPOINTS.roadWitness}/${slug}`, params).pipe(
      map((response: any) => {
        if (response.data) {
          const mapped: RoadWitness = new RoadWitness(response.data);
          return mapped;
        }
        return null;
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getActuTv(params?: any): Observable<ActuTV> {
    return this._apiService.get(`${ENDPOINTS.actuTv}`, params).pipe(
      map((response: any) => {
        return response.data ? new ActuTV(response.data) : null;
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getTrafficImage(params?: any): Observable<TrafficImage[]> {
    return this._apiService
      .get(`${ENDPOINTS.trafficImage}`, params)
      .pipe(
        map((response: any) => {
          const mapped: TrafficImage[] = response.items?.map((res: any) => {
            return new TrafficImage(res);
          });

          return mapped;
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getSignalMessage(params?: any): Observable<string[]> {
    return this._apiService.get(`${ENDPOINTS.signal}`, params).pipe(
      map((response: any) => {
        const mapped = response.items?.map(el => el.message);
        return mapped;
      }),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getIntroImage(params?: any) {
    return this._apiService.get(ENDPOINTS.introImage, params).pipe(
      map((response: any) => {
        if (response.items && !!response.items[0]?.enabled) {
          const mapped: IntroImage = new IntroImage(response.items[0]);
          return mapped;
        }
        return null;
      })
    );
  }

  postItem<T>(options: {item: FormGroup, params?: any, route: string}): Observable<T> {
    const { item, params, route } = options;
    let formData = new FormData();

    Object.keys(item.controls).forEach((key) => {
      const value = item.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    return this._apiService.post(`${route}`, formData, params).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }
}

import { Injectable } from '@angular/core';
import { ForgotPasswordCredentials } from '~shared/models/auth/forgotPasswordCredentials.model';
import { LoginCredentials } from '~shared/models/auth/loginCredentials.model';
import { ResetPasswordCredentials } from '~shared/models/auth/resetPasswordCredentials.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { CookiesService } from './cookies.service';
import { Router } from '@angular/router';
import { CurrentStateService } from './current-state.service';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  /**
   * Auth Endpoint
   */
  private authEndpoint = {
    login: 'auth/user/sign-in',
    forgotPassword: 'auth/user/password/forgot/request',
    resetPassword: 'auth/user/password/reset',
    logout: 'auth/user/password/forgot/confirm',
    confirmation: 'auth/user/confirmation-tokens',
  };

  /**
   * Auth Service constructor
   */
  constructor(
    private apiService: ApiService,
    private cookie: CookiesService,
    private router: Router,
    private state: CurrentStateService
  ) {}
  /**
   * Login
   */
  public login(credentials: LoginCredentials, params?: any): Observable<any> {
    return this.apiService
      .post(`${this.authEndpoint.login}`, credentials, params)
      .pipe(
        map((response: any) => {
          this.state.setUserState(true);
          return response;
        }),
        catchError((error) => throwError(error))
      );
  }

  /**
   * Logout
   */
  public logout(): Observable<any> {
    return this.cookie.clearCookie('credentials').pipe(
      tap(() => {
        this.router.navigate(['/auth']);
        this.state.setUserState(false);
      })
    );
  }

  /**
   * Reset Password
   */
  public forgotPassword(
    credentials: ForgotPasswordCredentials
  ): Observable<any> {
    return this.apiService
      .post(`${this.authEndpoint.forgotPassword}`, credentials)
      .pipe(
        map((response: any) => response),
        catchError((error) => throwError(error))
      );
  }

  /**
   * Change password
   */
  public resetPassword(credentials: ResetPasswordCredentials): Observable<any> {
    return this.apiService
      .post(`${this.authEndpoint.resetPassword}`, credentials)
      .pipe(
        map((response: any) => response),
        catchError((error) => throwError(error))
      );
  }

  /**
   * Confirm account
   */
  public confirmActivation(token: string): Observable<any> {
    return this.apiService
      .get(`${this.authEndpoint.confirmation}/${token}`)
      .pipe(
        map((response: any) => response),
        catchError((error) => throwError(error))
      );
  }

  /**
   * Confirm account
   */
  public createCredential(token: string, putContent: any): Observable<any> {
    return this.apiService
      .put(`${this.authEndpoint.confirmation}/${token}`, putContent)
      .pipe(
        map((response: any) => response),
        catchError((error) => throwError(error))
      );
  }
}

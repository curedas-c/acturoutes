import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, OnDestroy, PLATFORM_ID } from '@angular/core';
import { Observable, Subscription, EMPTY, fromEvent } from 'rxjs';

@Injectable()
export class ScrollService implements OnDestroy {
  scrollObs: Observable<any>;
  resizeObs: Observable<any>;
  pos: number;
  private scrollSub: Subscription = new Subscription();
  private resizeSub: Subscription = new Subscription();

  constructor(@Inject(PLATFORM_ID) private platformId: Object) {
    // set initial value
    const isBrowser = isPlatformBrowser(this.platformId);
    this.manageScrollPos();

    // create observable that we can subscribe to from component or directive
    this.scrollObs = isBrowser ?
      typeof window !== 'undefined' ? fromEvent(window, 'scroll') : EMPTY : EMPTY;

    // initiate subscription to update values
    this.scrollSub = this.scrollObs.subscribe(() => this.manageScrollPos());

    // create observable for changes in screen size
    this.resizeObs = isBrowser ?
      typeof window !== 'undefined' ? fromEvent(window, 'resize') : EMPTY: EMPTY;

    // initiate subscription to update values
    this.resizeSub = this.resizeObs.subscribe(() => this.manageScrollPos());
  }

  private manageScrollPos(): void {
    const isBrowser = isPlatformBrowser(this.platformId);
    this.pos = isBrowser ? typeof window !== 'undefined' ? window.pageYOffset : 0 : 0;
  }

  ngOnDestroy(): void {
    this.scrollSub.unsubscribe();
    this.resizeSub.unsubscribe();
  }
}

import { ApplicationRef, Injectable, NgZone } from '@angular/core';
import { Popup } from '~shared/models/popup/popup.model';
import { Observable } from 'rxjs';
import { StateService } from './state.service';

interface CurrentDataState {
  popupState: Popup;
  userConnected: boolean;
}

const initialState: CurrentDataState = {
  popupState: {
    visible: false,
    msg: '',
    colorClass: '',
  },
  userConnected: false
};

@Injectable({
  providedIn: 'root',
})
export class CurrentStateService extends StateService<CurrentDataState> {
  popup$: Observable<Popup> = this.select((state) => state.popupState);
  userConnected$: Observable<boolean> = this.select((state) => state.userConnected);

  constructor(private ngZone: NgZone, private ref: ApplicationRef) {
    super(initialState);
  }

  setPopupState(item: Popup) {
    this.setState({
      popupState: { ...item },
    });
    if (item.visible) {
      this.ngZone.runOutsideAngular(() => {
        setTimeout(() => {
          this.setState({
            popupState: { ...item, visible: false },
          });
          this.ref.tick()
        }, 5000);
      });
    }
  }

  setUserState(state: boolean) {
    this.setState({
      userConnected: state
    });
  }
}

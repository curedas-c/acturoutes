// import { NgModule } from '@angular/core';
// import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';

// import { AppModule } from './app.module';
// import { AppComponent } from './app.component';
// import { CookieBackendModule } from 'ngx-cookie-backend';
// import { NgxSsrTimeoutModule } from '@ngx-ssr/timeout';
// import { NoopAnimationsModule } from '@angular/platform-browser/animations';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';
// import { ServerStateInterceptor } from '~core/interceptors/serverstate.interceptor';

// @NgModule({
//   imports: [
//     AppModule,
//     ServerModule,
//     ServerTransferStateModule,
//     CookieBackendModule.forRoot(),
//     NoopAnimationsModule,
//     NgxSsrTimeoutModule.forRoot({ timeout: 2000 })
//   ],
//   providers: [
//     {
//       provide: HTTP_INTERCEPTORS,
//       useClass: ServerStateInterceptor,
//       multi: true
//     }
//   ],
//   bootstrap: [AppComponent],
// })
// export class AppServerModule {}

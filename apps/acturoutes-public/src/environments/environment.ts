// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  serverAssetUrl: 'http://localhost:3600/files/',
  apiUrl: 'http://localhost:3600/api/v1',
  articleShareUrl: 'https://acturoutes.info/articles/',
  weatherStackKey: '23ad2cc033ea6a4f2248210b428cf9fd',
  recaptchaSiteKey: "6LfE8SgdAAAAAO3u4I1HaMa_e2zWmRuNIzdAQ_al",
  racaptchaSecretKey: "6LfE8SgdAAAAADxD1URb-Gaos0ihpRxSyR9IOntr"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

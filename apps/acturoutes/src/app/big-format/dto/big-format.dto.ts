import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, ArrayNotEmpty, IsString } from 'class-validator';

export class BigFormatCreateDto {
  @ApiProperty()
  @ArrayNotEmpty()
  @ArrayMinSize(1)
  @IsString({ each: true })
  article: string[];
}
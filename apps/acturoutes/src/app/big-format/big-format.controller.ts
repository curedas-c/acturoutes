import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { BigFormatService } from './big-format.service';
import { BigFormatCreateDto } from './dto/big-format.dto';

@Controller('big-format')
export class BigFormatController {
  constructor(private readonly bigFormatService: BigFormatService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createBigFormatDto: BigFormatCreateDto) {
    await this.bigFormatService.create(createBigFormatDto);
    return {
      message: 'Articles ajoutés à : "Grand Format',
    };
  }

  @Get()
  findAll() {
    return this.bigFormatService.findAll();
  }
}

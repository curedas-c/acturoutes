import { Module } from '@nestjs/common';
import { BigFormatService } from './big-format.service';
import { BigFormatController } from './big-format.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BigFormatSchema } from '../@core/schemas/big-format.schema';
import { ArticleModule } from '../article/article.module';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'BigFormat',
        useFactory: () => {
          const schema = BigFormatSchema;
          return schema;
        },
      }
    ]),
    ArticleModule,
    SharedModule
  ],
  controllers: [BigFormatController],
  providers: [BigFormatService],
})
export class BigFormatModule {}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { BigFormatCreateDto } from './dto/big-format.dto';
import { ArticleService } from '../article/article.service';
import { BigFormatDocument } from '../@core/schemas/big-format.schema';

@Injectable()
export class BigFormatService {
  constructor(@InjectModel('BigFormat') private bigFormatModel: PaginateModel<BigFormatDocument>, private articleService: ArticleService) {}

  async create(createDto: BigFormatCreateDto) {
    const bigFormat = new this.bigFormatModel(createDto);
    const content = await bigFormat.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite.${err}`, HttpStatus.BAD_REQUEST);
    });

    const { article } = createDto;
    const articleUpdated = await this.articleService.updateMany(article, { isBigFormat: true });
    if (!articleUpdated) {
      await this.remove(article);
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.BAD_REQUEST);
    }

    return content;
  }

  async findAll() {
    const query = {};

    const res = await this.bigFormatModel.paginate(
      { query },
      {
        pagination: true,
        limit: 1,
        page: 1,
        sort: { createdAt: -1 },
        select: '-__v',
        populate: [
          {
            path: 'article',
            select: '-content -__v'
          },
        ],
      }
    );
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs[0]?.article
    };
  }

  async remove(ids: string[]) {
    await this.bigFormatModel.deleteMany({
      _id: ids,
    });
  }
}

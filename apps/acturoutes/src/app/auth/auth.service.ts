import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AdminDocument } from '../@core/schemas/admin.schema';
import { User } from '../@core/schemas/user.schema';
import { isMatch } from '../@shared/utils/encryption';
import { AdminService } from '../admin/admin.service';
import { AdminLoginDto } from '../admin/dto/admin.dto';
import { UserLoginDto } from '../user/dto/user.dto';
import { UserService } from '../user/user.service';


@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private adminService: AdminService,
    private jwtService: JwtService
  ) {}

  async logUser(authLoginDto: UserLoginDto) {
    const user = await this.validateUser(authLoginDto);

    const { password, ...sanitized } = user;
    return {
      access_token: this.jwtService.sign(sanitized)
    };
  }

  async logAdmin(authLoginDto: AdminLoginDto) {
    const admin = await this.validateAdmin(authLoginDto);
    const { password, ...sanitized } = admin;
    return {
      access_token: this.jwtService.sign(sanitized),
      admin_id: admin._id,
      role: admin.role,
    };
  }

  async validateUser(authLoginDto: UserLoginDto): Promise<User> {
    const { email, password } = authLoginDto;
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new UnauthorizedException(`Aucun compte n'est associé à cet adresse email`);
    }
    const passwordValid = await isMatch(password, user.password);
    if (!passwordValid) {
      throw new UnauthorizedException(`Le Mot de passe est invalide`);
    }
    return user;
  }

  async validateAdmin(authLoginDto: AdminLoginDto): Promise<AdminDocument> {
    const { email, password } = authLoginDto;
    const admin = await this.adminService.findByEmail(email);
    if (!admin) {
      throw new UnauthorizedException(`Aucun compte n'est associé à cet adresse email`);
    }
    const passwordValid = await isMatch(password, admin.password);
    if (!passwordValid) {
      throw new UnauthorizedException(`Le Mot de passe est invalide`);
    }
    return admin;
  }
}

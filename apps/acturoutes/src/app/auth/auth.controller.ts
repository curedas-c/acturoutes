import { ApiTags } from '@nestjs/swagger';
import { Controller, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AdminLoginDto } from '../admin/dto/admin.dto';
import { UserLoginDto } from '../user/dto/user.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('admin/sign-in')
  async logAdmin(@Body() authLoginDto: AdminLoginDto) {
    const res = await this.authService.logAdmin(authLoginDto);
    return {
      data: res
    };
  }

  @Post('user/sign-in')
  async logUser(@Body() authLoginDto: UserLoginDto) {
    const res = await this.authService.logUser(authLoginDto);
    return {
      data: res
    };
  }
}

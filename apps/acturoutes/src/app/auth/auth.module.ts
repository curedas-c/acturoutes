
import { Module } from '@nestjs/common';
import { UserModule } from '../user/user.module';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { AdminModule } from '../admin/admin.module';
import { SharedModule } from '../@shared/modules/shared.module';
import { JwtStrategy } from '../jwt.strategy';

@Module({
  imports: [
    UserModule,
    AdminModule,
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          secret: config.get<string>('NX_JWT_SECRET'),
          signOptions: {
            expiresIn: config.get<string | number>('NX_EXPIRES_IN'),
          },
        };
      },
    }),
    SharedModule
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
})
export class AuthModule {}

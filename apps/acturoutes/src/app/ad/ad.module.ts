import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { AdSchema } from '../@core/schemas/ad.schema';
import { SharedModule } from '../@shared/modules/shared.module';
import { AdController } from './ad.controller';
import { AdService } from './ad.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Ad',
        useFactory: () => {
          const schema = AdSchema;
          return schema;
        },
      }
    ]),
    MulterModule.register({
      limits: { fieldSize: 3 * 1024 * 1024 },
      storage: diskStorage({
        destination: '../files',
        filename: function (req, file, cb) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `${uniqueSuffix}${extname(file.originalname)}`);
        }
      })
    }),
    SharedModule
  ],
  controllers: [AdController],
  providers: [AdService]
})
export class AdModule {}

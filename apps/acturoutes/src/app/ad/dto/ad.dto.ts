import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { AdPositionType, AdType } from '../../@shared/models/ad.enum';
import {
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class AdCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(AdType)
  type: AdType;

  @ApiProperty()
  @IsNotEmpty()
  @IsEnum(AdPositionType)
  position: AdPositionType;

  @ApiProperty()
  @IsOptional()
  @IsString()
  link: string;
}

export class AdUpdateDto extends PartialType(AdCreateDto) {}

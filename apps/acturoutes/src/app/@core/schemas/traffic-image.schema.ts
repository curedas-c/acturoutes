import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TrafficImageDocument = TrafficImage & Document;

@Schema()
export class TrafficImage {
  @Prop({ required: true })
  _id: number;

  @Prop({ required: false, default: false })
  isRdcImage: boolean;

  @Prop({ required: true })
  image: string;

  @Prop({ required: false })
  title: string;
}

export const TrafficImageSchema =
  SchemaFactory.createForClass(TrafficImage);

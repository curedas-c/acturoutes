import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SignalDocument = Signal & Document;

@Schema()
export class Signal {
  @Prop({ required: true })
  message: string;

  @Prop({ required: false, default: false })
  isRdcMessage: boolean;
  
  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const SignalSchema = SchemaFactory.createForClass(Signal);

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type AdDocument = Ad & Document;

@Schema()
export class Ad {
  @Prop({ required: true })
  type: string;

  @Prop({ required: true })
  position: string;

  @Prop({ required: false })
  link: string;

  @Prop({ required: true })
  image: string;
  
  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const AdSchema = SchemaFactory.createForClass(Ad);

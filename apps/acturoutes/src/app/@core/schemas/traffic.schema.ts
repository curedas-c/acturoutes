import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TrafficDocument = Traffic & Document;

@Schema()
export class Traffic {
  @Prop({ required: true })
  message: string;
  
  @Prop({ required: false, default: false })
  isRdcMessage: boolean;
  
  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const TrafficSchema = SchemaFactory.createForClass(Traffic);

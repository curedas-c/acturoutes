import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export type BigFormatDocument = BigFormat & Document;

@Schema()
export class BigFormat {
  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Article' }],
  })
  article: mongoose.Types.ObjectId[];

  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const BigFormatSchema = SchemaFactory.createForClass(BigFormat);

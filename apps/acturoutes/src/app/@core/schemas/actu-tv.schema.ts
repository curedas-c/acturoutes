import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ActuTvDocument = ActuTv & Document;

@Schema()
export class ActuTv {
  @Prop({ default: 1 })
  _id: number;

  @Prop({ required: true })
  link: string;

  @Prop({ required: false })
  image: string;

  @Prop({ required: true })
  title: string;

  @Prop({ required: false })
  description: string;
}

export const ActuTvSchema =
  SchemaFactory.createForClass(ActuTv);

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export type ChatMessageDocument = ChatMessage & Document;

@Schema()
export class ChatMessage {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'ChatRoom' })
  room: mongoose.Types.ObjectId;

  @Prop({ required: true })
  type: string;

  @Prop({ type: Boolean, default: false })
  reply: boolean;

  @Prop({ required: true })
  text: string;

  @Prop({ required: false })
  files: string[];

  @Prop({ required: false })
  quote: string;

  @Prop({
    type: {
      name: { type: String, required: true },
      avatar: { type: String, required: false }
    }
  })
  user: { name: string, avatar: string };

  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  date: Date;
}

export const ChatMessageSchema = SchemaFactory.createForClass(ChatMessage);

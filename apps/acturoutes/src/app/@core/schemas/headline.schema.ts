import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export type HeadlineDocument = Headline & Document;

@Schema()
export class Headline {
  @Prop({
    type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Article' }],
  })
  article: mongoose.Types.ObjectId[];

  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const HeadlineSchema = SchemaFactory.createForClass(Headline);

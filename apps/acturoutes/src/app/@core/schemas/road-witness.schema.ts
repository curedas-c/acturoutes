import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export type RoadWitnessDocument = RoadWitness & Document;

@Schema()
export class RoadWitness {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  slug: string;

  @Prop({ required: true })
  image: string;

  @Prop({ required: true })
  content: string;

  @Prop({ required: false })
  author_name: string;

  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const RoadWitnessSchema = SchemaFactory.createForClass(RoadWitness);

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type IntroImageDocument = IntroImage & Document;

@Schema()
export class IntroImage {

  @Prop({ required: true })
  image: string;

  @Prop({ required: true })
  link: string;

  @Prop({ required: false, default: false })
  isRdcImage: boolean;

  @Prop({ required: false, default: true })
  enabled: boolean;
  
  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const IntroImageSchema = SchemaFactory.createForClass(IntroImage);

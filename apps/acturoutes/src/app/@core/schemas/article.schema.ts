import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { randomNumber } from '../../@shared/utils/random'

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop({ required: true })
  title: string;

  @Prop({ required: false })
  subtitle: string;

  @Prop({ required: true })
  slug: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: false })
  legend: string;

  @Prop({ required: false })
  content: string;

  @Prop({ required: false, default: randomNumber(127, 844) })
  viewCount: number;

  @Prop({ type: Boolean, default: false })
  isAgenda: boolean;

  @Prop({ type: Boolean, default: false })
  isEvent: boolean;

  @Prop({ type: Boolean, default: false })
  isHeadline: boolean;

  @Prop({ type: Boolean, default: false })
  isBigFormat: boolean;

  @Prop({ type: Boolean, default: false })
  isEnglishArticle: boolean;

  @Prop({ type: Boolean, default: false })
  isRdcArticle: boolean;

  @Prop({ required: true })
  main_image: string;
  
  @Prop({ type: Date, default: Date.now })
  updatedAt: Date;

  @Prop({ type: Date, default: Date.now })
  createdAt: Date;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);

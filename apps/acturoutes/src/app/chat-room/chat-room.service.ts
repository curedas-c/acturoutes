import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ChatRoomCreateDto, ChatRoomUpdateDto } from './dto/chat-room.dto';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { ChatMessageCreateDto } from './dto/chat-message.dto';
import { ChatRoomDocument } from '../@core/schemas/chat-room.schema';
import { ChatMessageDocument } from '../@core/schemas/chat-message.schema';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class ChatRoomService {
  constructor(
    @InjectModel('ChatRoom')
    private chatRoomModel: PaginateModel<ChatRoomDocument>,
    @InjectModel('ChatMessage')
    private chatMessageModel: PaginateModel<ChatMessageDocument>
  ) {}

  async createRoom(createDto: ChatRoomCreateDto) {
    const { name } = createDto;
    const isChatRoomExist = await this.chatRoomModel.findOne({ name });

    if (isChatRoomExist) {
      throw new HttpException(`Un canal avec le même nom existe déjà.`, HttpStatus.BAD_REQUEST);
    }

    const newChatRoom = new this.chatRoomModel(createDto);
    await newChatRoom.save().catch(() => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    return newChatRoom;
  }

  async findAllRoom(userId: any) {
    const res = await this.chatRoomModel.paginate(
      { admin: { $in: [userId] } },
      {
        pagination: false,
        sort: { createdAt: -1 },
        select: '-__v',
      }
    );
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: 1,
    };
  }

  async findOneRoom(id: string) {
    const item = await this.chatRoomModel.findById(id).select('-__v');

    if (!item) {
      throw new HttpException(`Cet canal n'existe pas ou a été supprimée`, HttpStatus.NOT_FOUND);
    }
    return item;
  }

  async updateRoom(id: string, updateDto: ChatRoomUpdateDto, file?: Express.Multer.File) {
    const room = await this.chatRoomModel.findById(id);
    const { name, admin } = updateDto;
    const adminId: any[] = [...admin];

    const res = await room.updateOne({ name, adminId, updatedAt: new Date() })
      .select('-_id -__v')
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
      });

    return res;
  }

  async remove(items: DeleteDto) {
    await this.chatRoomModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite ${err}`, HttpStatus.BAD_REQUEST);
      });

    return {
      message: 'Canal supprimé.',
      status_code: HttpStatus.OK,
    };
  }

  async getRoomMessages(id: string, adminName: string, params?: any) {
    const { page, limit } = params;
    const res = await this.chatMessageModel.paginate(
      { room: id },
      {
        pagination: true,
        limit: limit || 6,
        page: page || 1,
        sort: { date: -1 },
        select:  '-__v',
      }
    );

    // set reply to true if user is the message sender
    const messages = res.docs.map(el => {
      if (el.user.name === adminName) {
        el.reply = true;
      }
      return el;
    });

    return {
      items: messages,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  async createMessage(createDto: ChatMessageCreateDto) {
    const newChatMessage = new this.chatMessageModel(createDto);
    await newChatMessage.save().catch(() => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    return newChatMessage;
  }
}

import { Module } from '@nestjs/common';
import { ChatRoomService } from './chat-room.service';
import { ChatRoomController } from './chat-room.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ChatGateway } from './chat.gateway';
import { AdminModule } from '../admin/admin.module';
import { JwtStrategy } from '../jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { ChatMessageSchema } from '../@core/schemas/chat-message.schema';
import { ChatRoomSchema } from '../@core/schemas/chat-room.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'ChatRoom',
        useFactory: () => {
          const schema = ChatRoomSchema;
          return schema;
        },
      },
      {
        name: 'ChatMessage',
        useFactory: () => {
          const schema = ChatMessageSchema;
          return schema;
        },
      }
    ]),
    MulterModule.register({
      limits: { fieldSize: 15 * 1024 * 1024 },
      storage: diskStorage({
        destination: '../files',
        filename: function (req, file, cb) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `chat-${uniqueSuffix}${extname(file.originalname)}`);
        }
      })
    }),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          secret: config.get<string>('NX_JWT_SECRET'),
          signOptions: {
            expiresIn: config.get<string | number>('NX_EXPIRES_IN'),
          },
        };
      },
    }),
    AdminModule,
    SharedModule
  ],
  controllers: [ChatRoomController],
  providers: [ChatRoomService, ChatGateway, JwtStrategy],
  exports: [ChatRoomService]
})
export class ChatRoomModule {}

import { Controller, Get, Post, Body, Patch, Param, UseGuards, Query, Request } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ChatRoomService } from './chat-room.service';
import { ChatRoomCreateDto, ChatRoomUpdateDto } from './dto/chat-room.dto';
import { AdminService } from '../admin/admin.service';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';

@ApiTags('chat-room')
@Controller('chat-room')
export class ChatRoomController {
  constructor(
    private readonly chatRoomService: ChatRoomService,
    private admin: AdminService
  ) {}

  // @UseGuards(JwtAuthGuard)
  @Get()
  findAll(@Request() req) {
    const { user } = req;
    return this.chatRoomService.findAllRoom(user._id);
  }

  // @UseGuards(JwtAuthGuard)
  @Get(':id/messages')
  async getRoomMessages(@Param('id') id: string, @Query() query: QueryParamsDto, @Request() req) {
    const { user } = req;
    const admin = await this.admin.findOne(user._id);
    const messages = await this.chatRoomService.getRoomMessages(id, admin.name, query);
    return messages;
  }

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createDto: ChatRoomCreateDto) {
    await this.chatRoomService.createRoom(createDto);
    return {
      message: 'canal créé',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateDto: ChatRoomUpdateDto) {
    const room = await this.chatRoomService.updateRoom(id, updateDto);
    return {
      message: 'canal mis à jour',
      data: room,
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  remove(@Body() deleteDto: DeleteDto) {
    return this.chatRoomService.remove(deleteDto);
  }
}

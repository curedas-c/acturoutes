import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ChatRoomService } from './chat-room.service';
import { AdminService } from '../admin/admin.service';
import { JwtService } from '@nestjs/jwt';
import { AdminDocument } from '../@core/schemas/admin.schema';

@WebSocketGateway({ cors: true })
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {
  connectedAdminList = new Set();
  constructor(private roomService: ChatRoomService, private admin: AdminService, private jwtService: JwtService) {}

  @WebSocketServer()
  server: Server;

  async handleConnection(@ConnectedSocket() client: Socket) {
    const bearer = client.handshake.headers.authorization.split('Bearer ')[1];
    const token = await this.jwtService.verifyAsync(bearer);
    const admin: AdminDocument = token._doc;
    this.connectedAdminList.add(admin);
    // Notify connected clients of current users
    this.server.emit('new_connection', this.connectedAdminList);
  }

  async handleDisconnect(@ConnectedSocket() client: Socket) {
    const bearer = client.handshake.headers.authorization.split('Bearer ')[1];
    const admin = await this.getUserFromToken(bearer);
    
    // Set last connection date to know when messages are missed
    await this.admin.update(admin._id, { lastConnectionDate: new Date() });
    this.connectedAdminList.delete(admin);
    this.server.emit('new_connection', this.connectedAdminList);
  }

  @SubscribeMessage('send_message')
  async handleEvent(@ConnectedSocket() client: Socket, @MessageBody() data: any) {
    const event = 'new_message';

    const bearer = client.handshake.headers.authorization.split('Bearer ')[1];
    const admin = await this.getUserFromToken(bearer);

    const message = data.message;
    message.room = data.room;
    message.user = {
      name: admin.name,
    };

    await this.roomService.createMessage(message);

    const payload = { message, name: admin.name, room: data.room };
    client.to(data.room).emit(event, payload);

    // update room with empty data to track missed messages
    await this.roomService.updateRoom(data.room, {});
    return { event, data: payload };
  }

  @SubscribeMessage('join_room')
  async enterRoom(client: Socket, roomId: string) {
    client.join(roomId);
    return undefined;
  }

  async getUserFromToken(bearer: string) {
    const token = await this.jwtService.verifyAsync(bearer);
    return token._doc as AdminDocument;
  }
}

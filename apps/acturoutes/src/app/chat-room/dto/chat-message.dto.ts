import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsBooleanString,
  IsNotEmpty,
  IsObject,
  IsOptional,
  IsString,
} from 'class-validator';

export class ChatMessageCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  room: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  type: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  reply: boolean;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  text: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  quote: string;

  @ApiProperty()
  @IsObject()
  user: {
    name: string;
    avatar: string;
  };
}

export class ChatMessageUpdateDto extends PartialType(ChatMessageCreateDto) {}

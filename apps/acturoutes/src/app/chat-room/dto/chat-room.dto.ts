import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  ArrayMinSize,
  ArrayNotEmpty,
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class ChatRoomCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @ArrayNotEmpty()
  @ArrayMinSize(1)
  @IsString({ each: true })
  admin: string[];
}

export class ChatRoomUpdateDto extends PartialType(ChatRoomCreateDto) {}

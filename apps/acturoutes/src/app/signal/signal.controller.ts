import { Controller, Get, Post, Body, Patch, Param, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { SignalService } from './signal.service';
import { SignalCreateDto, SignalUpdateDto } from './dto/signal.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Controller('signal')
export class SignalController {
  constructor(private readonly signalService: SignalService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createSignalDto: SignalCreateDto) {
    const newMessage = await this.signalService.create(createSignalDto);
    return {
      data: newMessage,
      message: 'Message créé!',
    };
  }

  @Get()
  async findAll(@Query() query: QueryParamsDto) {
    const response = await this.signalService.findAll(query);
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateSignalDto: SignalUpdateDto) {
    await this.signalService.update(id, updateSignalDto);
    const response = {
      statusCode: HttpStatus.OK,
      message: 'Message mis à jour',
    };
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  async remove(@Body() deleteDto: DeleteDto) {
    return this.signalService.remove(deleteDto);
  }
}

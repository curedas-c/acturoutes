import { Module } from '@nestjs/common';
import { SignalService } from './signal.service';
import { SignalController } from './signal.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SignalSchema } from '../@core/schemas/signal.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Signal',
        useFactory: () => {
          const schema = SignalSchema;
          return schema;
        },
      }
    ]),
    SharedModule
  ],
  controllers: [SignalController],
  providers: [SignalService],
})
export class SignalModule {}

import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsBooleanString, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class SignalCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  message: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isRdcMessage: boolean;
}

export class SignalUpdateDto extends PartialType(SignalCreateDto) {}
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { SignalCreateDto, SignalUpdateDto } from './dto/signal.dto';
import { toRegex } from 'diacritic-regex';
import { SignalDocument } from '../@core/schemas/signal.schema';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class SignalService {
  constructor(@InjectModel('Signal') private signalModel: PaginateModel<SignalDocument>) {}

  async create(createDto: SignalCreateDto) {
    const { message } = createDto;
    const isExist = await this.signalModel.findOne({ message });

    if (isExist) {
      throw new HttpException(`Ce message existe déjà dans la liste.`, HttpStatus.BAD_REQUEST);
    }
    const signal = new this.signalModel(createDto);
    const newMessage = await signal.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.BAD_REQUEST);
    });

    return newMessage;
  }

  async findAll(queryParams: QueryParamsDto) {
    const { filter, order, sortBy, page, paginate, limit, fields, day, filterOn } = queryParams;
    let { filterOnValue } = queryParams;

    if (filterOn === 'isRdcMessage' && filterOnValue) {
      if (typeof filterOnValue === 'string') {
        filterOnValue = JSON.parse(filterOnValue);
      }
      filterOnValue = filterOnValue ? true : {$ne: true} as any;
    }

    const query = {
      ...(filter && {
        message: { $regex: toRegex()(filter)},
      })
    };

    const sort = order ? { [sortBy || 'createdAt']: order } : sortBy;
    const res = await this.signalModel.paginate({ ...query, ...(filterOn && { [filterOn]: filterOnValue === undefined ? true : filterOnValue }) }, {
      pagination: paginate || true,
      limit: limit || 10,
      page: page || 1,
      sort: sort || { createdAt: -1 },
      select: fields || '-__v',
    });
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
      params: { ...query, ...(filterOn && { [filterOn]: filterOnValue === undefined ? true : filterOnValue }) },
    };
  }

  async update(id: string, updateDto: SignalUpdateDto) {
    const updateOptions = {
      ...updateDto,
      updatedAt: new Date(),
    };
    const itemToUpdate = (await this.getFromParam({ _id: id }, false)) as SignalDocument;

    if (!itemToUpdate) {
      throw new HttpException(`Ce message n'existe pas ou a été supprimée.`, HttpStatus.BAD_REQUEST);
    }

    return await itemToUpdate.updateOne(updateOptions, { new: true }).catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });
  }

  async remove(items: DeleteDto) {
    await this.signalModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite lors de la suppression des éléments`,
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async getFromParam(params: any, multiple = true): Promise<SignalDocument[] | SignalDocument> {
    const res = multiple ? await this.signalModel.find(params) : await this.signalModel.findOne(params);
    return res;
  }
}

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { IntroImageService } from './intro-image.service';
import { IntroImageController } from './intro-image.controller';
import { IntroImageSchema } from '../@core/schemas/intro-image.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Intro-Image',
        useFactory: () => {
          const schema = IntroImageSchema;
          return schema;
        }
      }
    ]),
    MulterModule.register({
        limits: { fieldSize: 15 * 1024 * 1024 },
        storage: diskStorage({
          destination: '../files',
          filename: function (req, file, cb) {
            const uniqueSuffix =
              Date.now() + '-' + Math.round(Math.random() * 1e9);
            cb(null, `intro-image-${uniqueSuffix}${extname(file.originalname)}`);
          }
        })
      }),
      SharedModule
  ],
  controllers: [IntroImageController],
  providers: [IntroImageService]
})
export class IntroImageModule {}

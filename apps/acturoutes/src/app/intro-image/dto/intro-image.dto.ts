import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsBooleanString,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class IntroImageCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  link: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isRdcMessage: boolean;
}

export class IntroImageUpdateDto extends PartialType(IntroImageCreateDto) {}

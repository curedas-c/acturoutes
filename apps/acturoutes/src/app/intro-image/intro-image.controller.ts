import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Param,
  Query,
  Patch,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { IntroImageCreateDto, IntroImageUpdateDto } from './dto/intro-image.dto';
import { IntroImageService } from './intro-image.service';

@ApiTags('IntroImage')
@Controller('intro-image')
export class IntroImageController {
  constructor(private readonly introImageService: IntroImageService) {}

  @Get()
  findAll(@Query() query: QueryParamsDto) {
    return this.introImageService.getAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.introImageService.getOne(id);
  }

  // @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(
    @Body() createDto: IntroImageCreateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    const newIntro = await this.introImageService.create(createDto, file);
    return {
      data: newIntro,
      message: "Page d'intro mise à jour!",
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  update(
    @Param('id') id: string,
    @Body() updateDto: IntroImageUpdateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.introImageService.update(id, updateDto, file);
  }
}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { IntroImageDocument } from '../@core/schemas/intro-image.schema';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import {
  IntroImageCreateDto,
  IntroImageUpdateDto,
} from './dto/intro-image.dto';

@Injectable()
export class IntroImageService {
  constructor(
    @InjectModel('Intro-Image')
    private introImageModel: PaginateModel<IntroImageDocument>
  ) {}

  async create(createDto: IntroImageCreateDto, file: Express.Multer.File) {
    if (!file) {
      throw new HttpException(
        `Veuillez ajouter une image.`,
        HttpStatus.BAD_REQUEST
      );
    }

    const item = new this.introImageModel({
      ...createDto,
      ...(file && { image: file.filename }),
    });
    const newItem = await item.save().catch((err) => {
      throw new HttpException(
        `Une erreur s'est produite.`,
        HttpStatus.BAD_REQUEST
      );
    });

    return newItem;
  }

  async getAll(queryParams: QueryParamsDto) {
    const { filterOn } = queryParams;
    let { filterOnValue } = queryParams;

    if (filterOn === 'isRdcImage') {
      filterOnValue = [true , 'true'].includes(filterOnValue) ? true : {$ne: true} as any;
    }

    const res = await this.introImageModel.paginate(
      {...(filterOn && { [filterOn]: filterOnValue === undefined ? true : filterOnValue }) },
      {
        pagination: true,
        limit: 1,
        page: 1,
        sort: { createdAt: -1 },
        select: '-__v',
      }
    );
    return {
      items: res.docs,
      status_code: HttpStatus.OK,
    };
  }

  async getOne(id: number) {
    const item = await this.introImageModel.findById(id).select('-__v');
    return {
      data: item,
    };
  }

  async update(
    id: number | string,
    updateDto: IntroImageUpdateDto,
    file?: Express.Multer.File
  ) {
    const res = await this.introImageModel
      .findByIdAndUpdate(id, {
        ...updateDto,
        ...(file && { image: file.filename }),
      })
      .select('-__v')
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite${err}`,
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: `Image mise à jour.`,
      data: res,
      status_code: HttpStatus.OK,
    };
  }
}

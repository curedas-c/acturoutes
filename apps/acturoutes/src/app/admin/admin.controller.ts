import { Controller, Get, Post, Body, Patch, Param, HttpStatus, Query, UseGuards, Request } from '@nestjs/common';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { Role } from '../@shared/models/role.enum';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { AdminService } from './admin.service';
import { AdminCreateDto, AdminPasswordUpdateDTO, AdminResetDTO, AdminUpdateDto } from './dto/admin.dto';

@Controller('admin')
export class AdminController {
  constructor(private readonly adminService: AdminService) {}

  @Get('init')
  async init() {
    if (!process.env.NX_DEFAULT_USER || !process.env.NX_DEFAULT_USER_PASSWORD) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Données de compte par défaut non spécifiées.',
      };
    }

    const NX_DEFAULT_USER: AdminCreateDto = {
      name: process.env.NX_DEFAULT_USER,
      email: process.env.NX_DEFAULT_USER,
      password: process.env.NX_DEFAULT_USER_PASSWORD,
      role: Role.ADMIN,
      lastConnectionDate: null
    };

    const registeredUser = await this.adminService.findByEmail(NX_DEFAULT_USER.email);
    if (registeredUser) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Le compte par default à déjà été initialisé.',
      };
    }

    const newUser = await this.adminService.create(NX_DEFAULT_USER);
    return {
      data: newUser,
      message: 'Compte par défaut créé.',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createAdminDto: AdminCreateDto) {
    const registeredUser = await this.adminService.findByEmail(createAdminDto.email);
    if (registeredUser) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Cette adresse email existe déjà.',
      };
    }

    const newUser = await this.adminService.create(createAdminDto);
    return {
      data: newUser,
      message: 'Compte créé',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Query() query: QueryParamsDto) {
    const response = await this.adminService.findAll(query);
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Get('/details')
  async getDetails(
    @Request() req
  ) {
    const { user } = req;
    const res = await this.adminService.findOne(user._id);
    return res;
  }

  // @UseGuards(JwtAuthGuard)
  @Patch()
  async updateCurrentAdmin(@Request() req, @Body() updateAdminDto: AdminUpdateDto) {
    const { user } = req;
    await this.adminService.update(user._id, updateAdminDto);
    const response = {
      statusCode: HttpStatus.OK,
      message: 'Information mise à jour',
    };
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateAdminDto: AdminUpdateDto) {
    await this.adminService.update(id, updateAdminDto);
    const response = {
      statusCode: HttpStatus.OK,
      message: 'Compte mis à jour',
    };
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  async remove(@Body() deleteDto: DeleteDto) {
    return this.adminService.remove(deleteDto);
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/password/update')
  async updatePassWord(
    @Body() passwordDto: AdminPasswordUpdateDTO,
    @Request() req
  ) {
    const { user } = req;
    return this.adminService.updatePassword(user._id, passwordDto);
  }

  @Post('/password/reset')
  async resetPassWord(
    @Body() userDto: AdminResetDTO
  ) {
    return this.adminService.resetPassword(userDto);
  }
}

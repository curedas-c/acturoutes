import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { toRegex } from 'diacritic-regex';
import { PaginateModel } from 'mongoose';
import { AdminCreateDto, AdminPasswordUpdateDTO, AdminUpdateDto, AdminResetDTO } from './dto/admin.dto';
import { MailerService } from '@nestjs-modules/mailer';
import { AdminDocument } from '../@core/schemas/admin.schema';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { encrypt } from '../@shared/utils/encryption';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class AdminService {
  constructor(@InjectModel('Admin') private adminModel: PaginateModel<AdminDocument>, 
  private readonly mailerService: MailerService) {}

  async create(createDto: AdminCreateDto) {
    const { email } = createDto;
    const isUserExist = await this.adminModel.findOne({ email });

    if (isUserExist) {
      throw new HttpException(`Ce compte existe déjà`, HttpStatus.BAD_REQUEST);
    }

    const createdAdmin = new this.adminModel({
      ...createDto
    });

    const user = await createdAdmin.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite lors de la création du compte`, HttpStatus.BAD_REQUEST);
    });

    return this.sanitize(user);
  }

  async findAll(queryParams: QueryParamsDto) {
    const { filter, order, sortBy, page, paginate, limit, fields } = queryParams;
    const query = {
      ...(filter && {
        name: { $regex: toRegex()(filter) },
      }),
    };
    const sort = order ? { [sortBy || 'createdAt']: order } : sortBy;

    const res = await this.adminModel.paginate(query, {
      pagination: paginate || true,
      limit: limit || 10,
      page: page || 1,
      sort: sort || { createdAt: -1 },
      select: fields || '-password -__v',
    });
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  async findOne(id: string) {
    const user = await this.adminModel.findById(id).catch(() => {
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.NOT_FOUND);
    });
    if (!user) {
      throw new HttpException(`Cet admin n'existe pas`, HttpStatus.BAD_REQUEST);
    }
    
    return this.sanitize(user);
  }

  async update(id: string, updateDto: AdminUpdateDto) {
    const updateOptions = {
      ...updateDto,
      updatedAt: new Date(),
    };
    const itemToUpdate = (await this.getFromParam({ _id: id }, false)) as AdminDocument;

    if (!itemToUpdate) {
      throw new HttpException(`Ce compte n'existe pas ou a été supprimée.`, HttpStatus.BAD_REQUEST);
    }

    return await itemToUpdate.updateOne(updateOptions, { new: true }).catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });
  }

  async updatePassword(adminID: string, passwordDTO: AdminPasswordUpdateDTO) {
    const { password } = passwordDTO;

    const encryptedPassword = await encrypt(password);
    await this.adminModel
      .findByIdAndUpdate(adminID, {
        password: encryptedPassword,
      })
      .catch(() => {
        throw new HttpException(`L'utilisateur n'existe pas ou une erreur s'est produite.`, HttpStatus.NOT_FOUND);
      });

    return {
      message: 'Mot de passe Mis à jour',
      status_code: HttpStatus.OK,
    };
  }

  async resetPassword(userDto: AdminResetDTO) {
    const { email } = userDto;
    const password = 'U' + Math.round(Math.random() * 1e9);

    const user = await this.adminModel.findOne({ email }).catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    if (!user) {
      throw new HttpException(`Ce compte n'existe pas ou a été supprimé`, HttpStatus.BAD_REQUEST);
    }

    const encryptedPassword = await encrypt(password);
    await user.updateOne({ password: encryptedPassword })
      .catch(() => {
        throw new HttpException(`Une erreur s'est produite lors de la mise à jour du mot de passe.`, HttpStatus.NOT_FOUND);
      });

    await this.mailerService
      .sendMail({
        to: email,
        subject: 'Réinitialisation de compte',
        template: './user_password_reset',
        context: {
          username: user.name,
          password: password,
        },
      })
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite lors de l'envoi du mail.${err}`, HttpStatus.NOT_FOUND);
      });

    return {
      status_code: HttpStatus.OK,
    };
  }

  async findByEmail(email: string) {
    return await this.adminModel.findOne({ email });
  }

  async remove(items: DeleteDto) {
    await this.adminModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite lors de la suppression des éléments`, // TODO: remove ${err} form delete requests
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async getFromParam(params: any, multiple = true): Promise<AdminDocument[] | AdminDocument> {
    const res = multiple ? await this.adminModel.find(params) : await this.adminModel.findOne(params);
    return res;
  }

  sanitize(admin: AdminDocument) {
    const sanitized = admin.toObject();
    delete sanitized['password'];
    return sanitized;
  }
}

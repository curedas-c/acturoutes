import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { JwtModule } from '@nestjs/jwt';
import { Admin, AdminSchema } from '../@core/schemas/admin.schema';
import { encrypt } from '../@shared/utils/encryption';
import { ConfigService } from '@nestjs/config';
import { SharedModule } from '../@shared/modules/shared.module';
import { JwtStrategy } from '../jwt.strategy';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Admin',
        useFactory: () => {
          const schema = AdminSchema;
          // eslint-disable-next-line @typescript-eslint/ban-types
          schema.pre<Admin>('save', async function (next: Function) {
            const encrypted = await encrypt(this.password).catch((err) => {
              return next(err);
            });
            this.password = encrypted;
            next();
          });
          return schema;
        }
      }
    ]),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          secret: config.get<string>('NX_JWT_SECRET'),
          signOptions: {
            expiresIn: config.get<string | number>('NX_EXPIRES_IN'),
          },
        };
      },
    }),
    SharedModule
  ],
  controllers: [AdminController],
  providers: [AdminService, JwtStrategy],
  exports: [AdminService],
})
export class AdminModule {}

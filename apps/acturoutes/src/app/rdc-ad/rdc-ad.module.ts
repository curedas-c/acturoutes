import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { RdcAdSchema } from '../@core/schemas/rdc-ad.schema';
import { SharedModule } from '../@shared/modules/shared.module';
import { RdcAdController } from './rdc-ad.controller';
import { RdcAdService } from './rdc-ad.service';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'RdcAd',
        useFactory: () => {
          const schema = RdcAdSchema;
          return schema;
        },
      },
    ]),
    MulterModule.register({
      limits: { fieldSize: 3 * 1024 * 1024 },
      storage: diskStorage({
        destination: '../files',
        filename: function (req, file, cb) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `${uniqueSuffix}${extname(file.originalname)}`);
        },
      }),
    }),
    SharedModule,
  ],
  controllers: [RdcAdController],
  providers: [RdcAdService],
})
export class RdcAdModule {}

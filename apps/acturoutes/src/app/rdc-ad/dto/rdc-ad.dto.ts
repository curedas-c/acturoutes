import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { AdType } from '../../@shared/models/ad.enum';
import { IsNotEmpty, IsString } from 'class-validator';

export class RdcAdCreateDto {
  // @ApiProperty()
  // @IsNotEmpty()
  // @IsEnum(AdType)
  // type: AdType;

  // @ApiProperty()
  // @IsNotEmpty()
  // @IsEnum(AdPositionType)
  // position: AdPositionType;

  @ApiProperty()
  @IsOptional()
  @IsString()
  link: string;
}

export class RdcAdUpdateDto extends PartialType(RdcAdCreateDto) {}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseGuards,
  Query,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { RdcAdService } from './rdc-ad.service';
import { RdcAdCreateDto, RdcAdUpdateDto } from './dto/rdc-ad.dto';

@ApiTags('RdcAd')
@Controller('rdc-ad')
export class RdcAdController {
  constructor(private readonly adService: RdcAdService) {}

  @Get()
  findAll(@Query() query: QueryParamsDto) {
    return this.adService.findAll(query);
  }

  @Get('/stats')
  getStats() {
    return this.adService.getStats();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    const res = await this.adService.findOne(id);
    return {
      data: res,
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  create(
    @Body() createDto: RdcAdCreateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.adService.create(createDto, file);
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  update(
    @Param('id') id: string,
    @Body() updateDto: RdcAdUpdateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.adService.update(id, updateDto, file);
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  remove(@Body() deleteDto: DeleteDto) {
    return this.adService.remove(deleteDto);
  }
}

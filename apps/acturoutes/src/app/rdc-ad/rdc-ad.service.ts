import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { toRegex } from 'diacritic-regex';
import { RdcAdCreateDto, RdcAdUpdateDto } from './dto/rdc-ad.dto';
import { RdcAdDocument } from '../@core/schemas/rdc-ad.schema';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class RdcAdService {
  constructor(
    @InjectModel('RdcAd')
    private adModel: PaginateModel<RdcAdDocument>
  ) {}

  async create(createDto: RdcAdCreateDto, file: Express.Multer.File) {
    if (!file) {
      throw new HttpException(
        `Veuillez ajouter une image.`,
        HttpStatus.BAD_REQUEST
      );
    }

    const newAd = new this.adModel({ ...createDto, image: file.filename });
    await newAd.save().catch(() => {
      throw new HttpException(
        `Une erreur s'est produite`,
        HttpStatus.BAD_REQUEST
      );
    });

    return {
      message: 'Image publicitaire créée.',
      data: newAd,
      status_code: HttpStatus.CREATED,
    };
  }

  async findAll(params: any) {
    const { filter, order, sortBy, page, paginate, limit, fields } = params;
    const query = {
      ...(filter && {
        link: { $regex: toRegex()(filter) },
      }),
    };
    const sort = order ? { [sortBy || 'createdAt']: order } : sortBy;

    const res = await this.adModel.paginate(query, {
      pagination: paginate || true,
      limit: limit || 10,
      page: page || 1,
      sort: sort || { createdAt: -1 },
      select: fields || '-__v',
    });

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  async findOne(id: string) {
    const item = await this.adModel.findById(id).select('-__v');

    if (!item) {
      throw new HttpException(
        `Cet image publicitaire n'existe pas ou a été supprimée`,
        HttpStatus.NOT_FOUND
      );
    }
    return item;
  }

  async update(
    id: string,
    updateDto: RdcAdUpdateDto,
    file?: Express.Multer.File
  ) {
    const isAdExist = await this.adModel.findById({ _id: id });
    if (!isAdExist) {
      throw new HttpException(
        `L'image publicitaire n'existe pas.`,
        HttpStatus.BAD_REQUEST
      );
    }

    const res = await isAdExist
      .update({
        ...updateDto,
        ...(file && { image: file.filename }),
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite`,
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: `Image publicitaire mise à jour.`,
      status_code: HttpStatus.OK,
    };
  }

  async remove(items: DeleteDto) {
    await this.adModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite ${err}`,
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async getStats() {
    const count = await this.adModel.countDocuments().catch((err) => {
      throw new HttpException(
        `Une erreur s'est produite}`,
        HttpStatus.BAD_REQUEST
      );
    });

    return {
      count,
    };
  }
}

import {
    Controller,
    Get,
    Post,
    Body,
    UseGuards,
    UseInterceptors,
    UploadedFile,
    Param,
    Query,
  } from '@nestjs/common';
  import { FileInterceptor } from '@nestjs/platform-express';
  import { ApiTags } from '@nestjs/swagger';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { TrafficImageDto } from './dto/traffic-image.dto';
import { TrafficImageService } from './traffic-image.service';
  
  @ApiTags('TrafficImage')
  @Controller('traffic-image')
  export class TrafficImageController {
    constructor(private readonly trafficImageService: TrafficImageService) {}
  
    @Get()
    findAll(@Query() query: QueryParamsDto
    ) {
      return this.trafficImageService.getAll(query);
    }

    @Get(':id')
    findOne(@Param('id') id: number
    ) {
      return this.trafficImageService.getOne(id);
    }
  
    // @UseGuards(JwtAuthGuard)
    @Post()
    @UseInterceptors(FileInterceptor('image'))
    create(
      @Body() createDto: TrafficImageDto,
      @UploadedFile() file: Express.Multer.File
    ) {
      return this.trafficImageService.set(createDto, file);
    }
  }
  
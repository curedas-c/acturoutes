import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { TrafficImageDocument } from '../@core/schemas/traffic-image.schema';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { TrafficImageDto } from './dto/traffic-image.dto';

@Injectable()
export class TrafficImageService {
  constructor(
    @InjectModel('Traffic-Image')
    private trafficImageModel: PaginateModel<TrafficImageDocument>
  ) {}

  async set(createDto: TrafficImageDto, file: Express.Multer.File) {
    const { _id: id } = createDto;
    if (![1, 2, 3, 4, 5, 6].includes(id)) {
      throw new HttpException(`Emplacement inexistant.`, HttpStatus.BAD_REQUEST);
    }
    const isSet = await this.trafficImageModel.findById(id);

    if (isSet) {
      return this.update(id, createDto, file);
    } else {
      if (!file) {
        throw new HttpException(`Veuillez ajouter une image.`, HttpStatus.BAD_REQUEST);
      }
    }

    const trafficImage = new this.trafficImageModel({
      ...createDto,
      ...(file && { image: file.filename }),
    });
    await trafficImage.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    return {
      data: trafficImage,
      message: 'Image assignée.',
    };
  }

  async getAll(queryParams: QueryParamsDto) {
    
    const { filterOn } = queryParams;
    let { filterOnValue } = queryParams;

    if (filterOn === 'isRdcImage') {
      filterOnValue = [true , 'true'].includes(filterOnValue) ? true : {$ne: true} as any;
    }

    const res = await this.trafficImageModel.paginate(
      { ...(filterOn && { [filterOn]: filterOnValue === undefined ? true : filterOnValue }) },
      {
        pagination: true,
        limit: 3,
        page: 1,
        sort: { _id: 'asc' },
        select: '-__v',
      }
    );
    return {
      items: res.docs,
      status_code: HttpStatus.OK,
    };
  }

  async getOne(id: number) {
    const item = await this.trafficImageModel.findById(id).select('-__v');
    return {
      data: item,
    };
  }

  async update(id: number, updateDto: TrafficImageDto, file?: Express.Multer.File) {
    const res = await this.trafficImageModel
      .findByIdAndUpdate(id, {
        ...updateDto,
        ...(file && { image: file.filename }),
      })
      .select('-__v')
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
      });

    return {
      message: `Image mise à jour.`,
      data: res,
      status_code: HttpStatus.OK,
    };
  }
}

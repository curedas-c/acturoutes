import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { TrafficImageService } from './traffic-image.service';
import { TrafficImageController } from './traffic-image.controller';
import { TrafficImageSchema } from '../@core/schemas/traffic-image.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Traffic-Image',
        useFactory: () => {
          const schema = TrafficImageSchema;
          return schema;
        }
      }
    ]),
    MulterModule.register({
        limits: { fieldSize: 15 * 1024 * 1024 },
        storage: diskStorage({
          destination: '../files',
          filename: function (req, file, cb) {
            const uniqueSuffix =
              Date.now() + '-' + Math.round(Math.random() * 1e9);
            cb(null, `traffic-${uniqueSuffix}${extname(file.originalname)}`);
          }
        })
      }),
      SharedModule
  ],
  controllers: [TrafficImageController],
  providers: [TrafficImageService]
})
export class TrafficImageModule {}

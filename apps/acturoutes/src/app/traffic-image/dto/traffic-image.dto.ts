import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsBooleanString, IsNumber, IsOptional, IsString } from 'class-validator';

export class TrafficImageDto {
  @ApiProperty()
  @Type(() => Number)
  @IsNumber()
  _id: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  title: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isRdcImage: boolean;
}

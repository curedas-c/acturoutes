import { ApiProperty } from '@nestjs/swagger';
import {
  IsBooleanString,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class QueryParamsDto {
  @ApiProperty()
  @IsOptional()
  @IsString()
  filter: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  order: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  sortBy: string;

  @ApiProperty()
  @IsOptional()
  @IsNumberString()
  page: number;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  paginate: boolean;

  @ApiProperty()
  @IsOptional()
  @IsNumberString()
  limit: number;

  @ApiProperty()
  @IsOptional()
  @IsString()
  fields: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  day: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  filterOn: string;

  @ApiProperty()
  @IsOptional()
  filterOnValue: boolean;
}

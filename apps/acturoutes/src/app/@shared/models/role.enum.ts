export enum Role {
    ADMIN = "admin",
    REDACTOR = "redactor",
    COMMERCIAL = "commercial",
    INTRANET_USER = "intranet_user",
    RDC_USER = "rdc_user"
}
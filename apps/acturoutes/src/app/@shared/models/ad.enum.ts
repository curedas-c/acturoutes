export enum AdType {
  RECTANGLE = 'rectangle',
  SQUARE = 'square',
}

export enum AdPositionType {
  TOP = 'top',
  BOTTOM = 'bottom',
}

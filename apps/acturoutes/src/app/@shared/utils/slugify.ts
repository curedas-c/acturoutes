
// eslint-disable-next-line @typescript-eslint/no-var-requires
const slugify = require('slugify')

export const slugifyText = (slug: string): Promise<string> => {
  return slugify(slug, '_');
};

import * as fs from 'fs';

export async function deleteFile(filename: string, path?: string) {
  const DEFAULT_PATH = '../files'; // __dirname + 
  const PATH = path ? path : process.env.FILES_PATH || DEFAULT_PATH;
  // console.log(PATH + '/' + filename);
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  return await fs.promises.unlink(PATH + '/' + filename).catch(() => {});
}

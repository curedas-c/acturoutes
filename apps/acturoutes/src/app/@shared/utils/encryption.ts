import * as bcrypt from 'bcrypt';

export async function encrypt(item: any) {
  return await bcrypt.hash(item, 7);
}

export async function isMatch(password: any, encrypted: any) {
  const isMatch = await bcrypt.compare(password, encrypted);
  return isMatch;
}
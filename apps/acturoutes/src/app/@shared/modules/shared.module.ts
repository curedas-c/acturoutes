import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

const JWT = [
  PassportModule,
  // MulterModule.register({
  //   storage: diskStorage({
  //     destination: 'files',
  //     filename: function (req, file, cb) {
  //       const uniqueSuffix =
  //         Date.now() + '-' + Math.round(Math.random() * 1e9);
  //       cb(null, `acturoutes-${uniqueSuffix}${extname(file.originalname)}`);
  //     }
  //   })
  // })
];

@Module({
  imports: [...JWT],
  exports: [...JWT],
})
export class SharedModule {}

import { Module, Logger } from '@nestjs/common';
import { RoadWitnessService } from './road-witness.service';
import { RoadWitnessController } from './road-witness.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { AdminModule } from '../admin/admin.module';
import { JwtStrategy } from '../jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { RoadWitnessSchema } from '../@core/schemas/road-witness.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'RoadWitness',
        useFactory: () => {
          const schema = RoadWitnessSchema;
          return schema;
        },
      }]),
    MulterModule.register({
      limits: { fieldSize: 15 * 1024 * 1024 },
      storage: diskStorage({
        destination: '../files',
        filename: function (req, file, cb) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `witness-${uniqueSuffix}${extname(file.originalname)}`);
        }
      })
    }),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          secret: config.get<string>('NX_JWT_SECRET'),
          signOptions: {
            expiresIn: config.get<string | number>('NX_EXPIRES_IN'),
          },
        };
      },
    }),
    AdminModule,
    SharedModule
  ],
  controllers: [RoadWitnessController],
  providers: [RoadWitnessService, JwtStrategy, Logger],
  exports: [RoadWitnessService]
})
export class RoadWitnessModule {}

import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseGuards,
  Query,
  HttpException,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
  Logger,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { RoadWitnessCreateDto, RoadWitnessUpdateDto } from './dto/road-witness.dto';
import { RoadWitnessService } from './road-witness.service';

@ApiTags('road-witness')
@Controller('road-witness')
export class RoadWitnessController {
  constructor(
    private readonly roadWitnessService: RoadWitnessService,
    private readonly logger: Logger
  ) {}

  @Get()
  async findAll(@Query() query: QueryParamsDto) {
    const { page } = query;
    const res = await this.roadWitnessService.findAll(query).catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(`Erreur lors de la récupération des données`, HttpStatus.BAD_REQUEST);
    });

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  @Get(':slug')
  async findOne(@Param('slug') slug: string) {
    const res = await this.roadWitnessService.findBySlug(slug).catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(`Erreur lors de la récupération des données`, HttpStatus.BAD_REQUEST);
    });

    if (!res) {
      throw new HttpException(`Cet élément n'existe pas ou a été supprimé`, HttpStatus.NOT_FOUND);
    }

    return {
      data: res,
    };
  }

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(@Body() createDto: RoadWitnessCreateDto, @UploadedFile() file: Express.Multer.File) {
    if (!file) {
      throw new HttpException(`Veuillez joindre une image.`, HttpStatus.BAD_REQUEST);
    }
    await this.roadWitnessService.create(createDto, file).catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.BAD_REQUEST);
    });

    return {
      message: 'Témoin de la route posté.',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  async update(
    @Param('id') id: string,
    @Body() updateDto: RoadWitnessUpdateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    const roadWitnessElement = await this.roadWitnessService.getById(id);
    if (!roadWitnessElement) {
      throw new HttpException(`Cet élément n'existe pas.`, HttpStatus.BAD_REQUEST);
    }

    await this.roadWitnessService.update(roadWitnessElement, updateDto, file).catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    return {
      message: 'Témoin de la route mis à jour.',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  async remove(@Body() deleteDto: DeleteDto) {
    await this.roadWitnessService.remove(deleteDto).catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(`Erreur lors de la suppression`, HttpStatus.BAD_REQUEST);
    });

    return {
      message: 'Elément(s) supprimé(s).',
    };
  }
}

import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsOptional
} from 'class-validator';

export class RoadWitnessCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  author_name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  content: string;
}

export class RoadWitnessUpdateDto extends PartialType(RoadWitnessCreateDto) {}

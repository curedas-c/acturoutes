import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { RoadWitnessCreateDto, RoadWitnessUpdateDto } from './dto/road-witness.dto';
import { toRegex } from 'diacritic-regex';
import { slugifyText } from '../@shared/utils/slugify';
import { RoadWitnessDocument } from '../@core/schemas/road-witness.schema';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class RoadWitnessService {
  constructor(
    @InjectModel('RoadWitness')
    private roadWitnessModel: PaginateModel<RoadWitnessDocument>,
    private readonly logger: Logger
  ) {}

  async create(createDto: RoadWitnessCreateDto, file?: Express.Multer.File) {
    const dtoWithSlug = await this.addUniqueSlug(createDto);
    return new this.roadWitnessModel({
      ...dtoWithSlug,
      ...(file && { image: file.filename }),
    }).save();
  }

  async update(
    roadWitness: RoadWitnessDocument,
    updateDto: RoadWitnessUpdateDto,
    file?: Express.Multer.File
  ) {
    return roadWitness.update({
      ...updateDto,
      ...(file && { image: file.filename }),
    });
  }

  findAll(params: any) {
    const { filter, order, sortBy, page, paginate, limit, fields, filterOn } = params;
    const query = {
      ...(filter && {
        title: { $regex: toRegex()(filter) },
      }),
    };
    const sort = order ? { [sortBy || 'title']: order } : sortBy;

    return this.roadWitnessModel.paginate(
      { ...query, ...(filterOn && { [filterOn]: true }) },
      {
        pagination: paginate || true,
        limit: limit || 10,
        page: page || 1,
        sort: sort || { createdAt: -1 },
        select: fields || '-content -__v',
      }
    );
  }

  findBySlug(slug: string) {
    return this.roadWitnessModel.findOne({ slug }).select('-__v');
  }

  remove(items: DeleteDto) {
    return this.roadWitnessModel.deleteMany({
      _id: items.uuid,
    });
  }

  async addUniqueSlug(data: RoadWitnessCreateDto) {
    const slug = await slugifyText(data.title);
    const existingSlugs = await this.findExistingSlugs(slug);

    // if slug doesn't already exists
    if (!existingSlugs || existingSlugs.length === 0) {
      return { ...data, slug };
    }

    // Add a suffix
    const newSlug = slug + '_' + existingSlugs.length;

    return { ...data, newSlug };
  }

  async findExistingSlugs(slug: string) {
    return await this.roadWitnessModel.find({ slug }).catch((err) => {
      this.logger.log(0, err);
    });
  }

  getById(id: string) {
    return this.roadWitnessModel.findById({ _id: id }).catch((err) => {
      this.logger.log(0, err);
    });
  }
}

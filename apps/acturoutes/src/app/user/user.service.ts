import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { toRegex } from 'diacritic-regex';
import { PaginateModel } from 'mongoose';
import { UserDocument } from '../@core/schemas/user.schema';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { encrypt } from '../@shared/utils/encryption';
import { UserCreateDto, UserPasswordUpdateDTO, UserUpdateDto } from './dto/user.dto';

@Injectable()
export class UserService {

  constructor(@InjectModel('User') private userModel: PaginateModel<UserDocument>) { }


  async create(createDto: UserCreateDto) {
    const { email } = createDto;
    const isUserExist = await this.userModel.findOne({ email });

    if (isUserExist) {
      throw new HttpException(`Ce compte existe déjà`, HttpStatus.BAD_REQUEST);
    }

    const createdUser = new this.userModel({
      ...createDto
    });

    const user = await createdUser.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite lors de la création du compte`, HttpStatus.BAD_REQUEST);
    });

    return this.sanitize(user);
  }

  async findAll(queryParams: QueryParamsDto) {
    const { filter, order, sortBy, page, paginate, limit, fields } = queryParams;
    const query = {
      ...(filter && {
        name: { $regex: toRegex()(filter)},
      })
    };
    const sort = order ? { [sortBy || 'createdAt']: order } : sortBy;

    const res = await this.userModel.paginate(query, {
      pagination: paginate || true,
      limit: limit || 10,
      page: page || 1,
      sort: sort || { createdAt: -1 },
      select: fields || '-password -__v',
    });
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  async update(id: string, updateDto: UserUpdateDto) {
    const updateOptions = {
      ...updateDto,
      updatedAt: new Date(),
    };
    const itemToUpdate = (await this.getFromParam({ _id: id }, false)) as UserDocument;

    if (!itemToUpdate) {
      throw new HttpException(`Ce compte n'existe pas ou a été supprimée.`, HttpStatus.BAD_REQUEST);
    }

    return await itemToUpdate
      .updateOne(updateOptions, { new: true })
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
      });
  }

  async updatePassword(userID: string, passwordDTO: UserPasswordUpdateDTO) {
    const { password } = passwordDTO;

    const encryptedPassword = await encrypt(password);
    return await this.userModel
      .findByIdAndUpdate(userID, {
        password: encryptedPassword,
      })
      .catch(() => {
        throw new HttpException(`L'utilisateur n'existe pas ou une erreur s'est produite.`, HttpStatus.NOT_FOUND);
      });
  }

  async findByEmail(email: string) {
    return await this.userModel.findOne({ email });
  }

  async remove(items: DeleteDto) {
    await this.userModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite lors de la suppression des éléments`, // TODO: remove ${err} form delete requests
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async getFromParam(params: any, multiple = true): Promise<UserDocument[] | UserDocument> {
    const res = multiple ? await this.userModel.find(params) : await this.userModel.findOne(params);
    return res;
  }

  sanitize(user: UserDocument) {
    const sanitized = user.toObject();
    delete sanitized['password'];
    return sanitized;
  }
}

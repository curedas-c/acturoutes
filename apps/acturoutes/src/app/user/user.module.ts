import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import { User, UserSchema } from '../@core/schemas/user.schema';
import { encrypt } from '../@shared/utils/encryption';
import { JwtModule } from '@nestjs/jwt';
import { SharedModule } from '../@shared/modules/shared.module';
import { JwtStrategy } from '../jwt.strategy';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'User',
        useFactory: () => {
          const schema = UserSchema;
          // eslint-disable-next-line @typescript-eslint/ban-types
          schema.pre<User>('save', async function (next: Function) {
            const encrypted = await encrypt(this.password).catch((err) => {
              return next(err);
            });
            this.password = encrypted;
            next();
          });
          return schema;
        },
      },
    ]),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => {
        return {
          secret: config.get<string>('NX_JWT_SECRET'),
          signOptions: {
            expiresIn: config.get<string | number>('NX_EXPIRES_IN'),
          },
        };
      },
    }),
    SharedModule,
  ],
  controllers: [UserController],
  providers: [UserService, JwtStrategy],
  exports: [UserService],
})
export class UserModule {}

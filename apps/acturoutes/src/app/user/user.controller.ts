import { Controller, Get, Post, Body, Patch, Param, HttpStatus, Query, UseGuards } from '@nestjs/common';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { UserCreateDto, UserUpdateDto, UserPasswordUpdateDTO } from './dto/user.dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createUserDto: UserCreateDto) {
    const registeredUser = await this.userService.findByEmail(createUserDto.email);
    if (registeredUser) {
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Cette adresse email existe déjà.',
      };
    }
    
    const newUser = await this.userService.create(createUserDto);
    return {
      data: newUser,
      message: 'Compte créé',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Query() query: QueryParamsDto) {
    const response = await this.userService.findAll(query);
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateUserDto: UserUpdateDto) {
    await this.userService.update(id, updateUserDto);
    const response = {
      statusCode: HttpStatus.OK,
      message: 'Compte mis à jour'
    };
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/password/update/:id')
  async updatePassWord(@Param('id') id: string, @Body() passwordDto: UserPasswordUpdateDTO) {
    await this.userService.updatePassword(id, passwordDto);
    return {
      message: 'Mot de passe Mis à jour',
      status_code: HttpStatus.OK,
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  async remove(@Body() deleteDto: DeleteDto) {
    return this.userService.remove(deleteDto);
  }
}

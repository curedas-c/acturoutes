import { Logger, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AdminModule } from './admin/admin.module';
import { ConfigModule } from '@nestjs/config';
import { join } from 'path';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { ArticleModule } from './article/article.module';
import { TrafficModule } from './traffic/traffic.module';
import { HeadlineModule } from './headline/headline.module';
import { BigFormatModule } from './big-format/big-format.module';
import { ActuTvModule } from './actu-tv/actu-tv.module';
import { AdModule } from './ad/ad.module';
import { TrafficImageModule } from './traffic-image/traffic-image.module';
import { SignalModule } from './signal/signal.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';
import { ChatRoomModule } from './chat-room/chat-room.module';
import { RoadWitnessModule } from './road-witness/road-witness.module';
import * as mongoosePaginate from 'mongoose-paginate-v2';
import { ServeStaticModule } from '@nestjs/serve-static';
import { RdcAdModule } from './rdc-ad/rdc-ad.module';
import { IntroImageModule } from './intro-image/intro-image.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(process.mainModule['path'], '..', 'acturoutes-admin'),
      serveRoot: '/administration',
    }),
    ServeStaticModule.forRoot({
      rootPath: join(process.mainModule['path'], '..', 'acturoutes-public'),
      renderPath: '*',
      exclude: ['/api/*', '/socket.io/*', '/administration/*'],
    }),
    // ServeStaticModule.forRoot(
    //   {
    //     rootPath: join(__dirname, 'front/dist/acturoutes-admin'),
    //     serveRoot: '/administration',
    //     exclude: ['/api/*', '/socket.io/*'],
    //   }
    // ),
    // AngularUniversalModule.forRoot({
    //   bootstrap: AppServerModule,
    //   viewsPath: join(process.cwd(), 'front/dist/acturoutes-mono/browser'),
    // }),
    AuthModule,
    AdminModule,
    UserModule,
    ArticleModule,
    TrafficModule,
    HeadlineModule,
    BigFormatModule,
    ActuTvModule,
    AdModule,
    TrafficImageModule,
    SignalModule,
    ChatRoomModule,
    RoadWitnessModule,
    RdcAdModule,
    IntroImageModule,
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: process.env.NX_DB_URI,
        connectionFactory: (connection) => {
          connection.plugin(mongoosePaginate);
          return connection;
        },
      }),
    }),
    MailerModule.forRootAsync({
      useFactory: () => ({
        transport: {
          // service: 'yahoo',
          host: process.env.NX_MAIL_HOST || 'smtp.yahoo.com',
          port: Number(process.env.NX_MAIL_PORT) || 465,
          ignoreTLS: process.env.NX_MAIL_HOST ? false : true,
          secure: process.env.NX_MAIL_HOST ? true : false,
          auth: {
            user: process.env.NX_MAIL_USER,
            pass: process.env.NX_MAIL_PASSWORD,
          },
        },
        defaults: {
          from: process.env.NX_MAIL_FROM,
        },
        preview: process.env.NX_MAIL_HOST ? false : true,
        template: {
          dir: __dirname + '/templates',
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
    }),
  ],
  providers: [Logger],
})
export class AppModule {}

import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ActuTvDocument } from '../@core/schemas/actu-tv.schema';
import { ActuTvCreateDto, ActuTvUpdateDto } from './dto/actu-tv.dto';

@Injectable()
export class ActuTvService {
  constructor(
    @InjectModel('Actu-Tv')
    private actuTvModel: Model<ActuTvDocument>
  ) {}

  async set(createDto: ActuTvCreateDto, file: Express.Multer.File) {
    const isSet = await this.actuTvModel.findById(1);

    if (isSet) {
      return this.update(createDto as ActuTvUpdateDto, file);
    }

    const actuTv = new this.actuTvModel({
      ...createDto,
      ...(file && { image: file.filename })
    });
    await actuTv.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });

    return {
      data: actuTv,
      message: 'Paramêtres sauvegardés!',
    };
  }

  async getAll() {
    const res = await this.actuTvModel.findById(1).select('-_id -__v');
    return {
      data: res,
      status_code: HttpStatus.OK,
    };
  }

  async update(updateDto: ActuTvUpdateDto, file?: Express.Multer.File) {
    const res = await this.actuTvModel
      .findByIdAndUpdate(1, {
        ...updateDto,
        ...(file && { image: file.filename })
      })
      .select('-_id -__v')
      .catch((err) => {
        throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
      });

    return {
      message: `Paramêtres mis à jour.`,
      data: res,
      status_code: HttpStatus.OK,
    };
  }
}

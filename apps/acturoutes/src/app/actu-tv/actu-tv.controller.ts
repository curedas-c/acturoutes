import {
    Controller,
    Get,
    Post,
    Body,
    Patch,
    UseGuards,
    UseInterceptors,
    UploadedFile,
  } from '@nestjs/common';
  import { FileInterceptor } from '@nestjs/platform-express';
  import { ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { ActuTvService } from './actu-tv.service';
import { ActuTvCreateDto, ActuTvUpdateDto } from './dto/actu-tv.dto';
  
  @ApiTags('ActuTv')
  @Controller('actu-tv')
  export class ActuTvController {
    constructor(private readonly actuTvService: ActuTvService) {}
  
    @Get()
    findAll(
    ) {
      return this.actuTvService.getAll();
    }
  
    // @UseGuards(JwtAuthGuard)
    @Post()
    @UseInterceptors(FileInterceptor('image'))
    create(
      @Body() createDto: ActuTvCreateDto,
      @UploadedFile() file: Express.Multer.File
    ) {
      return this.actuTvService.set(createDto, file);
    }
  
    // @UseGuards(JwtAuthGuard)
    @Patch()
    @UseInterceptors(FileInterceptor('image'))
    update(
      @Body() updateDto: ActuTvUpdateDto,
      @UploadedFile() file: Express.Multer.File
    ) {
      return this.actuTvService.update(updateDto, file);
    }
  }
  
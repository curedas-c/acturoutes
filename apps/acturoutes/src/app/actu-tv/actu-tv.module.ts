import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { ActuTvController } from './actu-tv.controller';
import { ActuTvService } from './actu-tv.service';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { ActuTvSchema } from '../@core/schemas/actu-tv.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Actu-Tv',
        useFactory: () => {
          const schema = ActuTvSchema;
          return schema;
        }
      }
    ]),
    MulterModule.register({
        limits: { fieldSize: 15 * 1024 * 1024 },
        storage: diskStorage({
          destination: '../files',
          filename: function (req, file, cb) {
            const uniqueSuffix =
              Date.now() + '-' + Math.round(Math.random() * 1e9);
            cb(null, `tv-${uniqueSuffix}${extname(file.originalname)}`);
          }
        })
      }),
      SharedModule
  ],
  controllers: [ActuTvController],
  providers: [ActuTvService]
})
export class ActuTvModule {}

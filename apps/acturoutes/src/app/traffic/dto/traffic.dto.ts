import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsBooleanString, IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class TrafficCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  message: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isRdcMessage: boolean;
}

export class TrafficUpdateDto extends PartialType(TrafficCreateDto) {}
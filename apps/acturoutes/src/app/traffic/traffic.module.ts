import { Module } from '@nestjs/common';
import { TrafficService } from './traffic.service';
import { TrafficController } from './traffic.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { TrafficSchema } from '../@core/schemas/traffic.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Traffic',
        useFactory: () => {
          const schema = TrafficSchema;
          return schema;
        },
      }
    ]),
    SharedModule
  ],
  controllers: [TrafficController],
  providers: [TrafficService],
})
export class TrafficModule {}

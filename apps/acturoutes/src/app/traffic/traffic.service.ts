import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { TrafficCreateDto, TrafficUpdateDto } from './dto/traffic.dto';
import * as dayjs from 'dayjs';
import { toRegex } from 'diacritic-regex';
import { TrafficDocument } from '../@core/schemas/traffic.schema';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Injectable()
export class TrafficService {
  constructor(@InjectModel('Traffic') private trafficModel: PaginateModel<TrafficDocument>) {}

  async create(createDto: TrafficCreateDto) {
    const traffic = new this.trafficModel(createDto);
    const message = await traffic.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.BAD_REQUEST);
    });

    return message;
  }

  async findAll(queryParams: QueryParamsDto) {
    const { filter, order, sortBy, page, paginate, limit, fields, day, filterOn } = queryParams;
    let { filterOnValue } = queryParams;

    if (filterOn === 'isRdcMessage') {
      filterOnValue = [true , 'true'].includes(filterOnValue) ? true : {$ne: true} as any;
    }

    const dateStart = day ? dayjs(day).startOf('day').toDate() : dayjs().startOf('day').toDate();
    const dateEnd = day ? dayjs(day).endOf('day').toDate() : dayjs().endOf('day').toDate();

    const query = {
      ...(filter && {
        message: { $regex: toRegex()(filter)},
      }),
      createdAt: {
        $gte: dateStart,
        $lt: dateEnd
      }
    };

    const sort = order ? { [sortBy || 'createdAt']: order } : sortBy;
    const res = await this.trafficModel.paginate(
      { ...query, ...(filterOn && { [filterOn]: filterOnValue === undefined ? true : filterOnValue }) }, {
      pagination: paginate || true,
      limit: limit || 10,
      page: page || 1,
      sort: sort || { createdAt: -1 },
      select: fields || '-__v',
    });
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1
    };
  }

  async update(id: string, updateDto: TrafficUpdateDto) {
    const updateOptions = {
      ...updateDto,
      updatedAt: new Date(),
    };
    const itemToUpdate = (await this.getFromParam({ _id: id }, false)) as TrafficDocument;

    if (!itemToUpdate) {
      throw new HttpException(`Ce message n'existe pas ou a été supprimée.`, HttpStatus.BAD_REQUEST);
    }

    return await itemToUpdate.updateOne(updateOptions, { new: true }).catch((err) => {
      throw new HttpException(`Une erreur s'est produite`, HttpStatus.BAD_REQUEST);
    });
  }

  async remove(items: DeleteDto) {
    await this.trafficModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite lors de la suppression des éléments`, // TODO: remove ${err} form delete requests
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async getFromParam(params: any, multiple = true): Promise<TrafficDocument[] | TrafficDocument> {
    const res = multiple ? await this.trafficModel.find(params) : await this.trafficModel.findOne(params);
    return res;
  }
}

import { Controller, Get, Post, Body, Patch, Param, HttpStatus, Query, UseGuards, UseInterceptors, UploadedFile } from '@nestjs/common';
import { TrafficService } from './traffic.service';
import { TrafficCreateDto, TrafficUpdateDto } from './dto/traffic.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { DeleteDto } from '../@shared/dto/delete-item.dto';

@Controller('traffic')
export class TrafficController {
  constructor(private readonly trafficService: TrafficService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  async create(@Body() createTrafficDto: TrafficCreateDto, @UploadedFile() file: Express.Multer.File) {
    const newMessage = await this.trafficService.create(createTrafficDto);
    return {
      data: newMessage,
      message: 'Message créé!',
    };
  }

  // @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Query() query: QueryParamsDto) {
    const response = await this.trafficService.findAll(query);
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('image'))
  async update(@Param('id') id: string, @Body() updateTrafficDto: TrafficUpdateDto, @UploadedFile() file: Express.Multer.File) {
    await this.trafficService.update(id, updateTrafficDto);
    const response = {
      statusCode: HttpStatus.OK,
      message: 'Message mis à jour',
    };
    return response;
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  async remove(@Body() deleteDto: DeleteDto) {
    return this.trafficService.remove(deleteDto);
  }
}

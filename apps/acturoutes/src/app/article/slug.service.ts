import { Injectable } from '@nestjs/common';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const slugify = require('slugify')
@Injectable()
export class SlugService {

  slugify(slug: string): Promise<string> {
    return slugify(slug, '_');
  }
}

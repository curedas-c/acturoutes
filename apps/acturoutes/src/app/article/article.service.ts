import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { ArticleCreateDto } from './dto/create-article.dto';
import { ArticleUpdateDto } from './dto/update-article.dto';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { toRegex } from 'diacritic-regex';
import { SlugService } from './slug.service';
import { ArticleDocument } from '../@core/schemas/article.schema';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { deleteFile } from '../@shared/utils/file-deletion';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel('Article')
    private articleModel: PaginateModel<ArticleDocument>,
    private readonly logger: Logger,
    private slugService: SlugService
  ) {}

  async create(createDto: ArticleCreateDto, file: Express.Multer.File) {
    if (!file) {
      throw new HttpException(
        `Veuillez ajouter une image de couverture.`,
        HttpStatus.BAD_REQUEST
      );
    }

    const { title } = createDto;
    const isArticleExist = await this.articleModel.findOne({ title });

    if (isArticleExist) {
      throw new HttpException(
        `Un article avec le même titre existe déjà.`,
        HttpStatus.BAD_REQUEST
      );
    }

    createDto = await this.uniqueSlug(createDto);
    const newArticle = new this.articleModel({
      ...createDto,
      viewCount: 0,
      main_image: file.filename,
    });
    await newArticle.save().catch((err) => {
      this.logger.log(0, err);
      throw new HttpException(
        `Une erreur s'est produite`,
        HttpStatus.BAD_REQUEST
      );
    });

    return {
      message: 'Article créé.',
      data: newArticle,
      status_code: HttpStatus.CREATED,
    };
  }

  async findAll(params: any) {
    const { filter, order, sortBy, page, paginate, limit, fields, filterOn } =
      params;
    const query = {
      ...(filter && {
        title: { $regex: toRegex()(filter) },
      }),
    };

    const sort = order ? { [sortBy || 'title']: order } : sortBy;

    const res = await this.articleModel.paginate(
      {
        ...query,
        ...(filterOn && { [filterOn]: true }),
        ...(!filterOn && {
          isAgenda: { $ne: true },
          // isEvent: { $ne: true },
          isRdcArticle: { $ne: true },
          isEnglishArticle: { $ne: true },
        }),
      },
      {
        pagination: paginate || true,
        limit: limit || 10,
        page: page || 1,
        sort: sort || { createdAt: -1 },
        select: fields || '-content -__v',
      }
    );
    if (!res) {
      throw new HttpException(
        `Une erreur s'est produite}`,
        HttpStatus.BAD_REQUEST
      );
    }

    return {
      items: res.docs,
      total_items: res.totalDocs,
      total_pages: res.totalPages,
      current_page: page || 1,
    };
  }

  async findOne(id: string) {
    const item = await this.articleModel
      .findById(id)
      .select('-__v')
      .catch((err) => this.logger.log(0, err));

    if (!item) {
      throw new HttpException(
        `Cet article n'existe pas ou a été supprimée`,
        HttpStatus.NOT_FOUND
      );
    }
    return item;
  }

  async update(
    id: string,
    updateDto: ArticleUpdateDto,
    file?: Express.Multer.File
  ) {
    const isArticleExist = await this.articleModel.findById({ _id: id });
    if (!isArticleExist) {
      throw new HttpException(
        `L'article n'existe pas.`,
        HttpStatus.BAD_REQUEST
      );
    }

    if (updateDto.title) {
      const newSlug = await this.slugService.slugify(
        updateDto.title || isArticleExist.title
      );
      if (newSlug != updateDto.slug) {
        updateDto = {
          ...updateDto,
          slug: newSlug,
        };
      }
    }

    const res = await this.articleModel
      .findByIdAndUpdate(id, {
        ...updateDto,
        ...(file && { main_image: file.filename }),
      })
      .select('-_id -__v')
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite`,
          HttpStatus.BAD_REQUEST
        );
      });

    return {
      message: `Article mis à jour.`,
      data: res.toJSON(),
      status_code: HttpStatus.OK,
    };
  }

  async updateMany(ids: string[], options: any) {
    return await this.articleModel.updateMany(
      {
        _id: ids,
      },
      { ...options }
    );
  }

  async remove(items: DeleteDto) {
    await this.articleModel
      .deleteMany({
        _id: items.uuid,
      })
      .catch((err) => {
        throw new HttpException(
          `Une erreur s'est produite ${err}`,
          HttpStatus.BAD_REQUEST
        );
      });

    const articles = await this.articleModel.find({ _id: items.uuid });

    articles?.forEach(async (el) => {
      await deleteFile(el.main_image);
    });

    return {
      message: 'Les éléments ont étés supprimés.',
      status_code: HttpStatus.OK,
    };
  }

  async findBySlug(slug: string) {
    return this.articleModel.findOneAndUpdate(
      { slug },
      { $inc: { viewCount: 1 } },
      {
        new: true,
      }
    );
  }

  async getStats() {
    const count = await this.articleModel.countDocuments().catch((err) => {
      throw new HttpException(
        `Une erreur s'est produite}`,
        HttpStatus.BAD_REQUEST
      );
    });

    return {
      count,
    };
  }

  async uniqueSlug(article: ArticleCreateDto) {
    article.slug = await this.slugService.slugify(
      article.title.replace('.', '')
    );
    const exists = await this.findSlugs(article.slug);

    // if slug doesn't already exists
    if (!exists || exists.length === 0) {
      return article;
    }

    // Add to suffix
    article.slug = article.slug + '_' + exists.length;

    return article;
  }

  private async findSlugs(slug: string) {
    return await this.articleModel.find({ slug }).catch((err) => {
      this.logger.log(0, err);
    });
  }
}

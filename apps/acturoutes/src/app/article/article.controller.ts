import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  UseGuards,
  Query,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiTags } from '@nestjs/swagger';
import { DeleteDto } from '../@shared/dto/delete-item.dto';
import { QueryParamsDto } from '../@shared/dto/query-params.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';
import { ArticleService } from './article.service';
import { ArticleCreateDto } from './dto/create-article.dto';
import { ArticleUpdateDto } from './dto/update-article.dto';

@ApiTags('Article')
@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  findAll(
    @Query() query: QueryParamsDto
  ) {
    return this.articleService.findAll(query);
  }

  @Get('/stats')
  getStats() {
    return this.articleService.getStats();
  }

  @Get(':slug')
  async findOne(@Param('slug') slug: string) {
    const res = await this.articleService.findBySlug(slug);
    return {
      data: res
    }
  }

  // @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FileInterceptor('main_image'))
  create(
    @Body() createDto: ArticleCreateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.articleService.create(createDto, file);
  }

  // @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FileInterceptor('main_image'))
  update(
    @Param('id') id: string,
    @Body() updateDto: ArticleUpdateDto,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.articleService.update(id, updateDto, file);
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/delete')
  remove(@Body() deleteDto: DeleteDto) {
    return this.articleService.remove(deleteDto);
  }
}

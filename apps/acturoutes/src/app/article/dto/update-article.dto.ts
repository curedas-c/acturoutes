import { PartialType } from '@nestjs/swagger';
import { ArticleCreateDto } from './create-article.dto';

export class ArticleUpdateDto extends PartialType(ArticleCreateDto) {}

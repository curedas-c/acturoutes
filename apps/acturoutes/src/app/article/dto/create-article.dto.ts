import { ApiProperty } from '@nestjs/swagger';
import { IsBooleanString, IsOptional } from 'class-validator';
import {
  IsNotEmpty,
  IsString,
} from 'class-validator';

export class ArticleCreateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  subtitle: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  slug: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  description: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  legend: string;

  @ApiProperty()
  @IsOptional()
  @IsString()
  content: string;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isAgenda: boolean;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isEvent: boolean;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isHeadline: boolean;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isBigFormat: boolean;

  @ApiProperty()
  @IsOptional()
  @IsBooleanString()
  isEnglishArticle: boolean;
}

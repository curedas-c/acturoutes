import { Logger, Module } from '@nestjs/common';
import { ArticleService } from './article.service';
import { ArticleController } from './article.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { SlugService } from './slug.service';
import { ArticleSchema } from '../@core/schemas/article.schema';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Article',
        useFactory: () => {
          const schema = ArticleSchema;
          return schema;
        },
      }
    ]),
    MulterModule.register({
      limits: { fieldSize: 15 * 1024 * 1024 },
      storage: diskStorage({
        destination: '../files',
        filename: function (req, file, cb) {
          const uniqueSuffix =
            Date.now() + '-' + Math.round(Math.random() * 1e9);
          cb(null, `acturoutes-${uniqueSuffix}${extname(file.originalname)}`);
        }
      })
    }),
    SharedModule
  ],
  controllers: [ArticleController],
  providers: [ArticleService, SlugService, Logger],
  exports: [ArticleService]
})
export class ArticleModule {}

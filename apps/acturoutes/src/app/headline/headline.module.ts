import { Module } from '@nestjs/common';
import { HeadlineService } from './headline.service';
import { HeadlineController } from './headline.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { HeadlineSchema } from '../@core/schemas/headline.schema';
import { ArticleModule } from '../article/article.module';
import { SharedModule } from '../@shared/modules/shared.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Headline',
        useFactory: () => {
          const schema = HeadlineSchema;
          return schema;
        },
      }
    ]),
    ArticleModule,
    SharedModule
  ],
  controllers: [HeadlineController],
  providers: [HeadlineService],
})
export class HeadlineModule {}

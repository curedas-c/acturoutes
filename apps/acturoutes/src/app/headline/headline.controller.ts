import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';
import { HeadlineService } from './headline.service';
import { HeadlineCreateDto } from './dto/headline.dto';
import { JwtAuthGuard } from '../jwt-auth.guard';

@Controller('headline')
export class HeadlineController {
  constructor(private readonly headlineService: HeadlineService) {}

  // @UseGuards(JwtAuthGuard)
  @Post()
  async create(@Body() createHeadlineDto: HeadlineCreateDto) {
    await this.headlineService.create(createHeadlineDto);
    return {
      message: 'Articles ajoutés à : "A la une',
    };
  }

  @Get()
  findAll() {
    return this.headlineService.findAll();
  }
}

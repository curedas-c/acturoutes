import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PaginateModel } from 'mongoose';
import { HeadlineCreateDto } from './dto/headline.dto';
import { ArticleService } from '../article/article.service';
import { HeadlineDocument } from '../@core/schemas/headline.schema';

@Injectable()
export class HeadlineService {
  constructor(@InjectModel('Headline') private headlineModel: PaginateModel<HeadlineDocument>, private articleService: ArticleService) {}

  async create(createDto: HeadlineCreateDto) {
    const headline = new this.headlineModel(createDto);
    const content = await headline.save().catch((err) => {
      throw new HttpException(`Une erreur s'est produite.${err}`, HttpStatus.BAD_REQUEST);
    });

    const { article } = createDto;
    const articleUpdated = await this.articleService.updateMany(article, { isHeadline: true });
    if (!articleUpdated) {
      await this.remove(article);
      throw new HttpException(`Une erreur s'est produite.`, HttpStatus.BAD_REQUEST);
    }

    return content;
  }

  async findAll() {
    const query = {};

    const res = await this.headlineModel.paginate(
      { query },
      {
        pagination: true,
        limit: 1,
        page: 1,
        sort: { createdAt: -1 },
        select: '-__v',
        populate: [
          {
            path: 'article',
            select: '-content -__v'
          },
        ],
      }
    );
    if (!res) {
      throw new HttpException(`Une erreur s'est produite}`, HttpStatus.BAD_REQUEST);
    }

    return {
      items: res.docs[0]?.article,
    };
  }

  async remove(ids: string[]) {
    await this.headlineModel.deleteMany({
      _id: ids,
    });
  }
}

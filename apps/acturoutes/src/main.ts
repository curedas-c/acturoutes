/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

 import { ValidationPipe } from '@nestjs/common';
 import { NestFactory } from '@nestjs/core';
 import { NestExpressApplication } from '@nestjs/platform-express';
 import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
 import { WinstonModule } from 'nest-winston';
 import * as winston from 'winston';
 import { join } from 'path';
import { Logger } from '@nestjs/common';
import { AppModule } from './app/app.module';
import { getHeapStatistics } from 'v8';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: WinstonModule.createLogger({
      format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json(),
      ),
      transports: [
        new winston.transports.Console(),
        new winston.transports.File({
          dirname: join(__dirname, '/log/error/'), 
          filename: 'error.log',
          level: 'error',
        }),
        new winston.transports.File({
          dirname: join(__dirname, '/log/info/'),
          filename: 'info.log',
          level: 'info',
        }),
      ],
    })
  });
  app.enableCors({
    methods: ['GET', 'PATCH', 'POST'],
    maxAge: 3600,
  });
  const globalPrefix = 'api/v1';
  app.setGlobalPrefix(globalPrefix);
  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  // Assets Serve
  app.useStaticAssets(join(__dirname, '..', 'files'), {
    prefix: '/files'
  });
  // swagger config
  const config = new DocumentBuilder()
    .setTitle('Acturoutes API')
    .setDescription('Documentation de l\'API acturoutes.')
    .setVersion('1.0')
    .addTag('acturoutes')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);

  const used = process.memoryUsage().heapUsed / 1024 / 1024;
  console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
  console.log(`node heap limit = ${getHeapStatistics().heap_size_limit / (1024 * 1024)} Mb`)
  const port = process.env.NX_PORT || 3333;
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`
  );

  // enable hot reload
  // if (module.) {
  //   module.hot.accept();
  //   module.hot.dispose(() => app.close());
  // }
}

bootstrap();

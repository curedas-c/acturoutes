export const environment = {
  production: true,
  rootUrl: '/files',
  apiUrl: '/api/v1',
  socketUrl: '/',
};

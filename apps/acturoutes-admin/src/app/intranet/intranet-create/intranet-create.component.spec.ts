import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntranetCreateComponent } from './intranet-create.component';

describe('IntranetCreateComponent', () => {
  let component: IntranetCreateComponent;
  let fixture: ComponentFixture<IntranetCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntranetCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntranetCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

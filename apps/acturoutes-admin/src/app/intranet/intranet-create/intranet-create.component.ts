import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChatService } from '@core/services/chat.service';
import { ConfirmService } from '@core/services/confirm.service';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { AdminService } from '../../admin/shared/services/admin.service';

@Component({
  selector: 'app-intranet-create',
  templateUrl: './intranet-create.component.html',
  styleUrls: ['./intranet-create.component.scss'],
})
export class IntranetCreateComponent implements OnInit, OnDestroy {
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  adminList$: Observable<any> = this.admin.getAllItems();
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<IntranetCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private chat: ChatService,
    private admin: AdminService,
    private confirmService: ConfirmService
  ) {
    this.currentData = this.data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      name: [this.currentData?.name || '', [Validators.required]],
      admin: [this.currentData?.admin || '', [Validators.required]],
    });
  }

  sendForm() {
    this.switchButtonState();
    this.chat
      .setChatRoom(this.defaultForm.value, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        })
      )
      .subscribe();
  }

  onDelete(ID: string) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer ce canal ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON',
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dialog) => {
        if (dialog) {
          this.switchButtonState();
          this.chat
            .deleteChatRoom([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

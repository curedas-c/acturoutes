import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntranetRoutingModule } from './intranet-routing.module';
import { IntranetComponent } from './intranet.component';
import { SharedModule } from '@shared/modules/shared.module';
import { IntranetListComponent } from './intranet-list/intranet-list.component';
import { IntranetChatComponent } from './intranet-chat/intranet-chat.component';
import { IntranetCreateComponent } from './intranet-create/intranet-create.component';
import { NbChatModule } from '@nebular/theme';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NgxPermissionsModule } from 'ngx-permissions';
import { AdminService } from '../admin/shared/services/admin.service';
import { TruncateAfterPipe } from '@shared/pipes/truncate.pipe';
import { ConfirmModule } from '@shared/modules/confirm.module';


@NgModule({
  declarations: [
    IntranetComponent,
    IntranetListComponent,
    IntranetChatComponent,
    IntranetCreateComponent,
    TruncateAfterPipe
  ],
  imports: [
    CommonModule,
    IntranetRoutingModule,
    NbChatModule,
    MatDialogModule,
    ConfirmModule,
    SharedModule,
    NgxPermissionsModule.forChild()
  ],
  providers: [
    AdminService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: [] }
  ]
})
export class IntranetModule { }

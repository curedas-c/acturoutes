import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntranetChatComponent } from './intranet-chat.component';

describe('IntranetChatComponent', () => {
  let component: IntranetChatComponent;
  let fixture: ComponentFixture<IntranetChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntranetChatComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntranetChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

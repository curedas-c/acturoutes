import { Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { combineLatest, Subject } from 'rxjs';
import { AppStateService } from '@core/services/app-state.service';
import { finalize, takeUntil } from 'rxjs/operators';
import { ChatService } from '@core/services/chat.service';
import { ChatRoom } from '@shared/models/intranet/chat-room.model';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'app-intranet-chat',
  templateUrl: './intranet-chat.component.html',
  styleUrls: ['./intranet-chat.component.scss'],
})
export class IntranetChatComponent implements OnInit, OnDestroy {
  @ViewChild ('chatForm', { static: false }) nbChat: ElementRef ;
  messages: any[] = [];
  currentRoom: ChatRoom;
  isLoading = false;
  currentPage = 1;
  shouldScroll = true;
  private unsubscribe$ = new Subject<void>();
  constructor(
    private state: AppStateService,
    private chat: ChatService,
    private auth: AuthService,
    public renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this.listenToRoomChange();
    this.getMessages();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  sendMessage(event: any) {
    const files = !event.files
      ? []
      : event.files.map((file) => {
          return {
            url: file.src,
            type: file.type,
            icon: 'file-text-outline',
          };
        });

    this.chat.sendMessage(this.currentRoom._id, {
      text: event.message,
      date: new Date(),
      type: files.length ? 'file' : 'text',
      files: files,
    });
  }

  getMessages() {
    combineLatest([this.chat.getMessage(), this.auth.getDetails()])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([chatData, user]) => {
        if (this.currentRoom?._id === chatData?.room) {
          if (chatData.message.user.name === user.name) {
            chatData.message.reply = true;
          } else {
            chatData.message.reply = false;
          }
          this.enableScroll();
          this.messages.push(chatData.message);
        }
      });
  }

  getRoomMessage(roomId: string) {
    this.switchLoadingState();
    this.chat
      .getRoomMessages(roomId)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchLoadingState();
        })
      )
      .subscribe((res) => {
        this.currentPage = 1; // reset current page
        this.enableScroll();
        this.messages = res.reverse();
        setTimeout(() => {
          this.renderer.listen(this.nbChat['scrollable'].nativeElement, 'scroll', ($event) => {
            this.loadNextPage($event)
          });
        }, 500);
      });
  }

  listenToRoomChange() {
    this.state.currentCanal$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: ChatRoom) => {
        if (data) {
          this.currentRoom = data;
          this.getRoomMessage(data._id);
          // reset message list
          this.messages = [];
        }
      });
  }

  loadNextPage(event: any) {
    if (event.srcElement.scrollTop === 0) {
      this.chat
      .getRoomMessages(this.currentRoom._id, {page: this.currentPage + 1})
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe((res) => {
        this.disableScroll();
        this.currentPage += 1;
        res.forEach(msg => {
          this.messages.unshift(msg);
        });
        event.srcElement.scrollTop = 1; // make element scroll to be able to scroll top again
      });
    }
  }

  switchLoadingState() {
    this.isLoading = !this.isLoading;
  }

  enableScroll() {
    this.shouldScroll = true;
  }
  disableScroll() {
    this.shouldScroll = false;
  }
}

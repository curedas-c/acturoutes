import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { IntranetCreateComponent } from '../intranet-create/intranet-create.component';
import { AppStateService } from '@core/services/app-state.service';
import { forkJoin, Subject } from 'rxjs';
import { ChatService } from '@core/services/chat.service';
import { takeUntil } from 'rxjs/operators';
import { ChatRoom } from '@shared/models/intranet/chat-room.model';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'app-intranet-list',
  templateUrl: './intranet-list.component.html',
  styleUrls: ['./intranet-list.component.scss'],
})
export class IntranetListComponent implements OnInit, OnDestroy {
  roomList: ChatRoom[];
  currentRoom: ChatRoom;
  lastConnectionDate: Date;
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private state: AppStateService,
    private chat: ChatService,
    private auth: AuthService
  ) {}

  ngOnInit(): void {
    this.getChatRooms();
    this.getMessages();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.state.setCurrentCanal(null);
  }

  openModal(ev?: any) {
    const dialogRef = this.dialog.open(IntranetCreateComponent, {
      width: '800px',
      data: ev,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getChatRooms();
        this.setCanal(null)
      }
    });
  }

  setCanal(room: ChatRoom) {
    this.currentRoom = room;
    this.state.setCurrentCanal(room);
    const roomWithMessageId = this.roomList.findIndex(
      (el) => el._id === room._id
    );
    this.roomList[roomWithMessageId].haveUnreadMessage = false;
  }

  getChatRooms() {
    forkJoin([this.chat.getAllChatRooms(), this.auth.getDetails()])
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(([rooms, user]) => {
        const lastConnectionDate = user ? new Date(user.lastConnectionDate) : new Date();
        rooms.forEach((room, index) => {
          const lastMessageDate = room.updatedAt ? new Date(room.updatedAt) : new Date();
          if (lastConnectionDate < lastMessageDate) {
            rooms[index].haveUnreadMessage = true;
          }
        })
        this.roomList = rooms;
        rooms.forEach((el) => {
          this.chat.joinRoom(el._id);
        });
      });
  }

  getMessages() {
    this.chat
      .getMessage()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((data: any) => {
        if (data.room !== this.currentRoom?._id) {
          const roomWithMessageId = this.roomList.findIndex(
            (el) => el._id === data.room
          );
          this.roomList[roomWithMessageId].haveUnreadMessage = true;
        }
      });
  }

  isSelectedRoom(room: ChatRoom): boolean {
    return this.currentRoom === room;
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntranetListComponent } from './intranet-list.component';

describe('IntranetListComponent', () => {
  let component: IntranetListComponent;
  let fixture: ComponentFixture<IntranetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntranetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntranetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

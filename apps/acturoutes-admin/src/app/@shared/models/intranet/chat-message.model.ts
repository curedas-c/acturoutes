export class ChatMessage {
  date: Date;
  type: string;
  text: string;
  reply?: boolean;
  user?: {
    name: string;
    avatar?: string;
  };
  files?: string[];
  quote?: string;

  constructor(options: {
    date: Date;
    type: string;
    text: string;
    reply?: boolean;
    user?: {
      name: string;
      avatar?: string;
    };
    files?: string[];
    quote?: string;
  }) {
    this.date = options.date;
    this.type = options.type;
    this.text = options.text;
    this.reply = options.reply;
    this.user = options.user;
    this.files = options.files;
    this.quote = options.quote;
  }
}

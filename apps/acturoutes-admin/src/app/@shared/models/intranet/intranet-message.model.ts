export class IntranetMessage {
  _id: string;
  text: string;
  date: string;
  reply: boolean;
  type: 'file' | 'text';
  files: File[];
  user: {
    name: string;
    avatar: string;
  };

  constructor(options: {
    _id: string;
    text: string;
    date: string;
    reply: boolean;
    type: 'file' | 'text';
    files: File[];
    user: {
      name: string;
      avatar: string;
    };
  }) {
    this._id = options._id;
    this.reply = options.reply || false;
    this.date = options.date;
    this.text = options.text;
    this.type = options.type;
    this.files = options.files;
    this.user = options.user;
  }
}

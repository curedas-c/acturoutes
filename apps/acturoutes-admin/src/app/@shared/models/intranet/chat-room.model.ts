export class ChatRoom {
  _id: string;
  name: string;
  admin: string[];
  createdAt: string;
  updatedAt: string;
  haveUnreadMessage: boolean;

  public constructor(options: {
    _id: string;
    name: string;
    admin: string[];
    createdAt: string;
    updatedAt: string;
    haveUnreadMessage: boolean;
  }) {
    this._id = options._id;
    this.name = options.name;
    this.admin = options.admin;
    this.createdAt = options.createdAt;
    this.updatedAt = options.updatedAt;
    this.haveUnreadMessage = options.haveUnreadMessage || false;
  }
}

export class Ad {
    _id: string;
    image: string;
    type: 'square' | 'rectangle';
    position: 'top' | 'bottom';
    link: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    image: string;
    type: 'square' | 'rectangle';
    position: 'top' | 'bottom';
    link: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.image = options.image;
    this.type = options.type;
    this.link = options.link;
    this.createdAt = options.createdAt;
    this.position = options.position;
  }
}

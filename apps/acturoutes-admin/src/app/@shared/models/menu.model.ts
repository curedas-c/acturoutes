export interface menu {
    path: string;
    menu: {
        title: string;
        icon?: string;
    },
    permissions?: string[]
}
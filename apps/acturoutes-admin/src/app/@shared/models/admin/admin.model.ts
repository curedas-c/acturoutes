export class Admin {
    _id: string;
    name: string;
    email: string;
    role: string;
    password: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    name: string;
    email: string;
    role: string;
    password: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.name = options.name;
    this.email = options.email;
    this.role = options.role;
    this.password = options.password;
    this.createdAt = options.createdAt;
  }
}

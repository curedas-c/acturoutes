export class Traffic {
    _id: string;
    date: string;
    message: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    date: string;
    message: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.date = options.date;
    this.message = options.message;
    this.createdAt = options.createdAt;
  }
}

export interface tableColumn {
    label: string;
    name: string;
    isDate?: boolean;
}
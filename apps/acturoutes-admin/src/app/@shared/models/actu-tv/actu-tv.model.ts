export class ActuTv {
    link: string;
    image: string;
    title: string;
    description: string;

  constructor(options: {
    link: string;
    image: string;
    title: string;
    description: string;
  }) {
    this.title = options.title;
    this.description = options.description;
    this.image = options.image;
    this.link = options.link;
  }
}

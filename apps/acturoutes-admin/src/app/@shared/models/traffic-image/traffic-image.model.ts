export class TrafficImage {
    _id: string;
    title: string;
    image: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    title: string;
    image: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.title = options.title;
    this.image = options.image;
    this.createdAt = options.createdAt;
  }
}

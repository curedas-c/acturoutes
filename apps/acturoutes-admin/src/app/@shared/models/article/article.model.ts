export class Article {
    _id: string;
    title: string;
    subtitle: string;
    type: string;
    isEvent: boolean;
    isHeadline: boolean;
    isBigFormat: boolean;
    description: string;
    main_image: string;
    thumbnail_image: string;
    publish_date: string;
    slug: string;
    content: any;
    legend: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    title: string;
    subtitle: string;
    type: string;
    isEvent: boolean;
    isHeadline: boolean;
    isBigFormat: boolean;
    description: string;
    main_image: string;
    thumbnail_image: string;
    publish_date: string;
    slug: string;
    content: any;
    legend: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.title = options.title;
    this.subtitle = options.subtitle;
    this.type = options.type;
    this.slug = options.slug;
    this.isEvent = options.isEvent;
    this.isHeadline = options.isHeadline;
    this.isBigFormat = options.isBigFormat;
    this.content = options.content;
    this.main_image = options.main_image;
    this.description = options.description;
    this.thumbnail_image = options.thumbnail_image;
    this.publish_date = options.publish_date;
    this.legend = options.legend;
    this.createdAt = options.createdAt;
  }
}

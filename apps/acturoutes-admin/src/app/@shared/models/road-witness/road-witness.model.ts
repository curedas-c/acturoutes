export class RoadWitness {
    _id: string;
    title: string;
    slug: string;
    image: string;
    content: string;
    author_name: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    title: string;
    slug: string;
    image: string;
    content: string;
    author_name: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.title = options.title;
    this.image = options.image;
    this.slug = options.slug;
    this.content = options.content;
    this.author_name = options.author_name;
    this.createdAt = options.createdAt;
  }
}

export interface ConfirmInterface {
  type?: string;
  title?: string;
  image?: boolean;
  imageLink?: string;
  isHeader?: boolean;
  message?: string;
  btnYes?: string;
  btnNo?: string;
  data?: any;
}

export enum CONFIRMATION_TYPE {
  CONFIRM = 'confirm',
  SUCCESS = 'success',
  ERROR = 'error',
  CANCEL = 'cancel'
}

export class ConfirmDialogModel {
  constructor(
    public type: string,
    public title: string,
    public image: boolean,
    public imageLink: string,
    public isHeader: boolean,
    public message: string,
    public btnYes: string,
    public btnNo: string,
    public data: any
  ) {}
}

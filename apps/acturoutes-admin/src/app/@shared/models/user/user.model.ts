export class User {
    _id: string;
    name: string;
    email: string;
    password: string;
    position: string;
    createdAt: string;

  constructor(options: {
    _id: string;
    name: string;
    email: string;
    password: string;
    position: string;
    createdAt: string;
  }) {
    this._id = options._id;
    this.name = options.name;
    this.email = options.email;
    this.password = options.password;
    this.position = options.position;
    this.createdAt = options.createdAt;
  }
}

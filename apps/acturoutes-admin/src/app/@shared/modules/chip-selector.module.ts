import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MatChipsModule } from '@angular/material/chips';
import { ChipSelectorComponent } from '@shared/components/chip-selector/chip-selector.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { DragDropModule } from '@angular/cdk/drag-drop';

const IMPORTS = [
  MatChipsModule,
  MatButtonModule,
  MatIconModule,
  DragDropModule
];
@NgModule({
  declarations: [ChipSelectorComponent],
  imports: [CommonModule, ...IMPORTS],
  exports: [ChipSelectorComponent],
  providers: [],
})
export class ChipSelectorModule {}

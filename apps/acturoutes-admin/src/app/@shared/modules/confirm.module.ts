import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmService } from '@core/services/confirm.service';
import { DialogConfirmComponent } from '../components/dialog-confirm/dialog-confirm.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
  declarations: [DialogConfirmComponent],
  imports: [CommonModule, MatButtonModule],
  providers: [
    ConfirmService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ],
})
export class ConfirmModule {}

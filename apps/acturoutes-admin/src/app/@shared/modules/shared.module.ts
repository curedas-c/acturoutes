import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ErrorLoadingComponent } from '../components/error-loading/error-loading.component';
import { LoaderComponent } from '../components/loader/loader.component';

const IMPORTS = [
  FormsModule,
  ReactiveFormsModule,
  MatIconModule,
  MatButtonModule,
  MatListModule,
  MatCardModule,
  MatStepperModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatTooltipModule
];
@NgModule({
  declarations: [ErrorLoadingComponent, LoaderComponent],
  imports: [CommonModule, ...IMPORTS],
  exports: [...IMPORTS, ErrorLoadingComponent, LoaderComponent],
  providers: [],
})
export class SharedModule {}

export const editorToolbar = 
`
<span class="ql-formats">
<select class="ql-size" title="Taille de police">
  <option value="small">Petit</option>
  <option value="normal" selected>Normal</option>
  <option value="large">Grand</option>
  <option value="huge">Très Grand</option>
</select>
<select class="ql-header" title="Niveau de Titre">
  <option value="1">Titre 1</option>
  <option value="2">Titre 2</option>
  <option value="3">Titre 3</option>
  <option value="4">Titre 4</option>
  <option value="5">Titre 5</option>
  <option value="6" selected>Titre 6</option>
</select>
</span>

<span class="ql-formats">
<select class="ql-align" title="'Alignement'">
  <option selected>Aucun</option>
  <option value="center">Centre</option>
  <option value="right">Droit</option>
  <option value="justify">Gauche</option>
</select>
<button class="ql-indent" value="-1" title="Diminuer le retrait"></button>
<button class="ql-indent" value="+1" title="Augmenter le retrait"></button>
</span>

<span class="ql-formats">
<!-- <select class="ql-font">
<option selected value="roboto">Roboto</option>
</select> -->
<select class="ql-color" title="Couleur de Police"></select>
<select class="ql-background" title="Couleur de Remplissage"></select>
</span>

<span class="ql-formats">
<button class="ql-bold"></button>
<button class="ql-italic"></button>
<button class="ql-underline"></button>
<button class="ql-strike"></button>
</span>

<span class="ql-formats">
<button class="ql-list" value="ordered"></button>
<button class="ql-list" value="bullet"></button>
</span>

<span class="ql-formats">
<button class="ql-blockquote" title="Citation"></button>
<button class="ql-script" value="sub" title="Indice"></button>
<button class="ql-script" value="super" title="Exposant"></button>
<button class="ql-formula" title="Formule"></button>
<button class="ql-code-block" title="Block de code"></button>
</span>

<span class="ql-formats">
<button class="ql-link" title="Transformer en lien"></button>
<button class="ql-image" title="Insérer une image"></button>
<button class="ql-video" title="Insérer une vidéo"></button>
</span>

<span class="ql-formats">
<button class="ql-clean" title="Effacer Tous les styles"></button>
</span>
</div>
`;
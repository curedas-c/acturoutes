//ngx-quill
import * as QuillNamespace from 'quill';
import ImageResize from 'quill-image-resize-module';
let Quill: any = QuillNamespace;
Quill.register('modules/imageResize', ImageResize);
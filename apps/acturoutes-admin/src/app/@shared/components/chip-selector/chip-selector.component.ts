import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { inOutAnimation } from '../../animations/inOutAnimation';

@Component({
  selector: 'app-chip-selector',
  templateUrl: './chip-selector.component.html',
  styleUrls: ['./chip-selector.component.scss'],
  animations: [inOutAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChipSelectorComponent implements OnInit {

  @Input() itemList: any[];
  @Input() selectedList: any[] = [];
  @Input() observableList: Observable<any[]>;
  @Input() multiSelect: boolean = true;
  @Input() count: number = 3;
  @Input() buttonDisabled = false;
  @Output() onSelect = new EventEmitter<any>();

  selectedItem: any;
  constructor() { }

  ngOnInit(): void {
  }

  setSelectedItem(item: any) {
    this.selectedItem = item;
  }

  setItem(item: any) {
    if (this.isInList(item)) {
      this.selectedList = this.selectedList.filter((el) => el._id !== item._id);
    } else if (this.selectedList.length !== this.count) {
      this.selectedList = [...this.selectedList, item];
    }
  }

  isSelectedItem(item: any): boolean {
    return this.selectedItem === item;
  }

  isInList(item: any): boolean {
    return this.selectedList.some(el => el._id === item._id);
  }

  emit() {
    this.onSelect.emit(this.selectedList.map(el => el._id));
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.selectedList, event.previousIndex, event.currentIndex);
  }

  get listIsEmpty() {
    return this.itemList.length === 0;
  }

  get haveItems() {
    return this.itemList?.length !== 0 || this.selectedList?.length === this.count;
  }
}

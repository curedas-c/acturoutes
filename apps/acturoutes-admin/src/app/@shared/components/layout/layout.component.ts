import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { menu } from '@shared/models/menu.model';
import { AuthService } from '@core/services/auth.service';
import { MAIN_MENU } from '@core/constants/menu';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
  opened: boolean;
  menuButtonVisible: boolean;
  menu: menu[] = MAIN_MENU;

  constructor(public breakpointObserver: BreakpointObserver, 
    private auth: AuthService,) {}

  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(min-width: 768px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.menuButtonVisible = false;
          this.opened = true;
        } else {
          this.menuButtonVisible = true;
          this.opened = false;
        }
      });
  }

  closeMenu() {
    if (this.menuButtonVisible) {
      this.opened = false;
    }
  }

  disconnect() {
    this.auth.logout().subscribe();
  }

}

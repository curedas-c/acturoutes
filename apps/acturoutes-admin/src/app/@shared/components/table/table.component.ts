/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Output, EventEmitter } from '@angular/core';

import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

import { tableColumn } from '@shared/models/table/tableColumn.model';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';

import { fromEvent, merge, Subject } from 'rxjs';
import { inOutAnimation } from '../../animations/inOutAnimation';
import {
  debounceTime,
  distinctUntilChanged,
  finalize,
  map,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [inOutAnimation]
})
export class TableComponent implements AfterViewInit, OnChanges {
  @Input() title: string;
  @Input() displayedColumns: tableColumn[] = [];
  @Input() columns: string[] = [];
  @Input() dataService: any;
  @Input() exportName = 'excel_export';
  @Input() hiddenColumnsIndexes: number[] = [0];

  @Input() selectable = false;
  @Input() editable = false;
  @Input() searchable = true;
  @Input() requestParams: any = {};
  @Input() dataRoute: string;
  dataSource: MatTableDataSource<any> = new MatTableDataSource([]);
  selection = new SelectionModel<any>(true, []);
  pageEvent: PageEvent;

  totalItems = 0;
  itemsPerPage = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @Output() edit = new EventEmitter<any>();
  @Output() remove = new EventEmitter<any>();
  private unsubscribe$ = new Subject<void>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('searchInput') input: ElementRef;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.requestParams.firstChange) {
      this.getServerData();
    }
  }

  ngAfterViewInit() {
    this.listenToSearchInput(); // server-side search
    this.listenToSortChanges();
    this.listenSortAndPaginator();
  }

  getServerData() {
    this.isLoadingResults = true;
    this.dataService!.getTableData({ params: this.requestParams, ...(this.dataRoute && {route: this.dataRoute}) })
      .pipe(
        takeUntil(this.unsubscribe$),
        map((data: any) => {
          this.totalItems = data.total_items;
          this.itemsPerPage = data.items.length;
          return data.items;
        }),
        finalize(() => {
          this.isLoadingResults = false;
          this.cdRef.detectChanges();
        })
      )
      .subscribe((data) => {
        this.dataSource = new MatTableDataSource<any>(data);
        if (data.length < 1) {
          this.isRateLimitReached = true;
        } else {
          this.isRateLimitReached = false;
        }
      });
  }

  listenToSearchInput() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        takeUntil(this.unsubscribe$),
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => {
          const value = this.input.nativeElement.value;
          if (value.length > 0 && value.length < 3) {
            return;
          } else {
            this.paginator._changePageSize(this.itemsPerPage); // force table reload
          }
        })
      )
      .subscribe();
  }

  listenToSortChanges() {
    this.sort.sortChange.pipe(takeUntil(this.unsubscribe$)).subscribe(res => {
      this.paginator.pageIndex = 0;
      this.requestParams = {
        ...this.requestParams,
        sortBy: this.sort.active,
        order: this.sort.direction,
      };
      this.cdRef.detectChanges();
    })
  }

  listenSortAndPaginator() {
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(
        takeUntil(this.unsubscribe$),
        startWith({}),
        tap(() => {
          this.requestParams = {
            ...this.requestParams,
            filter: this.input.nativeElement.value,
            limit: this.paginator.pageSize,
            page: this.paginator.pageIndex + 1,
          };
        })
      )
      .subscribe(() => {
        this.getServerData();
      });
  }

  // Search
  search(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /*
   * Events
   */

  onEdit(item: any) {
    this.edit.emit(item);
  }

  onRemove() {
    const selectedIDs = this.selection.selected.map(selection => selection._id);
    this.remove.emit(selectedIDs);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

import { Component, OnInit,  Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CONFIRMATION_TYPE, ConfirmDialogModel } from '@shared/models/confirm/confirm.model';

@Component({
  selector: 'zip-business-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss']
})
export class DialogConfirmComponent implements OnInit {
  type: string;
  title: string;
  image: boolean;
  imageLink: string;
  isHeader: boolean;
  message: string;
  btnYes: string;
  btnNo: string;

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ConfirmDialogModel
  ) {
    this.type = data.type;
    this.title = data.title;
    this.image = data.image;
    this.imageLink = data.imageLink;
    this.isHeader = data.isHeader;
    this.message = data.message;
    this.btnYes = data.btnYes;
    this.btnNo = data.btnNo;
  }

  ngOnInit() {}

  onConfirm(): void {
    this.dialogRef.close(true);
  }

  onDismiss(): void {
    this.dialogRef.close(false);
  }

  get getHeaderStyle(): string {
    let headerStyle;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        headerStyle = 'header-success';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        headerStyle = 'header-confirm';
        break;
      case CONFIRMATION_TYPE.ERROR:
        headerStyle = 'header-error';
        break;
      default:
        headerStyle = 'header-default';
    }

    return headerStyle;
  }

  get getHeaderText() {
    let defaultTitle;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        defaultTitle = 'Validation succes';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        defaultTitle = 'Validation confirmation';
        break;
      case CONFIRMATION_TYPE.ERROR:
        defaultTitle = 'Validation erreur';
        break;
      default:
        defaultTitle = 'Validation';
    }

    return this.title ? this.title : defaultTitle;
  }

  get getIconType(): string {
    let iconType;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        iconType = 'icon-success';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        iconType = 'icon-warning';
        break;
      case CONFIRMATION_TYPE.ERROR:
        iconType = 'icon-error';
        break;
      default:
        iconType = 'icon-default';
    }

    return iconType;
  }

  get getIcon(): string {
    let icon;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        icon = 'done';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        icon = 'warning';
        break;
      case CONFIRMATION_TYPE.ERROR:
        icon = 'error';
        break;
      default:
        icon = 'apps';
    }

    return icon;
  }

  get getMessage() {
    const defaultMessage = `Cette action est irreversible, voulez vous continuer ?`;
    return this.message ? this.message : defaultMessage;
  }

  get getConfirmBtn(): string {
    let confirmBtn;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        confirmBtn = 'btn btn-add-success';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        confirmBtn = 'btn btn-add-warning';
        break;
      case CONFIRMATION_TYPE.ERROR:
        confirmBtn = 'btn btn-add-danger';
        break;
      case CONFIRMATION_TYPE.CANCEL:
        confirmBtn = 'btn btn-add-danger';
        break;
      default:
        confirmBtn = 'btn btn-add-default';
    }

    return confirmBtn;
  }

  get getConfirmOutlineBtn(): string {
    let confirmOutlineBtn;
    switch (this.type) {
      case CONFIRMATION_TYPE.SUCCESS:
        confirmOutlineBtn = 'btn btn-add-success-outline';
        break;
      case CONFIRMATION_TYPE.CONFIRM:
        confirmOutlineBtn = 'btn btn-add-warning-outline';
        break;
      case CONFIRMATION_TYPE.ERROR:
        confirmOutlineBtn = 'btn btn-add-danger-outline';
        break;
      case CONFIRMATION_TYPE.CANCEL:
        confirmOutlineBtn = 'btn btn-add-danger-outline';
        break;
      default:
        confirmOutlineBtn = 'btn btn-add-default-outline';
        break;
    }

    return confirmOutlineBtn;
  }

  onHideHeader() {
    return this.isHeader ? true : false;
  }

  get getValidationMsg() {
    return this.btnYes ? this.btnYes : 'Valider';
  }

  get getCancelMsg() {
    return this.btnNo ? this.btnNo : 'Annuler';
  }
}

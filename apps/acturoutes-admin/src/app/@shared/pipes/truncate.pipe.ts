import { Pipe, PipeTransform } from "@angular/core";

@Pipe({name: 'truncateAfter'})
export class TruncateAfterPipe implements PipeTransform {
  transform(value: string, maxLength: number = 36): string {
    if (value.constructor === String && value.length > maxLength) {
        const minifiedText = value.slice(0, maxLength) + '...';
        return minifiedText;
      }
    return value;
  }
}
import { AfterViewInit, Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@core/services/auth.service';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit, AfterViewInit, OnDestroy {

  nameForm: FormGroup;
  mailForm: FormGroup;
  passwordForm: FormGroup;
  isButtonDisabled = false;

  private unsubscribe$ = new Subject<void>();
  constructor(private fb: FormBuilder, private auth: AuthService) { }
  ngAfterViewInit(): void {
    this.getDetails();
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.nameForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
    });
    this.mailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
    this.passwordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  getDetails() {
    this.auth
      .getDetails()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (res) => {
          this.nameForm.controls.name.patchValue(res.name);
          this.mailForm.controls.email.patchValue(res.email);
        }
      );
  }

  updateName() {
    this.switchButtonState();
    this.auth
      .updateData(this.nameForm.value)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
      .subscribe();
  }

  updateEmail() {
    this.switchButtonState();
    this.auth
      .updateData(this.mailForm.value)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
      .subscribe();
  }

  updatePassword() {
    this.switchButtonState();
    this.auth
      .updatePassword(this.passwordForm.value)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
      .subscribe((res) => {
        this.passwordForm.reset();
      });
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  get haveData() {
    return !!this.mailForm.controls.email.value;
  }

}

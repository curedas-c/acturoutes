import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { User } from '@shared/models/user/user.model';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserCreateComponent } from '../user-create/user-create.component';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent {
  dataService = new UserService(this.apiService);
  params = {};
  displayedColumns: tableColumn[] = [
    {
      name: 'name',
      label: 'Nom',
    },
    {
      name: 'email',
      label: 'Email',
    },
    {
      name: 'position',
      label: 'Poste',
    },
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true
    }
  ];
  columns = [
    'edit_action',
    'name',
    'email',
    'position',
    'createdAt'
  ];
  private unsubscribe$ = new Subject<void>();
  constructor(public dialog: MatDialog, private apiService: ApiService, private userService: UserService) { }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  editItem(ev: User) {
    const dialogRef = this.dialog.open(UserCreateComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params = {...this.params};
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.userService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = {...this.params};
      });
  }

}

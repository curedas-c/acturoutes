import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserService } from './shared/services/user.service';
import { SharedModule } from '@shared/modules/shared.module';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { MatTabsModule } from '@angular/material/tabs';
import { TableModule } from '@shared/modules/table.module';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserListComponent } from './user-list/user-list.component';
import { NgxPermissionsModule } from 'ngx-permissions';


@NgModule({
  declarations: [
    UserComponent,
    UserCreateComponent,
    UserListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MatDialogModule,
    ConfirmModule,
    MatTabsModule,
    TableModule,
    UserRoutingModule,
    NgxPermissionsModule.forChild()
  ],
  providers: [
    UserService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ]
})
export class UserModule { }

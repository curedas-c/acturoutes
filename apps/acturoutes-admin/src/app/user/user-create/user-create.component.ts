import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmService } from '@core/services/confirm.service';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { UserService } from '../shared/services/user.service';
import { addControl } from '@shared/utils/formGroupModifier';
import { slideFromRight } from '@shared/animations/inOutAnimation';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
  animations: [slideFromRight]
})
export class UserCreateComponent implements OnInit {
  defaultForm: FormGroup = new FormGroup({});
  passwordForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  userList$: Observable<any>;
  isPasswordForm = false;
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<UserCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private userService: UserService,
    private auth: AuthService
  ) {
    this.currentData = this.data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      name: [this.currentData?.name || '', [Validators.required]],
      email: [this.currentData?.email || '', [Validators.required]],
      position: [this.currentData?.position || '', [Validators.required]]
    });
    if (!this.currentData) {
      this.defaultForm = addControl(this.defaultForm, 'password');
    }
    this.passwordForm = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  onDelete(ID: number) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression de Livreur',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer ce compte ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON'
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(dialog => {
        if (dialog) {
          this.switchButtonState();
          this.userService
            .deleteItem([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.userService
      .setItem(this.defaultForm.value, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        })
      )
      .subscribe();
  }

  updatePassword() {
    this.switchButtonState();
    this.userService
      .updatePassword(this.currentData._id, this.passwordForm.value)
      .pipe(takeUntil(this.unsubscribe$), finalize(() => { this.switchButtonState() }))
      .subscribe((res) => {
        this.passwordForm.reset();
      });
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  switchForms() {
    this.isPasswordForm = !this.isPasswordForm;
  }

}

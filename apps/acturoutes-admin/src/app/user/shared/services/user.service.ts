import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { User } from '@shared/models/user/user.model';
import { ResetPasswordCredentials } from '@shared/models/auth/resetPasswordCredentials.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class UserService {
  constructor(private _apiService: ApiService) { }

  getTableData(options: { params?: any}): Observable<DataTable<User>> {
    const { params } = options;
    return this._apiService
      .get(`${ENDPOINTS.user}`, params)
      .pipe(
        map((response: any) => {
          const mapped: User[] = response.items.map((res) => {
            return new User(res);
          });

          return {
            items: mapped,
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getItem(uuid: string | number, params?: any): Observable<User> {
    return this._apiService
      .get(`${ENDPOINTS.user}/${uuid}`, params)
      .pipe(
        map((response: any) => {
          return new User(response.data);
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(value: any, uuid?: string | number, params?: any): Observable<User> {
    const url = !!uuid ? `${ENDPOINTS.user}/${uuid}` : `${ENDPOINTS.user}`;
    const request = !!uuid ? this._apiService.patch(url, value, params) : this._apiService.post(url, value, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.userDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getAllItems(params?: any): Observable<User[]> {
    return this._apiService
      .get(`${ENDPOINTS.user}`, params)
      .pipe(
        map((response: any) => {
          const mapped: User[] = response.items.map((res) => {
            return new User(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  public updatePassword(ID: string, credentials: ResetPasswordCredentials): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.updatePassword}/${ID}`, credentials).pipe(
      map((response: any) => response),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }
}

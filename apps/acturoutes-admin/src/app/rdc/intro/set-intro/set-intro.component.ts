import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MatDialog } from '@angular/material/dialog';
import { finalize, Subject, takeUntil, tap } from 'rxjs';
import { PubService } from '../../../pubs/shared/services/pub.service';

@Component({
  selector: 'acturoutes-set-intro',
  templateUrl: './set-intro.component.html',
  styleUrls: ['./set-intro.component.scss'],
})
export class SetIntroComponent implements OnInit {
  @ViewChild('form') form: TemplateRef<any>;
  defaultForm: FormGroup = new FormGroup({});
  isButtonDisabled = false;
  defaultImage: string[];

  oldItem$ = this.pubService.getLastIntro({
    filterOn: 'isRdcImage',
    filterOnValue: true,
  });

  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private fb: FormBuilder,
    private pubService: PubService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      isRdcImage: [true, [Validators.required]],
      link: ['', Validators.required],
      enabled: [true, Validators.required],
      image: [null, Validators.required],
    });
  }

  sendForm() {
    this.switchButtonState();
    this.pubService
      .setIntro(this.defaultForm)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => this.switchButtonState()),
        tap({
          next: () => {
            this.oldItem$ = this.pubService.getLastIntro({
              filterOn: 'isRdcMessage',
              filterOnValue: true,
            });
            this.dialog.closeAll();
          }
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  update(intro: any, change: MatCheckboxChange) {
    this.switchButtonState();
    this.pubService
      .setIntroStatus(intro, change.checked)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => this.switchButtonState())
      )
      .subscribe();
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.image.patchValue(null);
    }
  }

  create() {
    const dialogRef = this.dialog.open(this.form, {
      maxWidth: '1000px',
      maxHeight: '80vh',
      position:  {
        top: '6%'
      }
    });
  }
}

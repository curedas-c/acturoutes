import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatePubsComponent } from './create-pubs.component';

describe('CreatePubsComponent', () => {
  let component: CreatePubsComponent;
  let fixture: ComponentFixture<CreatePubsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreatePubsComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(CreatePubsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppStateService } from '@core/services/app-state.service';
import { ConfirmService } from '@core/services/confirm.service';
import { environment } from '@environments/environment';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { addControl } from '@shared/utils/formGroupModifier';
import { finalize, Observable, Subject, takeUntil, tap } from 'rxjs';
import { PubService } from '../../shared/services/pub.service';

@Component({
  selector: 'rdc-create-pubs',
  templateUrl: './create-pubs.component.html',
  styleUrls: ['./create-pubs.component.scss'],
})
export class CreatePubsComponent implements OnInit {
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  placeList$: Observable<any>;
  defaultImage: string[];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<CreatePubsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private adService: PubService,
    private state: AppStateService
  ) {
    this.currentData = this.data;
    this.defaultImage = this.data?.image ? [`${environment.rootUrl}/${this.data?.image}`] : null;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      type: [this.currentData?.type || '', [Validators.required]],
      position: [this.currentData?.position || '', [Validators.required]],
      link: [this.currentData?.link || ''],
    });
    const validator = this.currentData ? [] : [Validators.required];
    this.defaultForm = addControl(this.defaultForm, 'image', null, validator);
  }

  onDelete(ID: number) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression de Livreur',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer cette affiche ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON'
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(dialog => {
        if (dialog) {
          this.switchButtonState();
          this.adService
            .deleteItem([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.adService
      .setItem(this.defaultForm, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        }),
        tap({
          next: () => this.state.triggerUpdate({...this.defaultForm.value, id: this.currentData?._id || ''})
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.image.patchValue(null);
    }
  }

}

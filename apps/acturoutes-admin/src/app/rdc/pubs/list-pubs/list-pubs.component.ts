import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { filter, Subject, takeUntil, tap } from 'rxjs';
import { PubService } from '../../shared/services/pub.service';
import { RdcAd } from '../../../../../../acturoutes/src/app/@core/schemas/rdc-ad.schema';
import { CreatePubsComponent } from '../create-pubs/create-pubs.component';
import { AppStateService } from '@core/services/app-state.service';

@Component({
  selector: 'acturoutes-list-pubs',
  templateUrl: './list-pubs.component.html',
  styleUrls: ['./list-pubs.component.scss'],
})
export class ListPubsComponent implements OnInit {
  dataService = new PubService(this.apiService);
  params = {};
  displayedColumns: tableColumn[] = [
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true
    },
    {
      name: 'type',
      label: 'Type',
    },
    {
      name: 'link',
      label: 'Lien',
    }
  ];
  columns = [
    'edit_action',
    'createdAt',
    'type',
    'link'
  ];
  private unsubscribe$ = new Subject<void>();
  constructor(public dialog: MatDialog, private apiService: ApiService, private adService: PubService, 
    private state: AppStateService) { }

  ngOnInit() {
    this.state.update$.pipe(takeUntil(this.unsubscribe$), filter(Boolean), tap(() => this.params = { ...this.params })).subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  editItem(ev: RdcAd) {
    const dialogRef = this.dialog.open(CreatePubsComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params = {...this.params};
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.adService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = {...this.params};
      });
  }
}

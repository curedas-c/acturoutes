import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TrafficImageComponent } from './traffic/traffic-image/traffic-image.component';
import { ListArticleComponent } from './article/list-article/list-article.component';
import { SetIntroComponent } from './intro/set-intro/set-intro.component';
import { ListPubsComponent } from './pubs/list-pubs/list-pubs.component';
import { RdcWrapperComponent } from './rdc-wrapper/rdc-wrapper.component';
import { ListSignalComponent } from './signal/list-signal/list-signal.component';
import { ListTrafficComponent } from './traffic/list-traffic/list-traffic.component';

const routes: Routes = [
  {
    path: '',
    component: RdcWrapperComponent,
    children: [
      {
        path: '',
        redirectTo: 'article',
        pathMatch: 'full',
      },
      {
        path: 'article',
        component: ListArticleComponent,
      },
      // {
      //   path: 'intro',
      //   component: SetIntroComponent,
      // },
      {
        path: 'pubs',
        component: ListPubsComponent,
      },
      {
        path: 'signal',
        component: ListSignalComponent,
      },
      {
        path: 'traffic',
        component: ListTrafficComponent,
      },
      {
        path: 'traffic-image',
        component: TrafficImageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RdcRoutingModule {}

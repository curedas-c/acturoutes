import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfirmService } from '@core/services/confirm.service';
import { environment } from '@environments/environment';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { editorToolbar } from '@shared/utils/editorToolbar';
import { addControl } from '@shared/utils/formGroupModifier';
import { finalize, Observable, Subject, takeUntil } from 'rxjs';
import { ArticleService } from '../../../article/shared/services/article.service';

@Component({
  selector: 'acturoutes-create-article',
  templateUrl: './create-article.component.html',
  styleUrls: ['./create-article.component.scss'],
})
export class CreateArticleComponent implements OnInit {
  editorToolbar: any;
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  adminList$: Observable<any>;
  defaultImage: string[];

  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<CreateArticleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private articleService: ArticleService,
    private sanitizer: DomSanitizer
  ) {
    this.editorToolbar = this.sanitizer.bypassSecurityTrustHtml(editorToolbar);
    this.currentData = this.data;
    this.defaultImage = this.data?.main_image ? [`${environment.rootUrl}/${this.data?.main_image}`] : null;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      title: [
        this.currentData?.title || '',
        [Validators.required, Validators.maxLength(97)],
      ],
      subtitle: [this.currentData?.subtitle || '', [Validators.maxLength(28)]],
      // isEvent: [this.currentData?.isEvent || false, [Validators.required]],
      // isEnglishArticle: [this.currentData?.isEvent || false, [Validators.required]],
      isRdcArticle: [true],
      isEnglishArticle: [this.currentData?.isEvent || false, [Validators.required]],
      description: [
        this.currentData?.description || '',
        [Validators.required, Validators.maxLength(250)],
      ],
      content: [this.currentData?.content || ''],
      legend: [this.currentData?.legend || ''],
    });
    const validator = this.currentData ? [] : [Validators.required];
    this.defaultForm = addControl(this.defaultForm, 'main_image', null, validator);
  }

  onDelete(ID: string) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression d\'article',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer cet article ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON',
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dialog) => {
        if (dialog) {
          this.switchButtonState();
          this.articleService
            .deleteItem({ uuid: [ID]})
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.articleService
      .setItem({ item: this.defaultForm, uuid: this.currentData?._id})
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.main_image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.main_image.patchValue(null);
    }
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafficImageComponent } from './traffic-image.component';

describe('TrafficImageComponent', () => {
  let component: TrafficImageComponent;
  let fixture: ComponentFixture<TrafficImageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TrafficImageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TrafficImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

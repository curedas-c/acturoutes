import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppStateService } from '@core/services/app-state.service';
import { ConfirmService } from '@core/services/confirm.service';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { finalize, Subject, takeUntil, tap } from 'rxjs';
import { TrafficService } from '../../../traffic/shared/services/traffic.service';

@Component({
  selector: 'rdc-create-traffic',
  templateUrl: './create-traffic.component.html',
  styleUrls: ['./create-traffic.component.scss'],
})
export class CreateTrafficComponent implements OnInit {
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<CreateTrafficComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private traffic: TrafficService,
    private state: AppStateService
  ) {
    this.currentData = this.data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      message: [this.currentData?.message || '', [Validators.required]],
      isRdcMessage: ['true']
    });
  }

  onDelete(ID: number) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression de Livreur',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer ce message ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON'
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dialog) => {
        if (dialog) {
          this.switchButtonState();
          this.traffic
            .deleteItem([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.traffic
      .setItem(this.defaultForm, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        }),
        tap({
          next: () => this.state.triggerUpdate({...this.defaultForm.value, id: this.currentData?._id || ''})
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

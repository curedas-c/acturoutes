import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RdcWrapperComponent } from './rdc-wrapper.component';

describe('RdcWrapperComponent', () => {
  let component: RdcWrapperComponent;
  let fixture: ComponentFixture<RdcWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RdcWrapperComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RdcWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { AppStateService } from '@core/services/app-state.service';
import { Signal } from '@shared/models/signal/signal.model';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { filter, Subject, takeUntil, tap } from 'rxjs';
import { SignalService } from '../../../signal/shared/services/signal.service';
import { CreateSignalComponent } from '../create-signal/create-signal.component';

@Component({
  selector: 'acturoutes-list-signal',
  templateUrl: './list-signal.component.html',
  styleUrls: ['./list-signal.component.scss'],
})
export class ListSignalComponent implements OnInit {
  dataService = new SignalService(this.apiService);
  params = {
    filterOn: 'isRdcMessage'
  };
  displayedColumns: tableColumn[] = [
    {
      name: 'message',
      label: 'Message',
    },
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true,
    }
  ];
  columns = ['edit_action', 'message', 'createdAt'];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private signalService: SignalService,
    private state: AppStateService
  ) {}

  ngOnInit() {
    this.state.update$.pipe(takeUntil(this.unsubscribe$), filter(Boolean), tap(() => this.params = { ...this.params })).subscribe();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  editItem(ev: Signal) {
    const dialogRef = this.dialog.open(CreateSignalComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.params = { ...this.params };
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.signalService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = { ...this.params };
      });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSignalComponent } from './list-signal.component';

describe('ListSignalComponent', () => {
  let component: ListSignalComponent;
  let fixture: ComponentFixture<ListSignalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListSignalComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ListSignalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

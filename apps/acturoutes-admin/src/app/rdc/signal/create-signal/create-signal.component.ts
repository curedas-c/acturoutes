import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppStateService } from '@core/services/app-state.service';
import { ConfirmService } from '@core/services/confirm.service';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { finalize, Subject, takeUntil, tap } from 'rxjs';
import { SignalService } from '../../../signal/shared/services/signal.service';

@Component({
  selector: 'rdc-create-signal',
  templateUrl: './create-signal.component.html',
  styleUrls: ['./create-signal.component.scss'],
})
export class CreateSignalComponent implements OnInit {
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;

  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<CreateSignalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private signal: SignalService,
    private state: AppStateService
  ) {
    this.currentData = this.data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      message: [this.currentData?.message || '', [Validators.required]],
      isRdcMessage: ['true', [Validators.required]]
    });
  }

  onDelete(ID: number) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer ce message ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON'
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dialog) => {
        if (dialog) {
          this.switchButtonState();
          this.signal
            .deleteItem([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.signal
      .setItem(this.defaultForm.value, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        }),
        tap({
          next: () => this.state.triggerUpdate({...this.defaultForm.value, id: this.currentData?._id || ''})
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

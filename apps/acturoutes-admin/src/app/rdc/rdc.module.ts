import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RdcRoutingModule } from './rdc-routing.module';
import { CreateSignalComponent } from './signal/create-signal/create-signal.component';
import { ListSignalComponent } from './signal/list-signal/list-signal.component';
import { ListTrafficComponent } from './traffic/list-traffic/list-traffic.component';
import { CreateTrafficComponent } from './traffic/create-traffic/create-traffic.component';
import { TrafficImageComponent } from './traffic/traffic-image/traffic-image.component';
import { CreatePubsComponent } from './pubs/create-pubs/create-pubs.component';
import { ListPubsComponent } from './pubs/list-pubs/list-pubs.component';
import { CreateArticleComponent } from './article/create-article/create-article.component';
import { ListArticleComponent } from './article/list-article/list-article.component';
import { RdcWrapperComponent } from './rdc-wrapper/rdc-wrapper.component';
import { SharedModule } from '@shared/modules/shared.module';
import { MatTabsModule } from '@angular/material/tabs';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { TableModule } from '@shared/modules/table.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { QuillModule } from 'ngx-quill';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ArticleService } from '../article/shared/services/article.service';
import { PubService } from './shared/services/pub.service';
import { TrafficService } from '../traffic/shared/services/traffic.service';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  declarations: [
    CreateSignalComponent,
    ListSignalComponent,
    ListTrafficComponent,
    CreateTrafficComponent,
    TrafficImageComponent,
    CreatePubsComponent,
    ListPubsComponent,
    CreateArticleComponent,
    ListArticleComponent,
    RdcWrapperComponent,
  ],
  imports: [
    CommonModule,
    RdcRoutingModule,
    SharedModule,
    MatTabsModule,
    FileSelectorModule,
    TableModule,
    MatSlideToggleModule,
    ConfirmModule,
    QuillModule,
    MatDialogModule,
    MatExpansionModule
  ],
  providers: [
    ArticleService,
    PubService,
    TrafficService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null },
  ],
})
export class RdcModule {}

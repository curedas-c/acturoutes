import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { Ad } from '@shared/models/ads/ads.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class PubService {
  constructor(private _apiService: ApiService) { }

  getTableData(options: { params?: any}): Observable<DataTable<Ad>> {
    const { params } = options;
    return this._apiService
      .get(`${ENDPOINTS.ads}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Ad[] = response.items.map((res) => {
            return new Ad(res);
          });

          return {
            items: mapped,
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getItem(uuid: string | number, params?: any): Observable<Ad> {
    return this._apiService
      .get(`${ENDPOINTS.ads}/${uuid}`, params)
      .pipe(
        map((response: any) => {
          return new Ad(response.data);
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(item: FormGroup, uuid?: string | number, params?: any): Observable<Ad> {
    let formData = new FormData();

    Object.keys(item.controls).forEach((key) => {
      const value = item.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    const url = !!uuid ? `${ENDPOINTS.ads}/${uuid}` : `${ENDPOINTS.ads}`;

    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.adsDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getAllItems(params?: any): Observable<Ad[]> {
    return this._apiService
      .get(`${ENDPOINTS.ads}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Ad[] = response.items.map((res) => {
            return new Ad(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setIntro(data: FormGroup, params?: any): Observable<any> {
    let formData = new FormData();

    Object.keys(data.controls).forEach((key) => {
      const value = data.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    return this._apiService.post(ENDPOINTS.introImage, formData, params).pipe(
      map((response: any) => response.data)
    );
  }

  setIntroStatus(intro: any, status: boolean) {
    return this._apiService.patch(`${ENDPOINTS.introImage}/${intro._id}`, {enabled: status});
  }

  getLastIntro<T>(params?: any) {
    return this._apiService
      .get(ENDPOINTS.introImage, params)
      .pipe(
        map((response: any) => {
          return response.items[0] as T[] || [];
        })
      );
  }
}

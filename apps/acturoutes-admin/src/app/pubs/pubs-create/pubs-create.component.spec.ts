import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PubsCreateComponent } from './pubs-create.component';

describe('PubsCreateComponent', () => {
  let component: PubsCreateComponent;
  let fixture: ComponentFixture<PubsCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PubsCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PubsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

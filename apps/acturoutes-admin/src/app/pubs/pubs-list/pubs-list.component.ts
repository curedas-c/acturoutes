import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { Ad } from '@shared/models/ads/ads.model';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PubsCreateComponent } from '../pubs-create/pubs-create.component';
import { PubService } from '../shared/services/pub.service';

@Component({
  selector: 'app-pubs-list',
  templateUrl: './pubs-list.component.html',
  styleUrls: ['./pubs-list.component.scss']
})
export class PubsListComponent implements OnInit, OnDestroy {
  dataService = new PubService(this.apiService);
  params = {};
  displayedColumns: tableColumn[] = [
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true
    },
    {
      name: 'type',
      label: 'Type',
    },
    {
      name: 'link',
      label: 'Lien',
    }
  ];
  columns = [
    'edit_action',
    'createdAt',
    'type',
    'link'
  ];
  private unsubscribe$ = new Subject<void>();
  constructor(public dialog: MatDialog, private apiService: ApiService, private adService: PubService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  editItem(ev: Ad) {
    const dialogRef = this.dialog.open(PubsCreateComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params = {...this.params};
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.adService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = {...this.params};
      });
  }


}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PubsComponent } from './pubs.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'create',
    pathMatch: 'full'
  },
  {
    path: 'create',
    component: PubsComponent
  },
  {
    path: 'list',
    component: PubsComponent
  },
  {
    path: 'intro', component: PubsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PubsRoutingModule { }

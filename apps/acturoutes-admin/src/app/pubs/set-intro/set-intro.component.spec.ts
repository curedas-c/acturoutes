import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetIntroComponent } from './set-intro.component';

describe('SetIntroComponent', () => {
  let component: SetIntroComponent;
  let fixture: ComponentFixture<SetIntroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SetIntroComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SetIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

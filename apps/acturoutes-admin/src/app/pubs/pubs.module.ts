import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PubsRoutingModule } from './pubs-routing.module';
import { PubsComponent } from './pubs.component';
import { SharedModule } from '@shared/modules/shared.module';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { PubsCreateComponent } from './pubs-create/pubs-create.component';
import { PubsListComponent } from './pubs-list/pubs-list.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { TableModule } from '@shared/modules/table.module';
import { PubService } from './shared/services/pub.service';
import { SetIntroComponent } from './set-intro/set-intro.component';


@NgModule({
  declarations: [
    PubsComponent,
    PubsCreateComponent,
    PubsListComponent,
    SetIntroComponent,
  ],
  imports: [
    CommonModule,
    PubsRoutingModule,
    SharedModule,
    MatTabsModule,
    ConfirmModule,
    MatDialogModule,
    TableModule,
    FileSelectorModule
  ],
  providers: [
    PubService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ]
})
export class PubsModule { }

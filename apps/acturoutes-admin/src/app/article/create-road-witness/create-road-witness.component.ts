import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer } from '@angular/platform-browser';
import { ConfirmService } from '@core/services/confirm.service';
import { environment } from '@environments/environment';
import {
  CONFIRMATION_TYPE,
  ConfirmInterface,
} from '@shared/models/confirm/confirm.model';
import { editorToolbar } from '@shared/utils/editorToolbar';
import { addControl } from '@shared/utils/formGroupModifier';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { ArticleService } from '../shared/services/article.service';
import { RoadWitness } from '@shared/models/road-witness/road-witness.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Component({
  selector: 'app-create-road-witness',
  templateUrl: './create-road-witness.component.html',
  styleUrls: ['./create-road-witness.component.scss']
})
export class CreateRoadWitnessComponent implements OnInit, OnDestroy {
  editorToolbar: any;
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  adminList$: Observable<any>;
  defaultImage: string[];

  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<CreateRoadWitnessComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private articleService: ArticleService,
    private sanitizer: DomSanitizer
  ) {
    this.editorToolbar = this.sanitizer.bypassSecurityTrustHtml(editorToolbar);
    this.currentData = this.data;
    this.defaultImage = this.data?.image ? [`${environment.rootUrl}/${this.data?.image}`] : null;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      title: [
        this.currentData?.title || '',
        [Validators.required, Validators.maxLength(97)],
      ],
      content: [this.currentData?.content || '', [Validators.required]]
    });
    const validator = this.currentData ? [] : [Validators.required];
    this.defaultForm = addControl(this.defaultForm, 'image', null, validator);
  }

  onDelete(ID: string) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer cet élément ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON',
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((dialog) => {
        if (dialog) {
          this.switchButtonState();
          this.articleService
            .deleteItem({ uuid:[ID] })
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.articleService
      .setItem<RoadWitness>({ item: this.defaultForm, uuid: this.currentData?._id, route: ENDPOINTS.roadWitness})
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.image.patchValue(null);
    }
  }

}

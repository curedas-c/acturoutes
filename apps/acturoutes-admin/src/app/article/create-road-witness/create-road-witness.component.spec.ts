import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateRoadWitnessComponent } from './create-road-witness.component';

describe('CreateRoadWitnessComponent', () => {
  let component: CreateRoadWitnessComponent;
  let fixture: ComponentFixture<CreateRoadWitnessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateRoadWitnessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRoadWitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

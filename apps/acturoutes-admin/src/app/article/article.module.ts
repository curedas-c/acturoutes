import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticleRoutingModule } from './article-routing.module';
import { ArticleComponent } from './article.component';
import { CreateArticleComponent } from './create-article/create-article.component';
import { ListArticleComponent } from './list-article/list-article.component';

import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '@shared/modules/shared.module';
import { TableModule } from '@shared/modules/table.module';
import { MatMenuModule } from '@angular/material/menu';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { NbChatModule } from '@nebular/theme';
import { QuillModule } from 'ngx-quill';
import { BigFormatComponent } from './big-format/big-format.component';
import { HeadlineComponent } from './headline/headline.component';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { ArticleService } from './shared/services/article.service';
import { ChipSelectorModule } from '@shared/modules/chip-selector.module';
import { CreateRoadWitnessComponent } from './create-road-witness/create-road-witness.component';
import { ListRoadWitnessComponent } from './list-road-witness/list-road-witness.component';
import {MatExpansionModule} from '@angular/material/expansion';

@NgModule({
  declarations: [
    ArticleComponent,
    CreateArticleComponent,
    ListArticleComponent,
    BigFormatComponent,
    HeadlineComponent,
    CreateRoadWitnessComponent,
    ListRoadWitnessComponent,
  ],
  imports: [
    CommonModule,
    ArticleRoutingModule,
    MatTabsModule,
    SharedModule,
    TableModule,
    MatDialogModule,
    MatMenuModule,
    MatSlideToggleModule,
    FileSelectorModule,
    NbChatModule,
    ConfirmModule,
    QuillModule,
    ChipSelectorModule,
    MatExpansionModule
  ],
  providers: [
    ArticleService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null },
  ],
})
export class ArticleModule {}

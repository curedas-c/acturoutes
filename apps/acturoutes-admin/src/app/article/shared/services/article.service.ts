import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { Article } from '@shared/models/article/article.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class ArticleService {

  constructor(private _apiService: ApiService) { }
  getTableData<T = Article>(options: { params?: any, route?: string}): Observable<DataTable<T>> {
    const { params, route } = options;
    return this._apiService
      .get(`${route || ENDPOINTS.article}`, params)
      .pipe(
        map((response: any) => {
          return {
            items: response.items as T[],
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem<T = Article>(options: {item: FormGroup, uuid?: string | number, params?: any, route?: string}): Observable<T> {
    const { item, uuid, params, route } = options;
    let formData = new FormData();

    Object.keys(item.controls).forEach((key) => {
      const value = item.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    const url = !!uuid ? `${route || ENDPOINTS.article}/${uuid}` : `${route || ENDPOINTS.article}`;

    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  setHeadline(value: any, params?: any): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.headline}`, value, params).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  setBigFormat(value: any, params?: any): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.bigFormat}`, value, params).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(options: { uuid?: string[] | number [], route?: string}): Observable<any> {
    const { uuid, route } = options;
    return this._apiService.post(`${route || ENDPOINTS.articleDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getAllItems<T>(params?: any, route?: string): Observable<T[]> {
    return this._apiService
      .get(`${route || ENDPOINTS.article}`, params)
      .pipe(
        map((response: any) => {
          return response.items as T[] || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }
}

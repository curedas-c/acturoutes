import { Component, ElementRef, OnInit, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, finalize, takeUntil, tap } from 'rxjs/operators';
import { ArticleService } from '../shared/services/article.service';

@Component({
  selector: 'app-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.scss'],
})
export class HeadlineComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('searchInput') input: ElementRef;
  defaultForm: FormGroup = new FormGroup({});
  itemList: any[];
  selectedList: any[];
  isButtonDisabled = false;
  private unsubscribe$ = new Subject<void>();
  constructor(private fb: FormBuilder, private articleService: ArticleService) {}

  ngOnInit(): void {
    this.initForm();
    this.getCurrentItem();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  ngAfterViewInit() {
    this.listenToInputChanges();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      title: ['', [Validators.required]],
      article: ['']
    });
  }

  listenToInputChanges() {
    fromEvent(this.input.nativeElement, 'keyup')
      .pipe(
        takeUntil(this.unsubscribe$),
        debounceTime(1000),
        distinctUntilChanged(),
        tap(() => {
          const value = this.input.nativeElement.value;
          if (value.length < 5) {
            return;
          } else {
            this.searchElement(value);
          }
        })
      )
      .subscribe();
  }

  searchElement(value: any) {
    const params = {
      limit: 25,
      filter: value,
    };
    this.articleService.getAllItems(params).pipe(takeUntil(this.unsubscribe$)).subscribe(val => {
      this.itemList = val;
    });
  }

  getCurrentItem() {
    this.articleService
      .getAllItems({}, 'headline')
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          if(!this.selectedList) {
            this.selectedList = [];
          }
        })
      )
      .subscribe((val) => {
        this.selectedList = val;
      });
  }

  setItem(item: any) {
    this.defaultForm.controls.article.patchValue(item);
    this.switchButtonState();
    this.articleService
      .setHeadline(this.defaultForm.value)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }
}

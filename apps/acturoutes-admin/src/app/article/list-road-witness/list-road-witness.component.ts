import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Article } from '@shared/models/article/article.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ArticleService } from '../shared/services/article.service';
import { MatDialog } from '@angular/material/dialog';
import { CreateRoadWitnessComponent } from '../create-road-witness/create-road-witness.component';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Component({
  selector: 'app-list-road-witness',
  templateUrl: './list-road-witness.component.html',
  styleUrls: ['./list-road-witness.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListRoadWitnessComponent implements OnDestroy {
  optionForm: FormGroup = new FormGroup({});
  dataService = new ArticleService(this.apiService);
  dataRoute = ENDPOINTS.roadWitness;
  params: any = {
    fields: '-__v'
  };
  displayedColumns: tableColumn[] = [
    {
      name: 'title',
      label: 'Titre',
    },
    {
      name: 'description',
      label: 'Description',
    },
  ];
  columns = ['edit_action', 'title', 'description'];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private articleService: ArticleService,
    private ref: ChangeDetectorRef
  ) {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  editOrCreate(ev?: Article) {
    const dialogRef = this.dialog.open(CreateRoadWitnessComponent, {
      maxWidth: '800px',
      maxHeight: '80vh',
      data: ev,
      position:  {
        top: '6%'
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.params = { ...this.params };
        this.ref.detectChanges();
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.articleService
      .deleteItem({ uuid: ids , route: ENDPOINTS.roadWitnessDelete})
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = { ...this.params };
      });
  }

  get haveParams() {
    return Object.keys(this.params).includes('filterOn');
  }

}

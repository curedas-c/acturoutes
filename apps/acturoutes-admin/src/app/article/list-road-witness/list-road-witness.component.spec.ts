import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListRoadWitnessComponent } from './list-road-witness.component';

describe('ListRoadWitnessComponent', () => {
  let component: ListRoadWitnessComponent;
  let fixture: ComponentFixture<ListRoadWitnessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListRoadWitnessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListRoadWitnessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

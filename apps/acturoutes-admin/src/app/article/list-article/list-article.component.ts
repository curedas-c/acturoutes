import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Article } from '@shared/models/article/article.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ArticleService } from '../shared/services/article.service';
import { CreateArticleComponent } from '../create-article/create-article.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-list-article',
  templateUrl: './list-article.component.html',
  styleUrls: ['./list-article.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListArticleComponent implements OnInit, OnDestroy {
  optionForm: FormGroup = new FormGroup({});
  dataService = new ArticleService(this.apiService);
  params: any = {
    fields: '-__v'
  };
  displayedColumns: tableColumn[] = [
    {
      name: 'title',
      label: 'Titre',
    },
    {
      name: 'description',
      label: 'Description',
    },
  ];
  columns = ['edit_action', 'title', 'description'];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private articleService: ArticleService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.optionForm = this.fb.group({
      type: [null],
    });
    this.listenToInputChanges();
  }

  listenToInputChanges() {
    this.optionForm.controls.type.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.params = {
          ...this.params,
          filterOn: value,
        };
        this.ref.detectChanges();
      });
  }

  editOrCreate(ev?: Article) {
    const dialogRef = this.dialog.open(CreateArticleComponent, {
      maxWidth: '1000px',
      maxHeight: '80vh',
      data: ev,
      position:  {
        top: '6%'
      }
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.params = { ...this.params };
        this.ref.detectChanges();
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.articleService
      .deleteItem({ uuid: ids })
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = { ...this.params };
      });
  }

  get haveParams() {
    return Object.keys(this.params).includes('filterOn');
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticleComponent } from './article.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'article',
    pathMatch: 'full'
  },
  {
    path: 'article', component: ArticleComponent
  },
  {
    path: 'road-witness', component: ArticleComponent
  },
  {
    path: 'headline', component: ArticleComponent
  },
  {
    path: 'big-format', component: ArticleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }

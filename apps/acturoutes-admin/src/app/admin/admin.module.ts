import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { SharedModule } from '@shared/modules/shared.module';
import { TableModule } from '@shared/modules/table.module';
import { AdminCreateComponent } from './admin-create/admin-create.component';
import { AdminListComponent } from './admin-list/admin-list.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { AdminService } from './shared/services/admin.service';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { NgxPermissionsModule } from 'ngx-permissions';


@NgModule({
  declarations: [
    AdminComponent,
    AdminCreateComponent,
    AdminListComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule,
    FileSelectorModule,
    MatDialogModule,
    MatTabsModule,
    TableModule,
    ConfirmModule,
    NgxPermissionsModule.forChild()
  ],
  providers: [
    AdminService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ]
})
export class AdminModule { }

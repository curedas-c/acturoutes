import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmService } from '@core/services/confirm.service';
import { CONFIRMATION_TYPE, ConfirmInterface } from '@shared/models/confirm/confirm.model';
import { addControl } from '@shared/utils/formGroupModifier';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { AdminService } from '../shared/services/admin.service';

@Component({
  selector: 'app-admin-create',
  templateUrl: './admin-create.component.html',
  styleUrls: ['./admin-create.component.scss']
})
export class AdminCreateComponent implements OnInit {
  defaultForm: FormGroup = new FormGroup({});
  currentData: any;
  isButtonDisabled = false;
  adminList$: Observable<any>;
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialogRef: MatDialogRef<AdminCreateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private confirmService: ConfirmService,
    private adminService: AdminService
  ) {
    this.currentData = this.data;
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      name: [this.currentData?.name || '', [Validators.required]],
      email: [this.currentData?.email || '', [Validators.required]],
      role: [this.currentData?.role || '', [Validators.required]],
    });
    if (!this.currentData) {
      this.defaultForm = addControl(this.defaultForm, 'password');
    }
  }

  onDelete(ID: number) {
    const dialogData: ConfirmInterface = {
      type: CONFIRMATION_TYPE.CANCEL,
      title: 'Suppression de Livreur',
      image: false,
      message: 'êtes vous sûr(e) de vouloir supprimer ce compte ?',
      btnYes: 'SUPPRIMER',
      btnNo: 'NON'
    };

    this.confirmService
      .openDialog(dialogData)
      .pipe(
        takeUntil(this.unsubscribe$)
      )
      .subscribe(dialog => {
        if (dialog) {
          this.switchButtonState();
          this.adminService
            .deleteItem([ID])
            .pipe(
              takeUntil(this.unsubscribe$),
              finalize(() => {
                this.switchButtonState();
              })
              )
            .subscribe((res) => {
              this.dialogRef.close(true);
            });
        }
      });
  }

  sendForm() {
    this.switchButtonState();
    this.adminService
      .setItem(this.defaultForm.value, this.currentData?._id)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
          if (this.dialogRef.id) {
            this.dialogRef.close(true);
          }
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

}

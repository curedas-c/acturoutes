import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { Admin } from '@shared/models/admin/admin.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class AdminService {
  constructor(private _apiService: ApiService) { }

  getTableData(options: { params?: any}): Observable<DataTable<Admin>> {
    const { params } = options;
    return this._apiService
      .get(`${ENDPOINTS.admin}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Admin[] = response.items.map((res) => {
            return new Admin(res);
          });

          return {
            items: mapped,
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getItem(uuid: string | number, params?: any): Observable<Admin> {
    return this._apiService
      .get(`${ENDPOINTS.admin}/${uuid}`, params)
      .pipe(
        map((response: any) => {
          return new Admin(response.data);
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(value: any, uuid?: string | number, params?: any): Observable<Admin> {
    const url = !!uuid ? `${ENDPOINTS.admin}/${uuid}` : `${ENDPOINTS.admin}`;
    const request = !!uuid ? this._apiService.patch(url, value, params) : this._apiService.post(url, value, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.adminDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getAllItems(params?: any): Observable<Admin[]> {
    return this._apiService
      .get(`${ENDPOINTS.admin}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Admin[] = response.items.map((res) => {
            return new Admin(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { Admin } from '@shared/models/admin/admin.model';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AdminCreateComponent } from '../admin-create/admin-create.component';
import { AdminService } from '../shared/services/admin.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.scss']
})
export class AdminListComponent implements OnInit, OnDestroy {
  dataService = new AdminService(this.apiService);
  params = {};
  displayedColumns: tableColumn[] = [
    {
      name: 'name',
      label: 'Nom',
    },
    {
      name: 'email',
      label: 'Email',
    },
    {
      name: 'role',
      label: 'Role',
    },
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true
    }
  ];
  columns = [
    'edit_action',
    'name',
    'email',
    'role',
    'createdAt'
  ];
  private unsubscribe$ = new Subject<void>();
  constructor(public dialog: MatDialog, private apiService: ApiService, private adminService: AdminService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


  editItem(ev: Admin) {
    const dialogRef = this.dialog.open(AdminCreateComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.params = {...this.params};
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.adminService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = {...this.params};
      });
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { TrafficImage } from '@shared/models/traffic-image/traffic-image.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { TrafficService } from '../shared/services/traffic.service';

@Component({
  selector: 'app-traffic-image',
  templateUrl: './traffic-image.component.html',
  styleUrls: ['./traffic-image.component.scss'],
})
export class TrafficImageComponent implements OnInit, OnDestroy {
  defaultForm: FormGroup = new FormGroup({});
  defaultImage: string[];
  currentData: TrafficImage;
  isButtonDisabled = false;

  params: any = {
    filterOn: 'isRdcImage',
    filterOnValue: false,
  };

  private unsubscribe$ = new Subject<void>();
  constructor(
    private fb: FormBuilder,
    private trafficService: TrafficService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      _id: ['', [Validators.required]],
      image: [''],
      isRdcImage: [false],
      title: [''],
    });
    this.listenToInputChanges();
  }

  listenToInputChanges() {
    this.defaultForm.controls._id.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.switchButtonState();
        this.defaultForm.controls.title.disable();
        this.trafficService
          .getImage(value, this.params)
          .pipe(
            takeUntil(this.unsubscribe$),
            finalize(() => {
              this.switchButtonState();
              this.defaultForm.controls.title.enable();
              this.defaultForm.controls.image.reset(null);
            })
          )
          .subscribe((res) => {
            if (res) {
              this.currentData = res;
              this.defaultImage = res.image ? [`${environment.rootUrl}/${res.image}`] : null;
              this.defaultForm.controls.title.patchValue(res.title);
            } else {
              this.defaultImage = null;
            }
          });
      });
  }

  sendForm() {
    this.switchButtonState();
    this.trafficService
      .setImage(this.defaultForm)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.image.patchValue(null);
    }
  }
}

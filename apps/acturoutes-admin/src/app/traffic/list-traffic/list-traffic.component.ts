import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Traffic } from '@shared/models/traffic/traffic.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CreateTrafficComponent } from '../create-traffic/create-traffic.component';
import { TrafficService } from '../shared/services/traffic.service';

@Component({
  selector: 'app-list-traffic',
  templateUrl: './list-traffic.component.html',
  styleUrls: ['./list-traffic.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListTrafficComponent implements OnInit, OnDestroy {
  optionForm: FormGroup = new FormGroup({});
  dataService = new TrafficService(this.apiService);
  params: any = {
    filterOn: 'isRdcMessage',
    filterOnValue: false,
  };
  displayedColumns: tableColumn[] = [
    {
      name: 'message',
      label: 'Message',
    },
  ];
  columns = ['edit_action', 'message'];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private trafficService: TrafficService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.optionForm = this.fb.group({
      date: ['', [Validators.required]],
    });
    this.listenToInputChanges();
  }

  listenToInputChanges() {
    this.optionForm.controls.date.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((value) => {
        this.params = {
          ...this.params,
          day: value,
        };
        this.ref.detectChanges();
      });
  }

  editItem(ev: Traffic) {
    const dialogRef = this.dialog.open(CreateTrafficComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.params = { ...this.params };
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.trafficService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = { ...this.params };
      });
  }

  get haveParams() {
    return Object.keys(this.params).length > 0;
  }
}

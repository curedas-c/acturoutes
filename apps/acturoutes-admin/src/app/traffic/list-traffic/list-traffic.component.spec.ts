import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTrafficComponent } from './list-traffic.component';

describe('ListTrafficComponent', () => {
  let component: ListTrafficComponent;
  let fixture: ComponentFixture<ListTrafficComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListTrafficComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTrafficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

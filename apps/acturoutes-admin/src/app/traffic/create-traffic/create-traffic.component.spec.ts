import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTrafficComponent } from './create-traffic.component';

describe('CreateTrafficComponent', () => {
  let component: CreateTrafficComponent;
  let fixture: ComponentFixture<CreateTrafficComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTrafficComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTrafficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

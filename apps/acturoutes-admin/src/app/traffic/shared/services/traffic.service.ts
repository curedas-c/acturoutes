import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { Traffic } from '@shared/models/traffic/traffic.model';
import { TrafficImage } from '@shared/models/traffic-image/traffic-image.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class TrafficService {
  constructor(private _apiService: ApiService) { }
  
  getTableData(options: { params?: any}): Observable<DataTable<Traffic>> {
    const { params } = options;
    return this._apiService
      .get(`${ENDPOINTS.traffic}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Traffic[] = response.items?.map((res) => {
            return new Traffic(res);
          });

          return {
            items: mapped,
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getItem(uuid: string | number, params?: any): Observable<Traffic> {
    return this._apiService
      .get(`${ENDPOINTS.traffic}/${uuid}`, params)
      .pipe(
        map((response: any) => {
          return response.data ? new Traffic(response.data) : null;
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(item: FormGroup, uuid?: string | number, params?: any): Observable<Traffic> {
    let formData = new FormData();

    Object.keys(item.controls).forEach((key) => {
      const value = item.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    const url = !!uuid ? `${ENDPOINTS.traffic}/${uuid}` : `${ENDPOINTS.traffic}`;

    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.trafficDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  getAllItems(params?: any): Observable<Traffic[]> {
    return this._apiService
      .get(`${ENDPOINTS.traffic}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Traffic[] = response.items?.map((res) => {
            return new Traffic(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getImage(id: number, params?: any): Observable<TrafficImage> {
    return this._apiService
      .get(`${ENDPOINTS.trafficImage}/${id}`, params)
      .pipe(
        map((response: any) => {
          return response.data ? new TrafficImage(response.data) : null;
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setImage(item: FormGroup, params?: any): Observable<Traffic> {
    let formData = new FormData();

    Object.keys(item.controls).forEach((key) => {
      const value = item.controls[key].value;
      if (value !== null && value !== undefined) {
        if (value?.constructor === File) {
          formData.append(key, value);
        } else if (value?.constructor === Array) {
          const isFileArray = value[0].constructor === File ? true : false;
          value.forEach((item) => {
            formData.append(isFileArray ? key : `${key}[]`, item);
          });
        } else {
          formData.append(key, value);
        }
      }
    });

    return this._apiService.post(`${ENDPOINTS.trafficImage}`, formData, params).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

}

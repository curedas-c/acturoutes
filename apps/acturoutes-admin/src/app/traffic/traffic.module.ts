import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrafficRoutingModule } from './traffic-routing.module';
import { TrafficComponent } from './traffic.component';
import { CreateTrafficComponent } from './create-traffic/create-traffic.component';
import { ListTrafficComponent } from './list-traffic/list-traffic.component';
import { SharedModule } from '@shared/modules/shared.module';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { MatTabsModule } from '@angular/material/tabs';
import { TrafficImageComponent } from './traffic-image/traffic-image.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { TableModule } from '@shared/modules/table.module';
import { TrafficService } from './shared/services/traffic.service';


@NgModule({
  declarations: [
    TrafficComponent,
    CreateTrafficComponent,
    ListTrafficComponent,
    TrafficImageComponent
  ],
  imports: [
    CommonModule,
    TrafficRoutingModule,
    SharedModule,
    FileSelectorModule,
    MatTabsModule,
    MatDialogModule,
    ConfirmModule,
    TableModule
  ],
  providers: [
    TrafficService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ]
})
export class TrafficModule { }

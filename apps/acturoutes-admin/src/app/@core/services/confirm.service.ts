import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConfirmInterface, ConfirmDialogModel } from '@shared/models/confirm/confirm.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogConfirmComponent } from '@shared/components/dialog-confirm/dialog-confirm.component';

@Injectable()
export class ConfirmService {
  constructor(public dialog: MatDialog) {}
  loadConfirmDialog(value: ConfirmInterface): Observable<any> {
    const dialogData: ConfirmInterface = new ConfirmDialogModel(
      value.type,
      value.title,
      value.image,
      value.imageLink,
      value.isHeader,
      value.message,
      value.btnYes,
      value.btnNo,
      value.data
    );
    // const dialogData: ConfirmInterface = ConfirmDialogModel.getInstance(value);

    const dialogRef = this.dialog.open(DialogConfirmComponent, {
      maxWidth: '400px',
      data: dialogData
    });

    return dialogRef.afterClosed();
  }

  openDialog(input: ConfirmInterface): Observable<any> {
    return this.loadConfirmDialog(input);
  }
}

import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { CookiesService } from '@core/services/cookies.service';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SocketService extends Socket {

  public constructor(private cookies: CookiesService) {
    super({
      url: environment.socketUrl, options: {
        transportOptions: {
          polling: {
            extraHeaders: {
              Authorization: 'Bearer ' + cookies.getAccessToken()
            }
          }
        }
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StateService } from './state.service';

interface AppState {
  currentCanal: any;
  update: any;
}

const initialState: AppState = {
  currentCanal: null,
  update: null
};

@Injectable({
  providedIn: 'root'
})
export class AppStateService extends StateService<AppState> {
  currentCanal$: Observable<any> = this.select((state) => state.currentCanal);
  update$: Observable<any> = this.select((state) => state.update);
  
  constructor() {
    super(initialState);
  }

  setCurrentCanal(item: any) {
    this.setState({
      currentCanal: item
    });
  }

  triggerUpdate(data: any) {
    this.setState({
      update: data
    });
  }
}

import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';
import { ForgotPasswordCredentials } from '@shared/models/auth/forgotPasswordCredentials.model';
import { LoginCredentials } from '@shared/models/auth/loginCredentials.model';
import { ResetPasswordCredentials } from '@shared/models/auth/resetPasswordCredentials.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CookiesService } from './cookies.service';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
/**
   * Auth Endpoint
   */
  private authEndpoint = {
    admin: 'admin',
    login: 'auth/admin/sign-in',
    resetPassword: 'admin/password/reset',
    updatePassword: 'admin/password/update',
    details: 'admin/details',
    confirmation: 'auth/admin/confirmation-tokens'
  };

  /**
   * Auth Service constructor
   */
  constructor(private apiService: ApiService, private router: Router, private cookie: CookiesService) {}
  /**
   * Login
   */
  public login(credentials: LoginCredentials, params?: any): Observable<any> {
    return this.apiService.post(`${this.authEndpoint.login}`, credentials, params).pipe(
      map((response: any) => response),
      catchError(error => throwError(error))
    );
  }

  /**
   * Logout
   */
  public logout(): Observable<any> {
    return this.cookie.clearCookie('admin_credentials').pipe(tap(() => {
      this.router.navigate(['/auth']);
    }));
  }

  /**
   * Reset Password
   */
  public resetPassword(credentials: ForgotPasswordCredentials): Observable<any> {
    return this.apiService.post(`${this.authEndpoint.resetPassword}`, credentials).pipe(
      map((response: any) => response),
      catchError(error => throwError(error))
    );
  }

  /**
   * Change password
   */
  public updatePassword(credentials: ResetPasswordCredentials): Observable<any> {
    return this.apiService.post(`${this.authEndpoint.updatePassword}`, credentials).pipe(
      map((response: any) => response),
      catchError(error => throwError(error))
    );
  }

  public getDetails(): Observable<any> {
    return this.apiService.get(`${this.authEndpoint.details}`).pipe(
      map((response: any) => response),
      catchError(error => throwError(error))
    );
  }

  public updateData(data: any): Observable<any> {
    return this.apiService.patch(`${this.authEndpoint.admin}`, data).pipe(
      map((response: any) => response),
      catchError(error => throwError(error))
    );
  }
}

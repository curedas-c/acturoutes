import { Injectable } from '@angular/core';
import { SocketService } from './socket.service';
import { ChatRoom } from '@shared/models/intranet/chat-room.model';
import { Admin } from '@shared/models/admin/admin.model';
import { ChatMessage } from '@shared/models/intranet/chat-message.model';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ApiService } from '@services/api.service';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  public constructor(private _apiService: ApiService, private socket: SocketService) {}

  public sendMessage(roomId: string, msg: ChatMessage): void {
    this.socket.emit('send_message', {room: roomId, message: msg});
  }

  public getMessage(): Observable<any> {
    return this.socket.fromEvent<any>('new_message');
  }

  public joinRoom(roomId: string): void {
    this.socket.emit('join_room', roomId);
  }

  public getRooms(): Observable<ChatRoom> {
    return this.socket.fromEvent<ChatRoom>('updated_rooms');
  }

  public addRoom(roomName: string): void {
    this.socket.emit('add_room', roomName);
  }

  public getAdmins(): Observable<Admin[]> {
    return this.socket.fromEvent<Admin[]>('admins_room');
  }

  public setChatRoom(formData: any, uuid?: string | number, params?: any): Observable<any> {
    const url = !!uuid ? `${ENDPOINTS.chatRoom}/${uuid}` : `${ENDPOINTS.chatRoom}`;
    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  public getAllChatRooms(params?: any): Observable<ChatRoom[]> {
    return this._apiService
      .get(`${ENDPOINTS.chatRoom}`, params)
      .pipe(
        map((response: any) => {
          const mapped: ChatRoom[] = response.items.map((res) => {
            return new ChatRoom(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  public getRoomMessages(roomId: string, params?: any): Observable<ChatMessage[]> {
    return this._apiService
      .get(`${ENDPOINTS.chatRoom}/${roomId}/messages`, params)
      .pipe(
        map((response: any) => {
          const mapped: ChatMessage[] = response.items.map((res) => {
            return new ChatMessage(res);
          });
          
          return mapped || [];
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  deleteChatRoom(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.chatRoomDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }
}

import { menu } from '@shared/models/menu.model';

export const MAIN_MENU: menu[] = [
  // Acceuil
  {
    path: '/dashboard/home',
    menu: {
      title: 'Acceuil',
      icon: 'home'
    }
  },
  {
    path: '/dashboard/publications',
    menu: {
      title: 'Publications',
      icon: 'feed'
    },
    permissions: ['admin', 'redactor']
  },
  {
    path: '/dashboard/rdc',
    menu: {
      title: 'Contenu RDC',
      icon: 'flag'
    },
    permissions: ['admin', 'redactor', 'rdc_user']
  },
  {
    path: '/dashboard/traffic',
    menu: {
      title: 'Gestion du traffic',
      icon: 'traffic'
    },
    permissions: ['admin', 'redactor']
  },
  {
    path: '/dashboard/signal',
    menu: {
      title: 'Gestion du Signal',
      icon: 'message'
    },
    permissions: ['admin', 'redactor']
  },
  {
    path: '/dashboard/pubs',
    menu: {
      title: 'Espaces Publicitaires',
      icon: 'ads_click'
    },
    permissions: ['admin', 'commercial']
  },
  {
    path: '/dashboard/tv',
    menu: {
      title: 'Acturoute TV',
      icon: 'tv'
    },
    permissions: ['admin', 'commercial']
  },
  {
    path: '/dashboard/intranet',
    menu: {
      title: 'Intranet',
      icon: 'forum'
    }
  },
  {
    path: '/dashboard/user',
    menu: {
      title: 'Utilisateurs',
      icon: 'person'
    },
    permissions: ['admin', 'commercial']
  },
  {
    path: '/dashboard/admin',
    menu: {
      title: 'Administrateurs',
      icon: 'admin_panel_settings'
    },
    permissions: ['admin']
  }
];

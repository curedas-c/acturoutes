import { Component } from '@angular/core';
import { CookiesService } from '@core/services/cookies.service';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { NgxPermissionsService } from 'ngx-permissions';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'acturoutes-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  private unsubscribe$ = new Subject<void>();
  constructor(public loader: LoadingBarService, private permissionsService: NgxPermissionsService, private cookie: CookiesService) {}
  ngOnInit() {
    this.cookie.getCookie('admin_credentials').pipe(takeUntil(this.unsubscribe$)).subscribe(res => {
      if (res?.role) {
        this.permissionsService.addPermission(res.role);
      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActuTvRoutingModule } from './actu-tv-routing.module';
import { ActuTvComponent } from './actu-tv.component';
import { SharedModule } from '@shared/modules/shared.module';
import { FileSelectorModule } from '@shared/modules/file-selector.module';
import { ActuTvService } from './shared/services/actu-tv.service';


@NgModule({
  declarations: [
    ActuTvComponent
  ],
  imports: [
    CommonModule,
    ActuTvRoutingModule,
    SharedModule,
    FileSelectorModule
  ],
  providers: [ActuTvService]
})
export class ActuTvModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ActuTvComponent } from './actu-tv.component';

const routes: Routes = [
  {
    path: '',
    component: ActuTvComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActuTvRoutingModule { }

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { ActuTv } from '@shared/models/actu-tv/actu-tv.model';
import { Subject } from 'rxjs';
import { finalize, takeUntil } from 'rxjs/operators';
import { ActuTvService } from './shared/services/actu-tv.service';

@Component({
  selector: 'app-actu-tv',
  templateUrl: './actu-tv.component.html',
  styleUrls: ['./actu-tv.component.scss'],
})
export class ActuTvComponent implements OnInit, OnDestroy {
  defaultForm: FormGroup = new FormGroup({});
  currentData: ActuTv;
  isButtonDisabled = false;
  isLoading = true;
  haveError = false;
  defaultImage: string[];
  private unsubscribe$ = new Subject<void>();
  constructor(private fb: FormBuilder, private actuTvService: ActuTvService) {}

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  initForm() {
    this.defaultForm = this.fb.group({
      title: [this.currentData?.title || '', [Validators.required]],
      description: [this.currentData?.description || '', [Validators.required]],
      link: [this.currentData?.link || '', [Validators.required, Validators.pattern('^(https|http):\/\/(?:www\.)?youtube.com\/embed\/[A-z0-9]+')]],
      image: [''],
    });
  }

  getData() {
    this.switchButtonState();
    this.actuTvService
      .getItem()
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
        })
      )
      .subscribe((res) => {
        if (res) {
          this.currentData = res;
          this.defaultImage = res.image ? [`${environment.rootUrl}/${res.image}`] : null;
        } else {
          this.defaultImage = null;
        }
        this.initForm();
        this.isLoading = false;
      });
  }

  sendForm() {
    this.switchButtonState();
    this.actuTvService
      .setItem(this.defaultForm, this.currentData ? 1 : null)
      .pipe(
        takeUntil(this.unsubscribe$),
        finalize(() => {
          this.switchButtonState();
        })
      )
      .subscribe();
  }

  switchButtonState() {
    this.isButtonDisabled = !this.isButtonDisabled;
  }

  async setFiles(files: File[]) {
    if (files[0]) {
      this.defaultForm.controls.image.patchValue(files[0]);
    } else {
      this.defaultForm.controls.image.patchValue(null);
    }
  }

  get haveValue() {
    return Object.values(this.defaultForm.value).length > 0;
  }
}

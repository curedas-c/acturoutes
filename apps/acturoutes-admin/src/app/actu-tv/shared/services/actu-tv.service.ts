import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { ActuTv } from '@shared/models/actu-tv/actu-tv.model';
import { FormGroup } from '@angular/forms';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable()
export class ActuTvService {
  constructor(private _apiService: ApiService) { }

  getItem(params?: any): Observable<ActuTv> {
    return this._apiService
      .get(`${ENDPOINTS.actuTv}`, params)
      .pipe(
        map((response: any) => {
          const res = response.data ? new ActuTv(response.data) : null;
          return res;
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(value: FormGroup, uuid?: string | number, params?: any): Observable<ActuTv> {
    let formData = new FormData();

    Object.keys(value.controls).forEach((key) => {
      formData.append(key, value.controls[key].value);
    });

    const url = `${ENDPOINTS.actuTv}`;
    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActuTvComponent } from './actu-tv.component';

describe('ActuTvComponent', () => {
  let component: ActuTvComponent;
  let fixture: ComponentFixture<ActuTvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActuTvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActuTvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

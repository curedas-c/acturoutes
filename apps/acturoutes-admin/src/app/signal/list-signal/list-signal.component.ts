import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from '@core/services/api.service';
import { Signal } from '@shared/models/signal/signal.model';
import { tableColumn } from '@shared/models/table/tableColumn.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CreateSignalComponent } from '../create-signal/create-signal.component';
import { SignalService } from '../shared/services/signal.service';

@Component({
  selector: 'app-list-signal',
  templateUrl: './list-signal.component.html',
  styleUrls: ['./list-signal.component.scss']
})
export class ListSignalComponent implements OnInit, OnDestroy {
  dataService = new SignalService(this.apiService);
  params = {
    filterOn: 'isRdcMessage',
    filterOnValue: false,
  };
  displayedColumns: tableColumn[] = [
    {
      name: 'message',
      label: 'Message',
    },
    {
      name: 'createdAt',
      label: 'Date de création',
      isDate: true,
    }
  ];
  columns = ['edit_action', 'message', 'createdAt'];
  private unsubscribe$ = new Subject<void>();
  constructor(
    public dialog: MatDialog,
    private apiService: ApiService,
    private signalService: SignalService
  ) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  editItem(ev: Signal) {
    const dialogRef = this.dialog.open(CreateSignalComponent, {
      width: '800px',
      maxHeight: '90vh',
      data: ev,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.params = { ...this.params };
      }
    });
  }

  removeItems(ids: string[] | number[]) {
    this.signalService
      .deleteItem(ids)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        this.params = { ...this.params };
      });
  }

}

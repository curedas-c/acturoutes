import { Injectable } from '@angular/core';
import { ApiService } from '@core/services/api.service';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { DataTable } from '@shared/models/table/dataTable.model';
import { Signal } from '@shared/models/signal/signal.model';
import { ENDPOINTS } from '@shared/constants/endpoints.constant';

@Injectable({
  providedIn: 'root'
})
export class SignalService {
  constructor(private _apiService: ApiService) { }
  
  getTableData(options: { params?: any}): Observable<DataTable<Signal>> {
    const { params } = options;
    return this._apiService
      .get(`${ENDPOINTS.signal}`, params)
      .pipe(
        map((response: any) => {
          const mapped: Signal[] = response.items?.map((res) => {
            return new Signal(res);
          });

          return {
            items: mapped,
            total_items: response.total_items,
            total_pages: response.total_pages,
          };
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  getItem(uuid: string | number, params?: any): Observable<Signal> {
    return this._apiService
      .get(`${ENDPOINTS.signal}/${uuid}`, params)
      .pipe(
        map((response: any) => {
          return response.data ? new Signal(response.data) : null;
        }),
        catchError((error: HttpErrorResponse) => {
          return observableThrowError(error);
        })
      );
  }

  setItem(formData: any, uuid?: string | number, params?: any): Observable<Signal> {
    const url = !!uuid ? `${ENDPOINTS.signal}/${uuid}` : `${ENDPOINTS.signal}`;

    const request = !!uuid ? this._apiService.patch(url, formData, params) : this._apiService.post(url, formData, params);

    return request.pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }

  deleteItem(uuid?: string[] | number []): Observable<any> {
    return this._apiService.post(`${ENDPOINTS.signalDelete}`, { uuid }).pipe(
      map((response: any) => response.data),
      catchError((error: HttpErrorResponse) => {
        return observableThrowError(error);
      })
    );
  }
}

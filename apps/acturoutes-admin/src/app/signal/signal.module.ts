import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignalRoutingModule } from './signal-routing.module';
import { SignalComponent } from './signal.component';
import { SharedModule } from '@shared/modules/shared.module';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmModule } from '@shared/modules/confirm.module';
import { TableModule } from '@shared/modules/table.module';
import { SignalService } from './shared/services/signal.service';
import { CreateSignalComponent } from './create-signal/create-signal.component';
import { ListSignalComponent } from './list-signal/list-signal.component';


@NgModule({
  declarations: [
    SignalComponent,
    CreateSignalComponent,
    ListSignalComponent
  ],
  imports: [
    CommonModule,
    SignalRoutingModule,
    SharedModule,
    MatTabsModule,
    MatDialogModule,
    ConfirmModule,
    TableModule
  ],
  providers: [
    SignalService,
    { provide: MatDialogRef, useValue: {} },
    { provide: MAT_DIALOG_DATA, useValue: null }
  ]
})
export class SignalModule { }

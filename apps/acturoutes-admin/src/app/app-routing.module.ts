import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from '@shared/components/layout/layout.component';
import { AuthGuard } from '@core/guards/auth.guard';
import { NgxPermissionsGuard } from 'ngx-permissions';

const routes: Routes = [
  {
    path: 'dashboard',
    component: LayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        loadChildren: () =>
          import('./home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'publications',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'redactor'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./article/article.module').then((m) => m.ArticleModule),
      },
      {
        path: 'traffic',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'redactor'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./traffic/traffic.module').then((m) => m.TrafficModule),
      },
      {
        path: 'signal',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'redactor'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./signal/signal.module').then((m) => m.SignalModule),
      },
      {
        path: 'pubs',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'commercial'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./pubs/pubs.module').then((m) => m.PubsModule),
      },
      {
        path: 'tv',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'commercial'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./actu-tv/actu-tv.module').then((m) => m.ActuTvModule),
      },
      {
        path: 'intranet',
        loadChildren: () =>
          import('./intranet/intranet.module').then((m) => m.IntranetModule),
      },
      {
        path: 'admin',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./admin/admin.module').then((m) => m.AdminModule),
      },
      {
        path: 'user',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'commercial'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./user/user.module').then((m) => m.UserModule),
      },
      {
        path: 'account',
        loadChildren: () =>
          import('./account/account.module').then((m) => m.AccountModule),
      },
      {
        path: 'rdc',
        canActivate: [NgxPermissionsGuard],
        data: {
          permissions: {
            only: ['admin', 'redactor', 'rdc_user'],
            redirectTo: '/dashboard/home',
          },
        },
        loadChildren: () =>
          import('./rdc/rdc.module').then((m) => m.RdcModule),
      },
      {
        path: '404',
        loadChildren: () =>
          import('./error-page/error-page.module').then(
            (m) => m.ErrorPageModule
          ),
      },
    ],
  },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/dashboard/home',
  },
  {
    path: '**',
    redirectTo: '/dashboard/404',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabledBlocking' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
